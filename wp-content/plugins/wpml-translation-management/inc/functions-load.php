<?php

function wpml_tm_load_status_display_filter() {
	global $wpml_tm_status_display_filter, $sitepress, $wpdb;

	if ( !isset( $wpml_tm_status_display_filter ) ) {
		$user_id                       = get_current_user_id ();
		$lang_pairs                    = get_user_meta ( $user_id, $wpdb->prefix . 'language_pairs', true );
		$wpml_tm_status_display_filter = new WPML_TM_Translation_Status_Display(
			$user_id,
			current_user_can ( 'manage_options' ),
			$lang_pairs,
			$sitepress->get_current_language (),
			$sitepress->get_active_languages ()
		);
	}

	$wpml_tm_status_display_filter->init ();
}

function wpml_tm_init_mail_notifications() {
	global $wpml_tm_mailer;

	if ( !isset( $wpml_tm_mailer ) ) {
		require WPML_TM_PATH . '/inc/local-translation/wpml-tm-mail-notification.class.php';
		$wpml_tm_mailer = new WPML_TM_Mail_Notification();
	}
	$wpml_tm_mailer->init();

	return $wpml_tm_mailer;
}