<?php
/**
 * @package wpml-tm
 */

static $migration_2_0_1_done = false;
if ( ! $migration_2_0_1_done && ! defined( 'DOING_AJAX' ) ) {
	// The following code will only fire the migration logic
	// no other migration actions will be taken here,
	// as they are supposed to be either handled via XMLRPC or a manual action
	TranslationProxy::start_migration();
	$migration_2_0_1_done = true;
}