<?php

require_once WPML_TM_PATH . '/inc/translation-jobs/jobs/wpml-translation-job.class.php';

class WPML_Post_Translation_Job extends WPML_Translation_Job {

	private $original_doc_id = false;

	public function __construct( $job_data ) {
		$this->basic_data = $job_data;
	}

	public function get_type() {
		return 'Post';
	}

	protected function load_resultant_element_id() {
		global $wpdb;

		return $wpdb->get_var( $wpdb->prepare( "SELECT element_id FROM {$wpdb->prefix}icl_translations WHERE translation_id = %d",
		                                       $this->basic_data->translation_id ) );
	}

	public function to_array() {
		$data_array                      = $this->basic_data_to_array( $this->basic_data );
		$data_array[ 'id' ]              = $this->basic_data->job_id;
		$data_array[ 'translation_id' ]  = $this->basic_data->translation_id;
		$data_array[ 'status' ]          = $this->get_status();
		$data_array[ 'translated_link' ] = $this->get_edit_link();

		return $data_array;
	}

	protected function load_status() {
		return TranslationManagement::get_job_status_string( $this->basic_data->status,
		                                                     $this->basic_data->needs_update );
	}


	public function get_edit_link() {
		$edit_link = false;
		if ( $this->get_status() == 'Complete' ) {
			$edit_link = get_edit_post_link( $this->get_resultant_element_id() );
		}

		return $edit_link;
	}

	public function get_original_element_id() {
		global $wpdb;
		if ( ! $this->original_doc_id ) {
			$original_element_id_query   = "SELECT field_data FROM {$wpdb->prefix}icl_translate WHERE job_id = %d AND field_type='original_id'";
			$original_element_id_args    = array( $this->get_id() );
			$original_element_id_prepare = $wpdb->prepare( $original_element_id_query, $original_element_id_args );
			$original_element_id         = $wpdb->get_var( $original_element_id_prepare );
			$this->original_doc_id       = $original_element_id;
		} else {
			$original_element_id = $this->original_doc_id;
		}

		return $original_element_id;
	}

	public function get_id() {
		return $this->basic_data->job_id;
	}

}
