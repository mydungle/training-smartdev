<?php

require_once WPML_TM_PATH . '/inc/translation-jobs/jobs/wpml-translation-job.class.php';

class WPML_String_Translation_Job extends WPML_Translation_Job {

	public function __construct( $job_data ) {
		$this->basic_data         = $job_data;
		$this->basic_data->job_id = $this->generate_job_id();
	}

	public function get_type() {
		return 'String';
	}

	protected function load_resultant_element_id() {
		global $wpdb;

		return $wpdb->get_var( $wpdb->prepare( "SELECT id FROM {$wpdb->prefix}icl_translations
												WHERE string_id = %d AND language_code = %s",
		                                       $this->get_original_element_id(),
		                                       $this->get_language_code() ) );
	}

	public function get_original_element_id() {
		return $this->basic_data->string_id;
	}

	protected function load_status() {
		$this->status = WPML_Remote_String_Translation::get_string_status_label( $this->basic_data->status );

		return $this->status;
	}

	public function get_translator_id() {
		$translator_id = false;
		if ( isset( $this->basic_data->translator_id ) ) {
			$translator_id = $this->basic_data->translator_id;
		}

		return $translator_id;
	}

	private function generate_job_id() {
		if ( property_exists( $this->basic_data, 'rid' ) ) {
			$job_id = $this->basic_data->id . '-' . $this->basic_data->rid;
		} else {
			$job_id = 'string|' . $this->basic_data->id;
		}

		return $job_id;
	}

	public function to_array() {
		$data_array = $this->basic_data_to_array( $this->basic_data );

		$data_array[ 'translation_id' ] = $this->basic_data->id;
		$data_array[ 'status' ]         = $this->get_status();
		$data_array[ 'id' ]             = $this->get_id();

		return $data_array;
	}

}
