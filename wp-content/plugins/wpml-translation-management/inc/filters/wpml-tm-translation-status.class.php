<?php

class WPML_TM_Translation_Status {

    public function init() {
        add_filter(
            'wpml_allowed_target_langs',
            array( $this, 'filter_target_langs' ),
            10,
            3
        );
        add_filter(
            'wpml_translation_status',
            array( $this, 'filter_translation_status' ),
            1,
            5
        );
        add_filter( 'wpml_job_assigned_to_after_assignment', array( $this, 'job_assigned_to_filter' ), 10, 4 );
    }

    protected function is_in_active_job(
        $element_id,
        $target_lang_code,
        $element_type_prefix,
        $return_status = false
    ) {
        /** @var TranslationManagement $iclTranslationManagement */
        /** @var SitePress $sitepress */
        global $iclTranslationManagement;
        $trid = $this->get_element_trid( $element_id, $element_type_prefix );
        if ( $return_status && SitePress::get_source_language_by_trid( $trid ) == $target_lang_code ) {
            $res = ICL_TM_COMPLETE;
        } else {
            $job_id          = $iclTranslationManagement->get_translation_job_id( $trid, $target_lang_code );
            $translation_job = $iclTranslationManagement->get_translation_job( $job_id );
            $res             = false;
            if ( $return_status && $translation_job ) {
                $res = $translation_job->status;
            } elseif ( $translation_job
                       && $translation_job->status != ICL_TM_COMPLETE
                       && $translation_job->status != ICL_TM_DUPLICATE
            ) {
                $res = ICL_TM_COMPLETE;
            }
        }

        return $res;
    }

    protected function needs_update( $trid, $target_lang_code ) {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare(
                " SELECT needs_update
                  FROM {$wpdb->prefix}icl_translation_status
                  WHERE translation_id = ( SELECT translation_id
                                           FROM {$wpdb->prefix}icl_translations
                                           WHERE trid = %d
                                            AND language_code = %s
                                           LIMIT 1)
                  LIMIT 1",
                $trid,
                $target_lang_code
            )
        );
    }

    /**
     * This filters the check whether or not a job is assigned to a specific translator for local string jobs.
     * It is to be used after assigning a job, as it will update the assignment for local string jobs itself.
     * @param bool       $assigned_correctly
     * @param string|int $job_id
     * @param int        $translator_id
     * @param string|int $service
     * @return bool
     */
    public function job_assigned_to_filter( $assigned_correctly, $job_id, $translator_id, $service ) {
        if ( ( !$service || $service === 'local' ) && strpos( $job_id, 'string|' ) !== false ) {
            global $wpdb;
            $job_id = preg_replace( '/[^0-9]/', '', $job_id );
            $wpdb->update(
                $wpdb->prefix . 'icl_string_translations',
                array( 'translator_id' => $translator_id ),
                array( 'id' => $job_id )
            );
            $assigned_correctly = true;
        }

        return $assigned_correctly;
    }

    protected function get_element_trid( $element_id, $element_type_prefix ) {
        global $wpdb;
        return $wpdb->get_var(
            $wpdb->prepare(
                "SELECT trid FROM {$wpdb->prefix}icl_translations WHERE element_id = %d AND element_type LIKE %s",
                $element_id,
                $element_type_prefix . '%'
            )
        );
    }

    public function is_in_basket( $element_id, $lang, $element_type_prefix ) {
        return TranslationProxy_Basket::anywhere_in_basket(
            $element_id,
            $element_type_prefix,
            array( $lang => 1 )
        );
    }

    public function filter_target_langs( $allowed_langs, $element_id, $element_type_prefix ) {
        if ( TranslationProxy_Basket::anywhere_in_basket( $element_id, $element_type_prefix ) ) {
            $allowed_langs = array();
        } else {
            foreach ( $allowed_langs as $key => $lang_code ) {
                if ( $this->is_in_active_job( $element_id, $lang_code, $element_type_prefix ) ) {
                    unset( $allowed_langs[ $key ] );
                }
            }
        }

        return $allowed_langs;
    }

    private function get_element_ids( $trid, $element_type_prefix ) {
        global $wpdb;
        return $wpdb->get_col(
            $wpdb->prepare(
                "SELECT element_id FROM {$wpdb->prefix}icl_translations WHERE trid = %d AND element_type LIKE %s",
                $trid,
                $element_type_prefix . '%'
            )
        );
    }

	public function filter_translation_status( $status, $trid, $target_lang_code, $element_type_prefix ) {
		$element_ids = $this->get_element_ids ( $trid, $element_type_prefix );
		foreach ( $element_ids as $id ) {
			if ( $this->is_in_basket ( $id, $target_lang_code, $element_type_prefix ) ) {
				$status = ICL_TM_IN_BASKET;
				break;
			} elseif ( (bool) ( $job_status = $this->is_in_active_job (
					$id,
					$target_lang_code,
					$element_type_prefix,
					true
				))
			                                  !== false
			) {
				$status = $job_status;
			}
		}
		$status = $status != ICL_TM_IN_BASKET && $this->needs_update ( $trid, $target_lang_code ) ? ICL_TM_NEEDS_UPDATE
			: $status;

		return $status;
	}
}