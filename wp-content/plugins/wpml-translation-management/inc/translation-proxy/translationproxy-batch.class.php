<?php
/**
 * @package wpml-core
 * @subpackage wpml-core
 */
if ( ! class_exists( 'TranslationProxy_Batch' ) ) {
	class TranslationProxy_Batch {
		public $id;
		public $name;
		public $source_language;
		public $target_languages;

		/**
		 * return TranslationProxy_Batch
		 */
		public static function get_batch_data() {
			$basket = TranslationProxy_Basket::get_basket();

			return $basket;
		}

		/**
		 * returns the name of a generic batch
		 * name is built based on the current's date
		 *
		 * @return string
		 */
		public static function get_generic_batch_name() {
			$batch_name = 'Manual Translations from ' . date( 'F \t\h\e jS\, Y' );

			return $batch_name;
		}

		/**
		 * returns the id of a generic batch
		 *
		 * @return int
		 */
		private static function create_generic_batch() {
			$batch_name = self::get_generic_batch_name();
			$batch_id   = TranslationManagement::update_translation_batch( $batch_name );

			return $batch_id;
		}

		public static function maybe_assign_generic_batch( $data ) {
			global $wpdb;

			$batch_id = $wpdb->get_var( $wpdb->prepare( "SELECT batch_id
														 FROM {$wpdb->prefix}icl_translation_status
														 WHERE translation_id=%d",
			                                            $data[ 'translation_id' ] ) );

			//if the batch id is smaller than 1 we assign the translation to the generic manual translations batch for today if the translation_service is local
			if ( ($batch_id < 1 ) && isset( $data [ 'translation_service' ] ) && $data [ 'translation_service' ] == "local" ) {
				//first we retrieve the batch id for today's generic translation batch
				$batch_id = self::create_generic_batch();
				//then we update the entry in the icl_translation_status table accordingly
				$data_where = array( 'rid' => $data[ 'rid' ] );
				$wpdb->update( $wpdb->prefix . 'icl_translation_status',
				               array( 'batch_id' => $batch_id ),
				               $data_where );
			}
		}
	}
}