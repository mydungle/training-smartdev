<script type="text/html" id="table-listing-filter">
<?php
	global $iclTranslationManagement, $sitepress;

	$translation_services = array( 'local', TranslationProxy::get_current_service_id() );
?>
	<label>
		<strong><?php _e( 'Translation jobs for:', 'wpml-translation-management' ) ?></strong>&nbsp;
		<?php
		$args = array(
				'name'         => 'filter[translator_id]',
				'default_name' => __( 'All', 'wpml-translation-management' ),
				'selected'     => isset( $icl_translation_filter[ 'translator_id' ] ) ? $icl_translation_filter[ 'translator_id' ] : '',
				'services'     => $translation_services,
		);
		TranslationManagement::translators_dropdown( $args );
		?>
	</label>&nbsp;
	<label>
		<strong><?php _e( 'Status', 'wpml-translation-management' ) ?></strong>&nbsp;
		<select id="filter-job-status" name="filter[status]">
			<option value=""><?php _e( 'All translation jobs', 'wpml-translation-management' ) ?></option>
			<option value="<?php echo ICL_TM_WAITING_FOR_TRANSLATOR ?>" ><?php echo TranslationManagement::status2text( ICL_TM_WAITING_FOR_TRANSLATOR ); ?></option>
			<option value="<?php echo ICL_TM_IN_PROGRESS ?>"><?php echo TranslationManagement::status2text( ICL_TM_IN_PROGRESS ); ?></option>
			<option value="<?php echo ICL_TM_COMPLETE ?>" ><?php echo TranslationManagement::status2text( ICL_TM_COMPLETE ); ?></option>
			<option value="<?php echo ICL_TM_DUPLICATE ?>"><?php _e( 'Content duplication', 'wpml-translation-management' ) ?></option>
		</select>
	</label>&nbsp;
	<label>
		<strong><?php _e( 'From', 'wpml-translation-management' ); ?></strong>
		<select id="filter-job-lang-from" name="filter[from]">
			<option value=""><?php _e( 'Any language', 'wpml-translation-management' ) ?></option>
			<?php foreach ( $sitepress->get_active_languages() as $lang ): ?>
				<option value="<?php echo $lang[ 'code' ] ?>"><?php echo $lang[ 'display_name' ] ?></option>
			<?php endforeach; ?>
		</select>
	</label>&nbsp;
	<label>
		<strong><?php _e( 'To', 'wpml-translation-management' ); ?></strong>
		<select id="filter-job-lang-to" name="filter[to]">
			<option value=""><?php _e( 'Any language', 'wpml-translation-management' ) ?></option>
			<?php foreach ( $sitepress->get_active_languages() as $lang ): ?>
				<option value="<?php echo $lang[ 'code' ] ?>"><?php echo $lang[ 'display_name' ] ?></option>
			<?php endforeach; ?>
		</select>
	</label>
</script>
