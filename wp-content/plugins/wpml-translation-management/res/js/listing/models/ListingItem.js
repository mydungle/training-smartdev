Translation_Jobs.listing.models.ListingItem = Backbone.Model.extend({

    initialize: function () {
        var self = this;
    },
    get_type: function () {
        return this.get('type');
    },
    is_string: function () {
        return this.get_type() == 'String';
    },
    is_post: function () {
        return this.get_type() == 'Post';
    }
});