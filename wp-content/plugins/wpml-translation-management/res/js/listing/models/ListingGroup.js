Translation_Jobs.listing.models.ListingGroup = Backbone.Model.extend({
	initialize: function(options){
		var self = this;
	//	console.log( self.get('id'), self.get('name'), self.get('items') );
	},
	parse: function (data) {
		var self = this;

		data.items = new Translation_Jobs.listing.models.ListingItems(data.items, {parse:true});

		return data;
	}
});