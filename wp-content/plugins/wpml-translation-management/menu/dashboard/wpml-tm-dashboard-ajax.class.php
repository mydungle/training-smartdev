<?php

class WPML_Dashboard_Ajax {

	public function __construct() {
		add_action ( 'wpml_tm_scripts_enqueued', array( $this, 'enqueue_js' ) );
		add_action ( 'init', array( $this, 'init' ) );
	}

	public function init(){
		add_action ( 'wp_ajax_wpml_duplicate_dashboard', array( $this, 'wpml_duplicate_dashboard' ) );
	}

	public function enqueue_js() {
		wp_register_script (
			'wpml-tm-dashboard-scripts',
			WPML_TM_URL . '/res/js/wpml-tm-dashboard.js',
			array( 'jquery', 'backbone' ),
			WPML_TM_VERSION
		);
		$wpml_tm_strings = $this->get_wpml_tm_script_js_strings ();
		wp_localize_script ( 'wpml-tm-dashboard-scripts', 'wpml_tm_strings', $wpml_tm_strings );
		wp_enqueue_script ( 'wpml-tm-dashboard-scripts' );
	}

	private function get_wpml_tm_script_js_strings() {
		$wpml_tm_strings = array(
			'BB_default'                     => __ ( 'Add to translation basket', 'wpml-translation-management' ),
			'BB_mixed_actions'               => __ (
				'Add to translation basket / Duplicate',
				'wpml-translation-management'
			),
			'BB_duplicate_all'               => __ ( 'Duplicate', 'wpml-translation-management' ),
			'BB_no_actions'                  => __ (
				'Choose at least one translation action',
				'wpml-translation-management'
			),
			'duplication_complete'                  => __ (
				'Finished Post Duplication',
				'wpml-translation-management'
			),
			'wpml_duplicate_dashboard_nonce' => wp_create_nonce ( 'wpml_duplicate_dashboard_nonce' ),
			'duplicating' => __ ( 'Duplicating', 'wpml-translation-management' ),
		);

		return $wpml_tm_strings;
	}

	public function wpml_duplicate_dashboard() {
		if ( !wpml_is_action_authenticated ( 'wpml_duplicate_dashboard' ) ) {
			wp_send_json_error ( 'Wrong Nonce' );
		}

		global $sitepress;

		$post_ids  = filter_input ( INPUT_POST, 'duplicate_post_ids' );
		$languages = filter_input ( INPUT_POST, 'duplicate_target_languages' );

		$post_ids  = $post_ids !== null ? explode ( ',', $post_ids ) : array();
		$languages = $languages !== null ? explode ( ',', $languages ) : array();
		$res       = array();

		foreach ( $post_ids as $pid ) {
			foreach ( $languages as $lang_code ) {
				if ( $sitepress->make_duplicate ( $pid, $lang_code ) !== false ) {
					$res[ $lang_code ] = $pid;
				}
			}
		}

		wp_send_json_success ( $res );
	}
}

new WPML_Dashboard_Ajax();