<?php
require WPML_TM_PATH . '/menu/wpml-translation-editor.class.php';

/** @var TranslationManagement $iclTranslationManagement */
/** @var WPML_Post_Translation $wpml_post_translations */
global $wpdb, $post, $sitepress, $iclTranslationManagement, $current_user, $wp_version;

$job_id = WPML_Translation_Editor::get_job_id_from_request();
if ( $job_id ) {
	WPML_Translation_Job_Terms::set_translated_term_values( $job_id );
}
$job = $iclTranslationManagement->get_translation_job( $job_id, false, true, 1 );

if ( empty( $job ) || ( ( ( $job->translator_id != 0 && $job->translator_id != $current_user->ID )
                          || ( !$iclTranslationManagement->is_translator(
                $current_user->ID,
                array(
                    'lang_from' => $job->source_language_code,
                    'lang_to' => $job->language_code
                )
            ) ) ) && !current_user_can( 'manage_options' ) )
) {
    $job_checked = true;
    include WPML_TM_PATH . '/menu/translations-queue.php';
    return;
}

$rtl_original = $sitepress->is_rtl($job->source_language_code);
$rtl_translation = $sitepress->is_rtl($job->language_code);
$rtl_original_attribute = $rtl_original ? ' dir="rtl"' : ' dir="ltr"';
$rtl_translation_attribute = $rtl_translation ? ' dir="rtl"' : ' dir="ltr"';

require_once(ABSPATH . 'wp-admin/includes/image.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/media.php');

?>
<div class="wrap icl-translation-editor">
    <div id="icon-wpml" class="icon32"><br /></div>
    <h2><?php echo __('Translation editor', 'wpml-translation-management') ?></h2>    
    
    <?php do_action('icl_tm_messages'); ?>
    <?php
    // we do not need the original document of the job here
    // but the document with the same trid and in the $job->source_language_code
    $all_translations = $sitepress->get_element_translations( $job->trid, $job->original_post_type );
    $original_post = false;
    foreach ( $all_translations as $t ) {
	    if ( $t->language_code === $job->source_language_code ) {
		    $original_post = $iclTranslationManagement->get_post($t->element_id, $job->element_type_prefix );
            //if this fails for some reason use the original doc from which the trid originated
            $original_post = $original_post ? $original_post :
                $iclTranslationManagement->get_post($job->original_doc_id, $job->element_type_prefix );
		    break;
	    }
    }
    $is_external_element = $iclTranslationManagement->is_external_type($job->element_type_prefix);

    $editor_object = new WPML_Translation_Editor( $job->to_language, $job->from_language, $rtl_original_attribute, $rtl_translation_attribute );

    if ( ! empty( $original_post ) &&
         ( $original_post->post_status === 'draft' || $original_post->post_status === 'private' )
         && $original_post->post_author != $current_user->data->ID
    ) {
	    $elink1 = '<i>';
	    $elink2 = '</i>';
    } else {
	    $elink1 = sprintf( '<a href="%s">', get_permalink( $original_post->ID ) );
	    $elink2 = '</a>';
    }
    ?>
    <p class="updated fade">
	    <?php
	    $tm_post_link = TranslationManagement::tm_post_link( $original_post->ID );
	    if ( $is_external_element ) {
		    $tm_post_link = apply_filters( 'wpml_external_item_link', $tm_post_link, $job->original_doc_id, false );
	    }
	    printf( __( 'You are translating %s from %s to %s.', 'wpml-translation-management' ), $tm_post_link, $job->from_language, $job->to_language );
        echo '<p>' . $editor_object->render_copy_from_original_link() . '</p>';
	    ?>
    </p>

    <?php
    if ( $translators_note = get_post_meta( $original_post->ID, '_icl_translator_note', true ) ) {
	    ?>
	    <i><?php _e( 'Note for translator', 'wpml-translation-management' ); ?></i>
	    <br/>
	    <div class="icl_cyan_box">
		    <?php echo $translators_note ?>
	    </div>
    <?php
    }
    ?>
    
    <form id="icl_tm_editor" method="post" action="">
    <input type="hidden" name="icl_tm_action" value="save_translation" />
        <input type="hidden" name="job_id" value="<?php echo esc_attr( $job_id ) ?>"/>
        <div id="dashboard-widgets-wrap">
        <?php
        $icl_tm_all_finished = true;

        foreach ( $job->elements as $element ){
	        if ( !$element->field_data ) {
		        continue;
	        }

            if(preg_match('/^t_/', $element->field_type)){
                $editor_object->add_term($element);
                continue;
            }

            $_iter = !isset($_iter) ? 1 : $_iter + 1; 
            if(!$element->field_finished){
                $icl_tm_all_finished = false;
            }

            $is_custom_field = 0 === strpos($element->field_type, 'field-');

        ?>        
        <div class="metabox-holder" id="icl-translation-job-elements-<?php echo $_iter ?>">
            <div class="postbox-container icl-tj-postbox-container-<?php echo $element->field_type ?>">
                <div class="meta-box-sortables ui-sortable" id="icl-translation-job-sortables-<?php echo $_iter ?>">
                    <div class="postbox" id="icl-translation-job-element-<?php echo $_iter ?>">
                        <?php echo $editor_object->get_click_to_toggle_html(); ?>
                        <?php 
						// allow custom field names to be filtered
						if($is_custom_field){
							$element_field_type  = apply_filters('icl_editor_cf_name', $element->field_type);                                
							$element_field_style = 1;
							$element_field_style = apply_filters('icl_editor_cf_style', $element_field_style, $element->field_type);
							$element = apply_filters('wpml_editor_cf_to_display', $element, $job);
						} else if ( $is_external_element ) {
							// Get human readable string Title and editor style from the WPML string package.
							$element_field_type  = apply_filters('wpml_tm_editor_string_name', $element->field_type, $original_post );
							$element_field_style = $element->field_type;
							$element_field_style = apply_filters('wpml_tm_editor_string_style', $element_field_style, $element->field_type, $original_post);
						} else {
							$element_field_type = $element->field_type;
							$element_field_style = false;
						}                            
                        ?>
                        <?php echo $editor_object->get_post_box_header( $element_field_type ) ?>
                        <div class="inside">
                            <?php 
                                // allow custom field descriptions to be set/filtered
                                if($is_custom_field){
                                    $icl_editor_cf_description = apply_filters('icl_editor_cf_description', '', $element->field_type);    
                                    if($icl_editor_cf_description !== null){
                                        echo '<p class="icl_tm_field_description">' . $icl_editor_cf_description . '</p>';
                                    }
                                }
                            ?>
                            <?php /* TRANSLATED CONTENT */ ?>
                            <?php 
                                $icl_tm_original_content = TranslationManagement::decode_field_data($element->field_data, $element->field_format);
                                $icl_tm_translated_content = TranslationManagement::decode_field_data($element->field_data_translated, $element->field_format);
                            ?>
                            <?php echo $editor_object->get_translated_content_paragraph($element->field_type, true); ?>

                            <?php // CASE 1 - body *********************** ?>
                            <?php if($element->field_type=='body'): ?>
                            <div id="poststuff">
                                <?php
                                if ( is_null( $post ) && !is_null( $original_post ) ) {
                                    $post = clone $original_post;
                                }
                                $settings = array(
                                    'media_buttons' => false,
                                    'textarea_name' => 'fields[' . strtolower( $element->field_type ) . '][data]',
                                    'textarea_rows' => 20,
                                    'editor_css' => $rtl_translation
                                        ? ' <style type="text/css">.wp-editor-container textarea.wp-editor-area{direction:rtl;}</style>'
                                        : ''
                                );
                                wp_editor( $icl_tm_translated_content, strtolower( $element->field_type ), $settings );
                                ?>
                            </div>    
                                                                          
                            <?php // CASE 2 - csv_base64 *********************** ?>         
                            <?php elseif($element->field_format == 'csv_base64'): ?>
                            <?php foreach($icl_tm_original_content as $k=>$c): ?>
                            <label><input id="<?php echo sanitize_title($element->field_type)?>" class="icl_multiple" type="text" name="fields[<?php echo esc_attr($element->field_type)
                                ?>][data][<?php echo $k ?>]" value="<?php if(isset($icl_tm_translated_content[$k])) 
                                    echo esc_attr($icl_tm_translated_content[$k]); ?>"<?php echo $rtl_translation_attribute; ?> /></label>
                            <?php endforeach;?>
                            
                            <?php // CASE 3 - multiple lines *********************** ?>         
                            <?php elseif(($is_custom_field || $original_post) && $element_field_style == 1): ?>
                                <textarea id="<?php echo sanitize_title($element->field_type) ?>" style="width:100%;" name="fields[<?php echo esc_attr($element->field_type) ?>][data]"<?php
                                    echo $rtl_translation_attribute; ?>><?php echo esc_html($icl_tm_translated_content); ?></textarea>

                            <?php // CASE 4 - wysiwyg *********************** ?>
                            <?php elseif ( ( $is_custom_field || $original_post ) && $element_field_style == 2 ):
                                $settings = array(
                                    'media_buttons' => false,
                                    'textarea_name' => 'fields[' . $element->field_type . '][data]',
                                    'textarea_rows' => 4
                                );
                                wp_editor( $icl_tm_translated_content, $element->field_type, $settings );
                                ?>
                            <?php // CASE 5 - one-liner *********************** ?>         
                            <?php else: ?>
                            <label><input id="<?php echo sanitize_title($element->field_type) ?>" type="text" name="fields[<?php echo esc_attr($element->field_type) ?>][data]" value="<?php
                                echo esc_attr($icl_tm_translated_content); ?>"<?php echo $rtl_translation_attribute; ?> /></label>
                            <?php endif; ?> 
                            <?php
                            $multiple = $element->field_format == 'csv_base64';
                            echo $editor_object->get_finished_checkbox_html($element->field_finished,$multiple, $element->field_type );
                            ?>

                            <br />                                                            
                            <?php /* TRANSLATED CONTENT */ ?>
                            
                            <?php /* ORIGINAL CONTENT */ ?>
                            <?php echo $editor_object->get_original_content_paragraph() ?>
                            <?php
                            $icl_wysiwyg_height = $element->field_type == 'body' ? get_option('default_post_edit_rows', 20)*20 : 100;
                            if($element->field_type=='body' || $element_field_style == 2){
                                    $icl_tm_original_content_html = esc_html($icl_tm_original_content);
                                    $icl_tm_original_content = apply_filters('the_content', $icl_tm_original_content);
                                }
                            ?>
                            <div class="icl-tj-original<?php if($is_custom_field) :?> icl-tj-original-cf<?php endif; ?>" >                                                                
                                <?php if($element->field_type=='body' || $element_field_style == 2): ?>
                                <div class="icl_single visual"<?php echo $rtl_original_attribute; ?>>
                                    <?php
                                    $settings = array(
                                        'media_buttons' => false,
                                        'textarea_name' => 'fields[' . strtolower( $element->field_type ) . '][original]',
                                        'textarea_rows' => 20,
                                        'editor_css' => $rtl_translation
                                            ? ' <style type="text/css">.wp-editor-container textarea.wp-editor-area{direction:rtl;}</style>'
                                            : ''
                                    );
                                    wp_editor(
                                        $icl_tm_original_content,
                                        'original_' . strtolower( $element->field_type ),
                                        $settings
                                    );
                                    ?>
                                <br clear="all"/></div>
                                <?php elseif($element->field_format == 'csv_base64'): ?>
                                <?php foreach($icl_tm_original_content as $c): ?>
                                        <div class="icl_multiple"<?php echo $rtl_original_attribute; ?>>
                                            <div style="float: left;margin-right:4px;"><?php echo $c ?></div>
                                            <?php if(isset($term_descriptions[$c])) icl_pop_info($term_descriptions[$c], 'info', array('icon_size'=>10)); ?>
                                            <br clear="all"/>
                                        </div>
                                    <?php endforeach;?>
                                <?php else: ?>
                                <div class="icl_single"<?php if ($rtl_original) echo ' dir="rtl" style="text-align:right;"'; else echo ' dir="ltr" style="text-align:left;"'; ?>><span style="white-space:pre-wrap;" id="icl_tm_original_<?php echo sanitize_title($element->field_type) ?>"><?php echo esc_html($icl_tm_original_content) ?></span><br clear="all"/></div>
                                <?php endif; ?>
                            </div>
                            <?php /* ORIGINAL CONTENT */ ?>
                            
                            <input type="hidden" name="fields[<?php echo esc_attr($element->field_type) ?>][format]" value="<?php echo $element->field_format ?>" />
                            <input type="hidden" name="fields[<?php echo esc_attr($element->field_type) ?>][tid]" value="<?php echo $element->tid ?>" />
                            
                            <?php if(!$element->field_finished && !empty($job->prev_version)): ?>                            
                                <?php 
                                    $prev_value = '';
                                    foreach($job->prev_version->elements as $pel){
                                        if($element->field_type == $pel->field_type){
                                            $prev_value = TranslationManagement::decode_field_data($pel->field_data, $pel->field_format);
                                        }    
                                    }
                                    if($element->field_format != 'csv_base64'){
                                        $diff = wp_text_diff( $prev_value, TranslationManagement::decode_field_data($element->field_data, $element->field_format) );  
                                    }
                                    if(!empty($diff)){
                                        ?>
                                        <p><a href="#" onclick="jQuery(this).parent().next().slideToggle();return false;"><?php 
                                            _e('Show Changes', 'sitepress'); ?></a></p>
                                        <div class="icl_tm_diff">
                                            <?php echo $diff ?>
                                        </div>
                                        <?php 
                                    }
                                ?>
                            <?php endif;?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }

            $html = $editor_object->render_term_metaboxes();
            echo $html;
        ?>
    </div>
    
    <br clear="all" />
    <label><input type="checkbox" name="complete" <?php if(!$icl_tm_all_finished): ?>disabled="disabled"<?php endif; ?> <?php 
    if($job->translated):?> checked="checked"<?php endif; ?> value="1" />&nbsp;<?php 
        _e('Translation of this document is complete', 'wpml-translation-management')?></label>
    
    <div id="icl_tm_validation_error" class="icl_error_text"><?php _e('Please review the document translation and fill in all the required fields.', 'wpml-translation-management') ?></div>
    <p class="submit-buttons">
        <input type="submit" class="button-primary" value="<?php _e('Save translation', 'wpml-translation-management')?>" />&nbsp;
        <?php
        if (isset($_POST['complete']) && $_POST['complete']) {
            $cancel_txt = __('Jobs queue', 'wpml-translation-management');
        } else {
            $cancel_txt = __('Cancel', 'wpml-translation-management');
        }
        ?>
        <a class="button-secondary" href="<?php echo admin_url('admin.php?page='.WPML_TM_FOLDER.'/menu/translations-queue.php') ?>"><?php echo $cancel_txt; ?></a>
        <input type="submit" id="icl_tm_resign" class="button-secondary" value="<?php _e('Resign', 'wpml-translation-management')?>" onclick="if(confirm('<?php echo esc_js(__('Are you sure you want to resign from this job?', 'wpml-translation-management')) ?>')) jQuery(this).next().val(1); else return false;" /><input type="hidden" name="resign" value="0" />
    </p>
    <?php do_action('edit_form_advanced'); ?>
    <?php wp_nonce_field('icl_get_job_original_field_content_nonce', 'icl_get_job_original_field_content_nonce') ?>
    </form>
        
</div>    
