<?php
/*
Plugin Name: WPML Translation Management
Plugin URI: https://wpml.org/
Description: Add a complete translation process for WPML. <a href="https://wpml.org">Documentation</a>.
Author: OnTheGoSystems
Author URI: http://www.onthegosystems.com/
Version: 2.0-RC3
*/

if ( defined( 'WPML_TM_VERSION' ) ) {
	return;
}

define( 'WPML_TM_VERSION', '2.0' );
define( 'WPML_TM_DEV_VERSION', '2.0-RC3' );
define( 'WPML_TM_PATH', dirname( __FILE__ ) );

require WPML_TM_PATH . '/inc/constants.php';
require WPML_TM_PATH . '/inc/translation-proxy/interfaces/TranslationProxy_Interface.php';
require WPML_TM_PATH . '/inc/translation-proxy/wpml-pro-translation.class.php';
require WPML_TM_PATH . '/inc/ajax.php';
require WPML_TM_PATH . '/inc/wpml-translation-management.class.php';
require WPML_TM_PATH . '/inc/wpml-translation-management-xliff.class.php';
require WPML_TM_PATH . '/inc/translation-proxy/translationproxy.class.php';
require WPML_TM_PATH . '/inc/functions-load.php';
wpml_tm_init_mail_notifications();

global $WPML_Translation_Management;
$WPML_Translation_Management = new WPML_Translation_Management();
require WPML_TM_PATH . '/inc/filters/wpml-tm-translation-status.class.php';
require WPML_TM_PATH . '/inc/filters/wpml-tm-translation-status-display.class.php';
$tm_status = new WPML_TM_Translation_Status();
add_action( 'wpml_loaded', array( $tm_status, 'init' ) );
add_action( 'wpml_pre_status_icon_display', 'wpml_tm_load_status_display_filter' );
