<?php
/*
Plugin Name: WPML String Translation
Plugin URI: https://wpml.org/
Description: Adds theme and plugins localization capabilities to WPML. <a href="https://wpml.org">Documentation</a>.
Author: OnTheGoSystems
Author URI: http://www.onthegosystems.com/
Version: 2.2-RC3
*/

if ( defined( 'WPML_ST_VERSION' ) ) {
	return;
}

define( 'WPML_ST_VERSION', '2.2' );
//define( 'WPML_PT_VERSION_DEV', '0.0.2' );
define( 'WPML_ST_PATH', dirname( __FILE__ ) );

require WPML_ST_PATH . '/inc/wpml-string-translation.class.php';
require WPML_ST_PATH . '/inc/constants.php';

global $WPML_String_Translation;
$WPML_String_Translation = new WPML_String_Translation();

require WPML_ST_PATH . '/inc/package-translation/wpml-package-translation.php';