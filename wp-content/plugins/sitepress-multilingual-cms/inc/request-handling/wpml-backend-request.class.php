<?php

/**
 * Class WPML_Backend_Request
 *
 * @package    wpml-core
 * @subpackage wpml-requests
 */
class WPML_Backend_Request extends WPML_Request {

	public function __construct( $active_languages, $default_language ) {
		parent::__construct ( $active_languages, $default_language );
		global $wpml_url_filters;

		if ( strpos ( (string) filter_var ( $_SERVER[ 'REQUEST_URI' ] ), 'wpml_root_page=1' ) !== false
		     || $wpml_url_filters->frontend_uses_root () !== false
		) {
			require_once ICL_PLUGIN_PATH . '/inc/url-handling/wpml-root-page.class.php';
		}
	}

	function check_if_admin_action_from_referer() {
		$referer = isset( $_SERVER[ 'HTTP_REFERER' ] ) ? $_SERVER[ 'HTTP_REFERER' ] : '';

		return strpos ( $referer, strtolower ( '/wp-admin/' ) ) !== false;
	}

	public function force_default(){
		return isset( $_GET[ 'page' ] )
		       && ( ( defined( 'WPML_ST_FOLDER' )
		              && $_GET[ 'page' ] === WPML_ST_FOLDER . '/menu/string-translation.php' )
		            || ( defined( 'WPML_TM_FOLDER' )
		                 && $_GET[ 'page' ] === WPML_TM_FOLDER . '/menu/translations-queue.php' ) );
	}

	/**
	 * Gets the source_language $_GET parameter from the HTTP_REFERER
	 *
	 * @return string|bool
	 */
	public function get_source_language_from_referer() {
		$referer = isset( $_SERVER[ 'HTTP_REFERER' ] ) ? $_SERVER[ 'HTTP_REFERER' ] : '';
		$query   = parse_url ( $referer, PHP_URL_QUERY );
		parse_str ( $query, $query_parts );
		$source_lang = isset( $query_parts[ 'source_lang' ] ) ? $query_parts[ 'source_lang' ] : false;

		return $source_lang;
	}

	public function get_post_edit_lang(){

		return filter_input(INPUT_POST, 'action') === 'editpost'
			? filter_input(INPUT_POST, 'icl_post_language') : null;
	}

	public function get_ajax_request_lang() {
		global $wpml_url_converter;

		$al   = $this->active_languages;
		$lang = isset( $_POST[ 'lang' ] ) && isset( $al[ $_POST[ 'lang' ] ] ) ? $_POST[ 'lang' ] : null;
		$lang = $lang === null && $this->check_if_admin_action_from_referer ()
			? ( $cookie_lang = $this->get_cookie_lang () ) : $lang;
		$lang = $lang === null && isset( $_SERVER[ 'HTTP_REFERER' ] )
			? $wpml_url_converter->get_language_from_url ( $_SERVER[ 'HTTP_REFERER' ] ) : $lang;
		$lang = $lang ? $lang : ( isset( $cookie_lang ) ? $cookie_lang : $this->get_cookie_lang () );
		$lang = $lang ? $lang : $this->default_language;

		return $lang;
	}

	public function get_requested_lang() {
		global $wpml_language_resolution;

		if (
			( $lang_code_get = filter_input ( INPUT_GET, 'lang', FILTER_SANITIZE_FULL_SPECIAL_CHARS ) ) !== null
			&& $lang_code_get === 'all' || $wpml_language_resolution->is_language_active ( $lang_code_get )
		) {
			$lang = $lang_code_get;
		} elseif ( $this->force_default () === true ) {
			$lang = $this->default_language;
		} elseif ( wpml_is_ajax () ) {
			$lang = $this->get_ajax_request_lang ();
		} elseif ( null !== ( $icl_p_lang = $this->get_post_edit_lang () ) ) {
			$lang = $icl_p_lang;
		} elseif ( ( $p = filter_input ( INPUT_GET, 'p', FILTER_SANITIZE_NUMBER_INT ) ) !== null ) {
			global $wpml_post_translations;
			$lang = $wpml_post_translations->get_element_lang_code ( $p );
		}
		$lang = !( isset( $lang ) && (bool) $lang === true ) ? $this->get_cookie_lang () : $lang;
		if ( empty( $lang ) && isset( $_SERVER[ "REQUEST_URI" ] ) ) {
			global $wpml_url_converter;

			$lang = $wpml_url_converter->get_language_from_url ( $_SERVER[ "REQUEST_URI" ] );
		}

		return $lang;
	}
}