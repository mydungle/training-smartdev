<?php

require_once ICL_PLUGIN_PATH . '/inc/url-handling/wpml-root-page.class.php';
require_once ICL_PLUGIN_PATH . '/inc/request-handling/redirection/wpml-redirect-by-subdir.class.php';

class WPML_Rootpage_Redirect_By_Subdir extends WPML_Redirect_By_Subdir {

	private $urls;
	private $request_uri;

	public function __construct( $urls, $request_uri, $requested_domain ) {
		$this->urls        = $urls;
		$this->request_uri = $request_uri;
	}

	/**
	 * @param $url
	 * Filters links to the root page, so that they are displayed properly in the front-end.
	 *
	 * @return mixed
	 */
	private function filter_root_permalink( $url ) {
		global $wpml_url_converter;

		if ( WPML_Root_Page::get_root_id () > 0 && WPML_Root_Page::is_root_page ( $url ) ) {

			$url_parts = parse_url ( $url );
			$query     = isset( $url_parts[ 'query' ] ) ? $url_parts[ 'query' ] : '';
			$path      = isset( $url_parts[ 'path' ] ) ? $url_parts[ 'path' ] : '';
			$slugs     = explode ( '/', $path );
			$last_slug = array_pop ( $slugs );

			list( $new_url, $query ) = $this->filter_pagination_on_root_preview (
				$last_slug,
				$query,
				$wpml_url_converter->get_abs_home ()
			);
			$query = self::unset_page_query_vars ( $query );
			$new_url = trailingslashit ( $new_url );
			$url = (bool) $query === true ? trailingslashit ( $new_url ) . '?' . $query : $new_url;
		}

		return $url;
	}

	private function unset_page_query_vars( $query ) {
		parse_str ( (string) $query, $query_parts );
		foreach ( array( 'p', 'page_id', 'page', 'pagename', 'page_name', 'attachement_id' ) as $part ) {
			if ( isset( $query_parts[ $part ] ) && !( $part === 'page_id' && !empty( $query_parts[ 'preview' ] ) ) ) {
				unset( $query_parts[ $part ] );
			}
		}

		return http_build_query ( $query_parts );
	}

	/**
	 * @param $last_slug string
	 * @param $query string
	 * @param $new_url string
	 *
	 * @return array
	 */
	private function filter_pagination_on_root_preview( $last_slug, $query, $new_url ) {

		if ( $query === 'preview=true'
		     || is_numeric ( $last_slug )
		        && strpos ( $query, 'preview_id=' . WPML_Root_Page::get_root_id () )
		) {
			$new_url = str_replace ( '/' . $last_slug . '/', '/', $new_url );
			$query .= '&page_id=' . WPML_Root_Page::get_root_id ();
		}

		return array( $new_url, $query );
	}


	public function get_redirect_target() {

		$target = parent::get_redirect_target ();
		$target = $target
			? $target
			: ( ( $filtered_root_url = $this->filter_root_permalink (
				wpml_strip_subdir_from_url ( site_url () ) . $this->request_uri
			) ) !== wpml_strip_subdir_from_url ( site_url () ) . $this->request_uri ? $filtered_root_url : false );

		if ( $target === false ) {
			$this->maybe_setup_rootpage ();
		}

		return $target;
	}

	private function maybe_setup_rootpage() {
		if ( WPML_Root_Page::is_current_request_root () ) {
			if ( WPML_Root_Page::uses_html_root () ) {
				$html_file = ( false === strpos ( $this->urls[ 'urls' ][ 'root_html_file_path' ], '/' ) ? ABSPATH : '' )
				             . $this->urls[ 'urls' ][ 'root_html_file_path' ];

				/** @noinspection PhpIncludeInspection */
				include $html_file;
				exit;
			} else {
				$root_page_actions = wpml_get_root_page_actions_obj ();
				$root_page_actions->wpml_home_url_setup_root_page ();
			}
		}
	}
}