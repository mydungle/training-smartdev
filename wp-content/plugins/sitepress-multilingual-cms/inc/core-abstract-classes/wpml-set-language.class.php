<?php

class WPML_Set_Language {
	/**
	 * @param int           $el_id
	 * @param string        $el_type
	 * @param int|bool|null $trid Trid the element is to be assigned to. Input that is == false will cause the term to
	 *                            be assigned a new trid and potential translation relations to/from it to disappear.
	 * @param string        $language_code
	 * @param null|string   $src_language_code
	 * @param bool          $check_duplicates
	 *
	 * @return bool|int|null|string
	 */
	public function set_element_language_details(
		$el_id,
		$el_type = 'post_post',
		$trid,
		$language_code,
		$src_language_code = null,
		$check_duplicates = true
	) {
		global $wpdb;
		// special case for posts and taxonomies
		// check if a different record exists for the same ID
		// if it exists don't save the new element and get out
		if ( $check_duplicates
		     && $el_id
		     && $this->check_duplicate ( $el_type, $el_id ) === true ) {
			trigger_error ( 'Element ID already exists with a different type', E_USER_NOTICE );
			return false;
		}
		if ( $trid ) { // it's a translation of an existing element
			// check whether we have an orphan translation - the same trid and language but a different element id
			$translation_id = $wpdb->get_var (
				$wpdb->prepare (
					"SELECT translation_id
					 FROM {$wpdb->prefix}icl_translations
					 WHERE   trid = %d
						AND language_code = %s
						AND element_id <> %d
						AND source_language_code IS NOT NULL
					",
					$trid,
					$language_code,
					$el_id
				)
			);

			if ( $translation_id ) {
				$wpdb->query (
					$wpdb->prepare (
						"DELETE FROM {$wpdb->prefix}icl_translations WHERE translation_id=%d",
						$translation_id
					)
				);
				icl_cache_clear ();
			}

			if ( !is_null ( $el_id ) && $translation_id = $wpdb->get_var (
					$wpdb->prepare (
						"SELECT translation_id
                                     FROM {$wpdb->prefix}icl_translations
                                     WHERE element_type= %s AND element_id= %d
                                      AND trid = %d",
						$el_type,
						$el_id,
						$trid
					)
				)
			) {
				//case of language change
				$wpdb->update (
					$wpdb->prefix . 'icl_translations',
					array( 'language_code' => $language_code ),
					array( 'translation_id' => $translation_id )
				);
			} elseif ( !is_null ( $el_id ) && $translation_id = $wpdb->get_var (
					$wpdb->prepare (
						  "SELECT translation_id
	                       FROM {$wpdb->prefix}icl_translations
	                       WHERE element_type= %s
	                        AND element_id= %d",
						$el_type,
						$el_id
					)
				)
			) {
				//case of changing the "translation of"
				if ( empty( $src_language_code ) ) {
					$src_language_code = $wpdb->get_var (
						$wpdb->prepare (
							 "SELECT language_code
	                          FROM {$wpdb->prefix}icl_translations
	                          WHERE trid = %d
	                            AND source_language_code IS NULL
	                          LIMIT 1",
							$trid
						)
					);
				}
				$wpdb->update (
					$wpdb->prefix . 'icl_translations',
					array(
						'trid'                 => $trid,
						'language_code'        => $language_code,
						'source_language_code' => $src_language_code
					),
					array( 'element_type' => $el_type, 'element_id' => $el_id )
				);
				icl_cache_clear ();
			} elseif ( ( $translation_id = $wpdb->get_var (
				$wpdb->prepare (
						"SELECT translation_id
						 FROM {$wpdb->prefix}icl_translations
						 WHERE trid=%d
							AND language_code=%s
							AND element_id IS NULL",
					$trid,
					$language_code
				) ) ) ) {
				$wpdb->update (
					$wpdb->prefix . 'icl_translations',
					array( 'element_id' => $el_id ),
					array( 'translation_id' => $translation_id )
				);
			} else {
				//get source
				if ( empty( $src_language_code ) ) {
					$src_language_code = $wpdb->get_var (
						$wpdb->prepare (
							" SELECT language_code
	                          FROM {$wpdb->prefix}icl_translations
	                          WHERE trid = %d
	                            AND source_language_code IS NULL
	                          LIMIT 1",
							$trid
						)
					);
				}

				// case of adding a new language
				$new = array(
					'trid'                 => $trid,
					'element_type'         => $el_type,
					'language_code'        => $language_code,
					'source_language_code' => $src_language_code
				);
				if ( $el_id ) {
					$new[ 'element_id' ] = $el_id;
				}
				$wpdb->insert ( $wpdb->prefix . 'icl_translations', $new );
				$translation_id = $wpdb->insert_id;
				icl_cache_clear ();

			}
		} else { // it's a new element or we are removing it from a trid
			$wpdb->query (
				$wpdb->prepare (
					"DELETE FROM {$wpdb->prefix}icl_translations
					 WHERE element_type = %s
				      AND element_id = %d",
					$el_type,
					$el_id ) );


			$trid = 1 + $wpdb->get_var ( "SELECT MAX(trid) FROM {$wpdb->prefix}icl_translations" );

			$new = array(
				'trid'          => $trid,
				'element_type'  => $el_type,
				'language_code' => $language_code
			);
			if ( $el_id ) {
				$new[ 'element_id' ] = $el_id;
			}

			$wpdb->insert ( $wpdb->prefix . 'icl_translations', $new );
			$translation_id = $wpdb->insert_id;
		}

		if ( $translation_id && substr ( $el_type, 0, 4 ) === 'tax_' ) {
			$taxonomy = substr ( $el_type, 4 );
			do_action ( 'created_term_translation', $taxonomy, $el_id, $language_code );
		}

		do_action ( 'icl_set_element_language', $translation_id, $el_id, $language_code, $trid );
		icl_cache_clear ();

		return $translation_id;
	}

	public function repair_broken_assignments(){
		$this->fix_missing_original();
		$this->fix_wrong_source_language();
	}

	private function check_duplicate($el_type, $el_id){
		global $wpdb;

		$res   = false;
		$exp   = explode ( '_', $el_type );
		$_type = $exp[ 0 ];
		if ( in_array ( $_type, array( 'post', 'tax' ) ) ) {
			$_el_exists = $wpdb->get_var (
				$wpdb->prepare (
					"SELECT translation_id
                        FROM {$wpdb->prefix}icl_translations
                        WHERE element_id = %d
                          AND element_type <> %s
                          AND element_type LIKE %s",
					$el_id,
					$el_type,
					$_type . '%'
				)
			);
			if ( $_el_exists ) {
				$res = true;
			}
		}

		return $res;
	}

	private function fix_wrong_source_language(){
		global $wpdb;

		$wpdb->query( "	UPDATE {$wpdb->prefix}icl_translations
							SET source_language_code = NULL
						WHERE source_language_code = ''
							OR source_language_code = language_code" );

	}

	private function fix_missing_original(){
		global $wpdb;

		$broken_elements = $wpdb->get_results(
									   "SELECT MIN(iclt.element_id) AS element_id, iclt.trid
										FROM {$wpdb->prefix}icl_translations iclt
										LEFT JOIN {$wpdb->prefix}icl_translations iclo
											ON iclt.trid = iclo.trid
											AND iclo.source_language_code IS NULL
										WHERE iclo.translation_id IS NULL
										GROUP BY iclt.trid" );

		foreach ( $broken_elements as $element ) {
			$wpdb->query (
				$wpdb->prepare (
					"UPDATE {$wpdb->prefix}icl_translations
					SET source_language_code = NULL
					WHERE trid = %d AND element_id = %d",
					$element->trid,
					$element->element_id
				)
			);
		}
	}
}