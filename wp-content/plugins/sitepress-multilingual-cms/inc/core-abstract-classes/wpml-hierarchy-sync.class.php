<?php

abstract class WPML_Hierarchy_Sync {

	protected $original_elements_table_alias            = 'org';
	protected $translated_elements_table_alias          = 'tra';
	protected $original_elements_language_table_alias   = 'iclo';
	protected $translated_elements_language_table_alias = 'iclt';
	protected $correct_parent_table_alias               = 'corr';
	protected $correct_parent_language_table_alias      = 'iclc';
	protected $original_parent_table_alias              = 'parents';
	protected $original_parent_language_table_alias     = 'parent_lang';
	protected $element_id_column;
	protected $parent_element_id_column;
	protected $parent_id_column;
	protected $element_type_column;
	protected $element_type_prefix;
	protected $elements_table;
	protected $lang_info_table;

	public function __construct() {
		global $wpdb;

		$this->lang_info_table = $wpdb->prefix . 'icl_translations';
	}

	public function get_unsynced_elements( $element_types, $ref_lang_code = false ) {
		global $wpdb;

		$element_types = (array) $element_types;

		return !empty( $element_types ) ? $wpdb->get_results(
			$this->get_select_statement() . "
	            FROM " . $this->get_source_element_table()
			. $this->get_source_element_join()
			. $this->get_join_translation_language_data( $ref_lang_code )
			. $this->get_translated_element_join()
			. $this->get_original_parent_join()
			. $this->get_original_parent_language_join()
			. $this->get_correct_parent_language_join()
			. $this->get_correct_parent_element_join()
			. $this->get_where_statement( $element_types, $ref_lang_code )
		) : array();
	}

	public function sync_element_hierarchy( $element_types, $ref_lang_code = false ) {
		$unsynced = $this->get_unsynced_elements( $element_types, $ref_lang_code );

		foreach ( $unsynced as $row ) {
			$this->sync_row( $row );
		}
	}

	private final function sync_row( $row ) {
		global $wpdb;

		$wpdb->update(
			$this->elements_table,
			array( $this->parent_id_column => (int) $row->correct_parent ),
			array( $this->element_id_column => $row->translated_id )
		);
	}

	private final function get_source_element_join() {

		return "JOIN {$this->lang_info_table} {$this->original_elements_language_table_alias}
					ON {$this->original_elements_table_alias}.{$this->element_id_column}
						= {$this->original_elements_language_table_alias}.element_id
	                    AND {$this->original_elements_language_table_alias}.element_type
	                        = CONCAT('{$this->element_type_prefix}', {$this->original_elements_table_alias}.{$this->element_type_column})";
	}

	private final function get_translated_element_join() {

		return "JOIN {$this->elements_table} {$this->translated_elements_table_alias}
					ON {$this->translated_elements_table_alias}.{$this->element_id_column}
					= {$this->translated_elements_language_table_alias}.element_id ";
	}

	private final function get_source_element_table() {

		return " {$this->elements_table} {$this->original_elements_table_alias} ";
	}

	private function get_join_translation_language_data($ref_language_code) {

		$res = " JOIN {$this->lang_info_table} {$this->translated_elements_language_table_alias}
	               ON {$this->translated_elements_language_table_alias}.trid
	                 = {$this->original_elements_language_table_alias}.trid ";
		if ( (bool) $ref_language_code === true ) {
			$res .= "AND {$this->translated_elements_language_table_alias}.language_code
						!= {$this->original_elements_language_table_alias}.language_code ";
		} else {
			$res .= " AND {$this->translated_elements_language_table_alias}.source_language_code
                         = {$this->original_elements_language_table_alias}.language_code ";
		}

		return $res;
	}

	private function get_select_statement() {

		return " SELECT {$this->translated_elements_table_alias}.{$this->element_id_column} AS translated_id
						 , IFNULL({$this->correct_parent_table_alias}.{$this->parent_element_id_column}, 0) AS correct_parent ";
	}

	private final function get_original_parent_join() {

		return " LEFT JOIN {$this->elements_table} {$this->original_parent_table_alias}
	                ON {$this->original_parent_table_alias}.{$this->parent_element_id_column}
	                    = {$this->original_elements_table_alias}.{$this->parent_id_column} ";
	}

	private final function get_original_parent_language_join() {

		return " LEFT JOIN {$this->lang_info_table} {$this->original_parent_language_table_alias}
	               ON {$this->original_parent_table_alias}.{$this->element_id_column}
	                = {$this->original_parent_language_table_alias}.element_id
	                 AND {$this->original_parent_language_table_alias}.element_type
	                     = CONCAT('{$this->element_type_prefix}', {$this->original_parent_table_alias}.{$this->element_type_column}) ";
	}

	private final function get_correct_parent_language_join() {

		return " LEFT JOIN {$this->lang_info_table} {$this->correct_parent_language_table_alias}
	              ON {$this->correct_parent_language_table_alias}.language_code
	                    = {$this->translated_elements_language_table_alias}.language_code
	                AND {$this->original_parent_language_table_alias}.trid
	                    = {$this->correct_parent_language_table_alias}.trid ";
	}

	private final function get_correct_parent_element_join() {

		return " LEFT JOIN {$this->elements_table} {$this->correct_parent_table_alias}
	              ON {$this->correct_parent_table_alias}.{$this->element_id_column}
	                = {$this->correct_parent_language_table_alias}.element_id ";
	}

	private final function get_where_statement( $element_types, $ref_lang_code ) {
		global $wpdb;

		$filter_originals_snippet = $ref_lang_code
			? $wpdb->prepare( " AND {$this->original_elements_language_table_alias}.language_code = %s ", $ref_lang_code)
			:  " AND {$this->translated_elements_language_table_alias}.source_language_code IS NOT NULL ";

		return " WHERE {$this->original_elements_table_alias}.{$this->element_type_column}
					IN (" . wpml_prepare_in( $element_types ) . ")
                    AND IFNULL({$this->correct_parent_table_alias}.{$this->parent_element_id_column}, 0)
                        != {$this->translated_elements_table_alias}.{$this->parent_id_column} " . $filter_originals_snippet;
	}
}