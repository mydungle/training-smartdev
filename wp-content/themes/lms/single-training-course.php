<?php
    
    /*
     * Template of Training course in English and Vietnamese.
     */
?>
<?php
    get_header();
    // Generating Registe form in popup
    addDialog ();
    // Define texts translated by String Translations
    $curr_cert_course_category = __('certified-courses', 'dt_themes');
    $curr_non_cert_course_category = __('non-certified-courses', 'dt_themes');
    $text_course_information = __('Course information', 'dt_themes');
    $text_location = __('Location', 'dt_themes');
    $text_duration = __('Duration', 'dt_themes');
    $text_date = __('Date', 'dt_themes');
    $curr_course_category = __('certified-course-catalog', 'dt_themes');
    $text_download_course = __('Download this course', 'dt_themes');
    $text_language = __('Language', 'dt_themes');
    $text_batch_size = __('Batch size', 'dt_themes');
    $text_hosted_by = __('Hosted by', 'dt_themes');
    $other_certificated_courses = __('Other Certified Courses', 'dt_themes');
    $text_unit_price = __('Unit price', 'dt_themes');
    $text_excluded_tax = __('Excluded tax', 'dt_themes');
    $text_register = __('Register', 'dt_themes');
    $text_venue = __('Venue', 'dt_themes');
    $text_title_slide_image_detail_course = __('Some photos from this course', 'dt_themes');
    $other_non_certificated_courses = __('Other Non-certified Courses', 'dt_themes');
    // Get id of the current post
    if( have_posts() ): while( have_posts() ): the_post();
    $post_id = get_the_ID();
    break;
    endwhile; endif;
    // Get id of the current post
    if ($post_id <= 0) $post_id = $post->ID;
    $course_title = $post->post_title;
    $custom_fields = get_post_custom ( $post_id );
    // This is flag of empty course information. It's true once it's empty, a message will be displayed in this case.
    $empty_course_info_flag = true;
    
    // Get basic course information form catalog if this course belongs to an available course catalog
    $catalog_id = $custom_fields[CUSTOM_FIELD_COURSE_CATALOG_ID];
    if (is_array($catalog_id)) {
        $catalog_id = (int) $catalog_id[0];
    } else {
        $catalog_id =  0;
    }
    // If the course belongs to at least one course catalog, getting the catalog information
    if ($catalog_id > 0) {
        // Getting basic course information from the course catalog
        $catalog = get_post_meta($catalog_id);
        $basic_catalog_info = get_post($catalog_id);
        $content = apply_filters ("the_content", $basic_catalog_info->post_content);
        $catalog_content = $content;
        // Get heading 1
        $heading1 = $catalog ['_heading_1'] [0];
        // Get trainer
        $trainer_id = $catalog [CUSTOM_FIELD_COURSE_TRAINER] [0];
        $trainer_id = icl_object_id($trainer_id, 'page', false, ICL_LANGUAGE_CODE);
        $trainer_name = get_the_title($trainer_id);
        $trainer_profile = get_the_permalink($trainer_id);
        // Get languages
        $languages = $catalog ['_languages'] [0];
        $languages = unserialize ( $languages );
        $languages_arr = array();
        if (is_array ( $languages )) {
            foreach($languages as $ll) {
                if ($ll != DEFAULT_VALUE_SELECT_NONE_VALUE) $languages_arr[] = $ll;
            }
            $languages = $languages_arr;
            if (count($languages) > 0) {
                $languages_list = implode ( ', ', $languages );
            } else {
                $languages_list = '';
            }
            if (count ( $languages ) == 2) {
                $languages_list = implode ( ' and ', $languages );
            } else {
                $languages_list = implode ( ', ', $languages );
            }
        } else {
            $languages_list = '';
        }
        // Get batch size
        $size = $catalog ['_batch_size'] [0];
         // Get hosted by value
        $hosted_by = $catalog['_hosted_by'];
        if (!is_array($hosted_by)) $hosted_by = '';
        else $hosted_by = $hosted_by[0];
        // Get Register url of Epicoaching course
        $epicoaching_reg_url = $catalog['_epicoaching_register_url'];
        if ($epicoaching_reg_url) {
            $epicoaching_reg_url = $epicoaching_reg_url[0];
        }
        //Get url of upload file
        $file_upload = $catalog ['_upload_file'] [0];
        $file_upload = unserialize ( $file_upload );
        if (is_array ( $file_upload ) && count ( $file_upload ) > 0) {
        	$file_upload = $file_upload [0];
        	$file_upload_path = $file_upload ['image'];
        	$file_upload_title = $file_upload ['title'];
        	$index_pos = strpos ( $file_upload_path, '/wp-content/' );
        	if ($index_pos >= 0) {
        		// Getting sub path since /wp-content
        		$file_upload_path = substr ( $file_upload_path, $index_pos );
        	} else {
        		$file_upload_path = '';
        	}
        } else {
        	$file_upload_path = '';
        }
        // Get message in case there is no course information
        $course_message = $catalog['_message_no_course_information'][0];
    } else {
        // Get main overview
        $catalog_content = '';
        $heading1 = '';
        $trainer_name = '';
        $trainer_profile = '';
        $languages_list = '';
        $size = '';
        $hosted_by = '';
        $epicoaching_reg_url = '';
        $file_upload_path = '';
        $photo_slider_id = 0;
        $course_message = '';
    }
    // Get location
    $location = $custom_fields ['_location'] [0];
    $location = unserialize ( $location );
    $location_arr = array();
    if (is_array ( $location )) {
        foreach($location as $lc) {
            if ($lc != DEFAULT_VALUE_SELECT_NONE_VALUE) $location_arr[] = $lc;
        }
        $location = $location_arr;
        if (count($location) > 0) {
            $location_list = implode ( ', ', $location );
        } else {
            $location_list = '';
        }
    } else {
        $location_list = '';
    }
    // Get start date
    $start_date = $custom_fields[CUSTOM_FIELD_COURSE_START_DATE];
    if ($start_date) {
        $start_date = $start_date[0];
         
    } else {
        $start_date = '';
    }
    // Get expiry date
    $end_date = $custom_fields[CUSTOM_FIELD_COURSE_END_DATE];
    if ($end_date) {
        $end_date = $end_date[0];
        // Check expired time of the course
        $is_expiry = is_expiry($end_date);
    } else {
        $end_date = '';
        $is_expiry = 0;
    }
    // Get custom time
    $custom_time = $custom_fields ['_custom_time'] [0];
    // Get custom date
    $custom_date = $custom_fields ['_custom_date'] [0];
    if (strlen($custom_date) <= 0) $empty_course_info_flag = true;
    // Get duration
    $duration = $custom_fields ['_duration'] [0];
    // Get currency unit
    $currency_unit = $custom_fields ['_currency_unit'];
    if (count ( $currency_unit ) > 0)
        $currency_unit = $currency_unit [0];
        // Get discount
    $discount = $custom_fields ['_discount'];
    if (count ( $discount ) > 0) {
        $discount = $discount [0];
    }
    // Get discount image
    $discount_img = $custom_fields ['_discount_image'] [0];
    $discount_img = unserialize ( $discount_img );
    if (is_array ( $discount_img ) && count ( $discount_img ) > 0) {
        $discount_img = $discount_img [0];
        $discount_img_path = $discount_img ['image'];
        $index_pos = strpos ( $discount_img_path, '/wp-content/' );
        if ($index_pos >= 0) {
            // Getting sub path since /wp-content
            $discount_img_path = substr ( $discount_img_path, $index_pos );
        } else {
            $discount_img_path = '';
        }
    } else {
        $discount_img_path = '';
    }
    // Get logos if any
    $course_logos = $custom_fields ['_other_images'] [0];
    $course_logos = unserialize ( $course_logos );
    
    // Get sub course packages
    $packages = $custom_fields ['_sub_course_packages'];
    if (count ( $packages ) > 0) {
        $packages = unserialize ( $packages [0] );
        if (strlen($packages[0]['name']) <= 0 && strlen($packages[0]['original_price']) <= 0 && strlen($packages[0]['sell_price']) <= 0) {
            $packages = array();
        }
    } else {
        $packages = array ();
    }
    $rows = 1;
    $cols = 3;
    $packages_amount = count ( $packages );
    
    // Calculating amount of columns and rows to display sub course packages
    // If the amount of packages is less than Maximum columns, displaying within 1 row.
    if ($packages_amount < MAX_SUB_COURSE_PACKAGE_COLUMN && $packages_amount > 0) {
        $rows = 1;
        $cols = $packages_amount % MAX_SUB_COURSE_PACKAGE_COLUMN;
        $each_col_width = COLUMNS_PER_ROW / $cols;
    } else if ($packages_amount == 0) {
        $rows = 0;
        $cols = 0;
        $each_col_width = 0;
    } else {
        $rows = Math . ceil ( $packages_amount / MAX_SUB_COURSE_PACKAGE_COLUMN );
        $cols = 3;
        $each_col_width = COLUMNS_PER_ROW / $cols;
    }
    
    $venue = $custom_fields ['_venue'] [0];
    // Checking which category the current post belongs, certificated or non-ceritficated course
    $current_cats = get_the_category ();
    // It belongs to Certificated course, a list of certificated courses shown in the footer.
    $certificate_flag = 1;
    foreach ( $current_cats as $cat ) {
        if ($cat->slug === $curr_cert_course_category) {
            $certificate_flag = 1;
            break;
        } else if ($cat->slug === $curr_non_cert_course_category) {
            $certificate_flag = 0;
            break;
        }
    }
    // Getting all certificated courses to show in the bottom of the page.
    if ($certificate_flag) {
        $args = array (
                'category_name' => $curr_cert_course_category,
                'meta_key' => CUSTOM_FIELD_COURSE_UNIX_START_DATE,
                'orderby' => 'meta_value post_title',
                'order' => 'DESC',
                'post_type' => POST_TYPE_TRAINING_COURSE,
                'post_status' => 'publish',
                'posts_per_page' => - 1 
        );
        $cert_courses = get_posts ( $args );
        $cert_courses_size = count ( $cert_courses );
    } else {
        // Getting all non-certificated courses.
        $args = array (
                'category_name' => $curr_non_cert_course_category,
                'meta_key' => CUSTOM_FIELD_COURSE_UNIX_START_DATE,
                'orderby' => 'meta_value post_title',
                'order' => 'DESC',
                'post_type' => POST_TYPE_TRAINING_COURSE,
                'post_status' => 'publish',
                'posts_per_page' => - 1 
        );
        $cert_courses = get_posts ( $args );
        $cert_courses_size = count ( $cert_courses );
    }
    
    // If all course information is empty, not show table content of Course information
    if (strlen($location_list) <= 0 && strlen($duration) <= 0 && strlen($custom_date) <= 0 && strlen($size) <= 0) $empty_course_info_flag = true;
    else $empty_course_info_flag = false;
    // If start date and end date are undefined, showing a message of contacting us
    if (strlen($start_date) <= 0 && strlen($end_date) <= 0) {
        $enough_course_info = false;
    } else {
        $enough_course_info = true;
    }

    // Get basic course information form catalog if this course belongs to an available course catalog
    $course_linked_photos_id = $custom_fields[CUSTOM_FIELD_COURSE_PHOTO_SLIDER_ID];
    if (is_array($course_linked_photos_id)) {
    	$course_linked_photos_id = (int) $course_linked_photos_id[0];
    } else {
    	$course_linked_photos_id =  0;
    }
?>

<div class="container">
    <div id="detail-course-page-wrapper">
        <div class="breadcrums-detail-course">
                <?php if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                } ?>
        </div>
        <div id="detail-course-page-content">
            <div class="">
                <div class="detail-course-page-title"><h1><?php echo $course_title; ?></h1></div>
                <h1 class="hide"><?php
                if (strlen($heading1) > 0) echo $heading1;
                else echo $course_title;
                ?></h1>
            </div>
            <div class="course-information-wrapper">
                <div class="course-overview">
                    <?php
						// If file infomation course existed
						if (strlen($file_upload_path) > 0) {?>
					<div class="left-col-overview">
					    <h2 class="course-overview-title"><?php echo $text_course_information;?></h2>
					</div>
					<div>
						<a
						class="dt-sc-button filled small"
						title="<?php echo strlen($file_upload_title) > 0 ? $file_upload_title: '';?>" 
						href= <?php echo $file_upload_path;?> download ><?php echo $text_download_course;?>
						</a>
					</div>
					<?php } else {?>
					    <h2 class="course-overview-title"><?php echo $text_course_information;?></h2>
					<?php }?>
                    <?php if ($empty_course_info_flag) {
                        //echo $course_message;
                    } else {?>
                        <div class="course-overview-content">
                        <div class="first-col">
                                <?php
                                if (strlen($location_list) > 0) {
                                ?>
                                <div
                                class="course-overview-content-field">
                                <div class="course-field-lable"><?php echo $text_location;?>:</div>
                                <div class="course-field-value"><?php echo $location_list;?></div>
                            </div>
                                <?php }?>
                                <?php
                                if (strlen($custom_date) > 0) {
                                ?>
                                <div
                                class="course-overview-content-field">
                                <div class="course-field-lable"><?php echo $text_date;?>:</div>
                                <div class="course-field-value">
                                <?php echo $custom_date;?>
                                </div>
                            </div>
                                <?php }?>
                                <?php
                                if (strlen($duration) > 0) {
                                ?>
                                <div
                                class="course-overview-content-field">
                                <div class="course-field-lable"><?php echo $text_duration;?>:</div>
                                <div class="course-field-value">
                                <?php echo $duration;
                                    if (strlen($custom_time) > 0) echo '; '. $custom_time;
                                ?>
                                </div>
                            </div>
                                <?php }?>
                            </div>
                        <div class="first-col">
                                <?php
                                if (strlen($languages_list) > 0) {
                                ?>
                                <div
                                class="course-overview-content-field">
                                <div class="course-field-lable"><?php echo $text_language;?>:</div>
                                <div class="course-field-value"><?php echo $languages_list;?></div>
                            </div>
                                <?php }?>
                                <?php
                                if (strlen($size) > 0) {
                                ?>
                                <div
                                class="course-overview-content-field">
                                <div class="course-field-lable"><?php echo $text_batch_size;?>:</div>
                                <div class="course-field-value"><?php echo $size;?></div>
                            </div>
                                <?php }?>
                                <?php
                                if (strlen($hosted_by) > 0) {
                                ?>
                                <div
                                class="course-overview-content-field">
                                <div class="course-field-lable"><?php echo $text_hosted_by;?>:</div>
                                <div class="course-field-value"><?php echo $hosted_by;?></div>
                            </div>
                                <?php }?>
                            </div>
                    </div>
                    <?php } ?>
                    <div class="course-message"><?php if (!$enough_course_info) echo $course_message;?></div>
                </div>
                <?php if ($packages_amount >  0) {?>
                <div class="course-price-wrapper">
                    <div class="course-unit-price-title">
                        <h2 class="course-field-lable"><?php echo $text_unit_price;?>:</h2>
                        <div class="course-field-value">(<?php echo $text_excluded_tax;?>)</div>
                    </div>
                    <div class="row course-price-detail">
                        <?php
                    $i = 0;
                    foreach ( $packages as $single_package ) {
                        if ($i > SUB_COURSE_PACKAGES_MAX)
                            break;
                        ?>
                        <div
                            class="col-md-<?php echo $each_col_width;?> course-unit-price-col">
                            <div class="course-unit-price-package">
                                <div class="course-unit-price-name"><?php if (strlen($single_package['name']) > 0) echo $single_package['name'];?></div>
                                <div class="course-unit-price-date"><?php if (strlen($single_package['end_date']) > 0) echo $single_package['end_date'];?></div>
                                <div class="original-course-unit-price"><?php if (strlen($single_package['original_price']) > 0) {
                                    echo $single_package['original_price'];
                                    if (strlen($currency_unit) > 0) echo ' '.$currency_unit;
                                }?>
                                </div>
                                <p><span class="course-unit-price"><?php if (strlen($single_package['sell_price']) > 0) {
                                    echo $single_package['sell_price'];
                                    if (strlen($currency_unit) > 0) echo ' '.$currency_unit;
                                }?></span></p>
                            </div>
                        </div>
                        <?php
                        $i ++;
                    }
                    ?>
                    </div>
                </div>
                <?php } ?>
                <?php
                // If the course information is not empty, showing Register button in Course information area
                if ($enough_course_info) {?>
                <div class="row course-information-footer">
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <div class="course-unit-price-off">
                            <?php if (strlen($discount_img_path) > 0) {?>
                            <img class="course-detail-image"
                                src='<?php echo $discount_img_path;?>'
                                alt="course-detail-off-price" title="Course Detail Off" />
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <div class="course-register-button-wrapper">
                            
                            <?php
                            if ($hosted_by == EPICOACHING) {
                                if (!$is_expiry) {
                            ?>
                            <a
                                href="<?php echo $epicoaching_reg_url;?>"
                                rel="nofollow" target="_blank">
                                <div class="course-detail-button dt-sc-button filled"><?php echo $text_register;?>
                                </div>
                            </a>
                            <?php } else {
                            ?>
                            <div class="course-detail-button dt-sc-button filled <?php echo $is_expiry == 1?STYLE_CLASS_EXPIRY_ON:STYLE_CLASS_EXPIRY_OFF;?>">
                            Register
                            </div>
                            <?php 
                                }
                            } else {?>
                            <a
                                onclick="loadCourseInfoIntoRegisterPopup(
                                '<?php echo $course_title;?>','<?php echo $hosted_by;?>',<?php echo $certificate_flag;?>,'<?php echo $custom_date?>','<?php echo $location_list;?>','<?php echo $trainer_name;?>')"
                                    href="#login-box" class="register-login <?php echo $is_expiry == 1?STYLE_CLASS_EXPIRY_ON:STYLE_CLASS_EXPIRY_OFF;?>">
                                <div class="course-detail-button dt-sc-button filled"><?php echo $text_register;?>
                                </div>
                            </a>
                            <?php }?>
                            
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <?php } // End if (!$empty_course_info_flag)?>
            </div>
            <div id="course-detail-content-wrapper">
                <div class="detail-content-item">
                    <div class="detail-content-item-body">
                        <?php echo $catalog_content; ?>
                    </div>
                </div>
                
                <?php
                // If $venue is not empty, showing this information.
                if (strlen($venue) > 0 && trim(strip_tags($venue)) != '&nbsp;') {?>
                <div class="detail-content-item">
                    <h2 class="detail-content-item-title"><?php echo $text_venue;?></h2>
                    <div class="detail-content-item-body"><?php echo $venue;?></div>
                </div>
                <?php } ?>
                <div class="row course-detail-content-footer">
                    <div class="col-md-2 second-register-button-wrapper">
                    <?php
                    if ($hosted_by == EPICOACHING) {
                        if (!$is_expiry) {
                    ?>
                        <a href="<?php echo $epicoaching_reg_url;?>"
                           rel="nofollow" target="_blank">
                            <div class="course-detail-button dt-sc-button filled"><?php echo $text_register;?>
                            </div>
                        </a>
                    <?php } else {
                    ?>
                        <div class="course-detail-button dt-sc-button filled <?php echo $is_expiry == 1?STYLE_CLASS_EXPIRY_ON:STYLE_CLASS_EXPIRY_OFF;?>">
                            Register
                        </div>
                    <?php 
                        }
                    } else {?>
                        <a
                            onclick="loadCourseInfoIntoRegisterPopup(
                                    '<?php echo $course_title;?>','<?php echo $hosted_by;?>',<?php echo $certificate_flag;?>,'<?php echo $custom_date?>','<?php echo $location_list;?>','<?php echo $trainer_name;?>')"
                            href="#login-box" class="register-login <?php echo $is_expiry == 1?STYLE_CLASS_EXPIRY_ON:STYLE_CLASS_EXPIRY_OFF;?>">
                            <div class="course-detail-button dt-sc-button filled"><?php echo $text_register;?>
                            </div>
                        </a>
                        <?php }?>
                    </div>
                </div>
            </div>
            
            <?php 
            $portfolio_settings = get_post_meta ($course_linked_photos_id, '_portfolio_settings', TRUE );
            $portfolio_settings = is_array ( $portfolio_settings ) ? $portfolio_settings : array ();
            
            ?>
            
             <?php if(isset($portfolio_settings["items_name"]) && count($portfolio_settings["items_name"]) > 0){?>
                <div id="course-photos-slider-wrapper">
                <h2 class="course-detail-content-title"><?php echo $text_title_slide_image_detail_course;?></h2>
                
            <div class="photos-center">
	               <ul class="portfolio-slider"><?php
							if( array_key_exists("items_name",$portfolio_settings) ) {
								foreach( $portfolio_settings["items_name"] as $key => $item ){
									$current_item = $portfolio_settings["items"][$key];
									
									if( "video" === $item ) {
										echo "<li>".wp_oembed_get( $current_item )."</li>";
									} else {
										echo "<li> <img src='{$current_item}' alt='' title='' /></li>";
									}
								}
							} else {
								echo "<li> <img src='http://placehold.it/1170x878&text=Portfolio' alt='' title=''/></li>";
							}?>
				    </ul>
			</div>
                <div class="course-detail-content-slider" id="course-detail-content-slider-id">
                

                </div>                     
            </div>
            <?php } ?>
            <div class="hrbreak"></div>
            <div class="other-certificated-courses certificate">
                <div class="detail-content-item">
                    <div class="detail-content-item-title">
                        <h3>
                        <?php
                        if ($certificate_flag) echo $other_certificated_courses;
                        else echo $other_non_certificated_courses;
                        ?>
                        </h3>
                    </div>
                    <div class="detail-content-item-body">
                        <?php
                        // Get all certified courses
                        if ($certificate_flag == 1) {
                        	include(locate_template('get-certified-courses.php'));
                        }
                        else {
                        	// Get all non certified courses
                        	include(locate_template('get-non-certified-courses.php'));
                        }
                        // Showing the courses list in table template
                        include(locate_template('public-courses-table-template.php'));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>

<script type="text/javascript">
function updateTraingCourseTables() {
    var windowWidth = jQuery('html').width();
    if (windowWidth <= 565) {
        // Hide some column in training courses tables
        jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(3)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(4)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(5)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(3)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(4)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(5)').addClass('hide');

        jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(4)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(5)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(6)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(4)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(5)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(6)').addClass('hide');

        jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(2)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(2)').addClass('hide');

        jQuery('.ie8 .course-price-detail .course-unit-price-col').css('float','none');
    } else if (windowWidth <= 900) {
        // Hide some column in training courses tables
        jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(3)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(4)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(5)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(3)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(4)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(5)').addClass('hide');

        jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(4)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(5)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(6)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(4)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(5)').addClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(6)').addClass('hide');
    } else if (windowWidth > 900) {
        // Show some column in training courses tables
        jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(3)').removeClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(4)').removeClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(5)').removeClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(3)').removeClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(4)').removeClass('hide');
        jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(5)').removeClass('hide');

        jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(4)').removeClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(5)').removeClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(6)').removeClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(4)').removeClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(5)').removeClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(6)').removeClass('hide');
        jQuery('.ie8 .course-price-detail .course-unit-price-col').css('float','left');
        jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(2)').removeClass('hide');
        jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(2)').removeClass('hide');
        
    }
}
jQuery(document).ready(function(){
    var logo = jQuery('.single-training-course #course-detail-content-wrapper .detail-course-logo:first').css('background-image');
    if (logo) {
        // Restyle background  position if the logo is Management 3.0
        if (logo.indexOf('Management3.0-logo') > 0&&!isIE()) {
            jQuery('.single-training-course #course-detail-content-wrapper .detail-course-logo:first').css('background-position',"100% 60%");
        }
    }
    // For IE 8, Set width for img tag of certificate icon if its width is greater than 200
    if (isIE() <= 8) {
        var certIcons = jQuery('.ie8 .other-certificated-courses tbody tr td.course-cert-icon img');
        var certIconWidth;
        if (certIcons.length > 0) {
            certIcons.each(function() {
                certIconWidth = jQuery(this).width();
                if (certIconWidth > 180) {
                    jQuery(this).width(180+'px');
                    }
                });
        }
        // Updating training course tables by hidding some column in small screen sizes
        updateTraingCourseTables();
    }
    jQuery('html.ie8').resize(function(){
        updateTraingCourseTables();
    });
    
});
</script>

