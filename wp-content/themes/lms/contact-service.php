<?php require('../../../wp-load.php'); ?>
<?php
if(isset($_POST['firstname'])){
	global $contact_errors;
	global $message;
	$contact_errors = false;
	//get training and coaching infor
	$coursename= strip_tags($_POST['course']);
	$date=strip_tags($_POST['date']);
	$location=strip_tags($_POST['location']);
	$trainer=strip_tags($_POST['trainer']);
	$hosted=$_POST['hosted'];

	//put message variable
	$message ="<h3>COURSE INFORMATION</h3>";
	$message .= "<table cellpadding='4'><tr> <td><b>Course Name:</b></td><td> $coursename</td></tr>";
	$message .= "<tr><td><b>Date:</b></td><td> $date</td></tr>";
	$message .= "<tr><tr><b>Loaction:</b></td><td>$location</td></tr>";
	$message .= "<tr><tr><b>Hosted by:</b></td><td>$hosted</td></tr>";
	$message .= "<tr><td><b>Trainer:</b></td><td> $trainer</td></tr></table><br/><hr/><br/>";
	//get ATTENDEE INFORMATION
	$firstname= strip_tags($_POST['firstname']);
	$lastname= strip_tags($_POST['lastname']);
	$gender= strip_tags($_POST['gender']);
	$jobtitle= strip_tags($_POST['jobtitle']);
	$company= strip_tags($_POST['company']);
	$email= strip_tags($_POST['email']);
	$workphone= strip_tags($_POST['workphone']);
	$address=strip_tags($_POST['address']);
	//put message variable
	$message .="<h3>ATTENDEE INFORMATION</h3>";
	$message .= "<table cellpadding='4'><tr><td><b>Title:</b></td><td> $gender</td></tr>";
	$message .= "<tr><td><b>First Name:</b></td><td> $firstname</td></tr>";
	$message .= "<tr><td><b>Last Name:</b></td><td> $lastname</td></tr>";
	$message .= "<tr><td><b>Job Title:</b></td><td> $jobtitle</td></tr>";
	$message .= "<tr><td><b>Company:</b></td><td> $company</td></tr>";
	$message .= "<tr><td><b>Email:</b></td><td> $email</td></tr>";
	$message .= "<tr><td><b>Work Phone:</b></td><td> $workphone</td></tr>";
	$message .= "<tr><td><b>Address:</b></td><td> $address</td></tr></table>";
	$subject = '[Training Course]['.$hosted.'][Register] '.$_POST['course'];
	$subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
	add_filter( 'wp_mail_content_type', 'set_html_content_type' );
	$user = get_userdatabylogin('smartdev_training');
	$to = $user->user_email;
	// $to= get_option( 'admin_email' );
	// $to = "contact@smartdev.vn";
	// send the email using wp_mail()
	$contact_errors = wp_mail($to, $subject, $message);
	remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

	echo $contact_errors;
}