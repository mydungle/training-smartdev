<?php
	include 'utilities.php';
	function showDialog()
	{
	  echo ''?>
	  <div>
	    <div id="login-box" class="container-dialog row">
	      <div class="row title-dialog">
	        <div class="row title-course">SELECTED COURSE</div>
	        <div class="row container-name-course" id="iconsourse">
	          <img class="img-course" src="<?php echo get_template_directory_uri() . '/images/customize/ico1.png';?>" />
	          <div id="name-course"></div>
	        </div>
	        <div class="row">
	          <div class="row title-form">ATTENDEE INFORMATION</div>
	        </div>
	        <div class="container-input">
	          <div class="row">
	            <div class="col-md-4 label-form">First Name *
	              <input class="in-address" id="firstname" type="text"/>
	            </div>
	            <div class="col-md-4 label-form">Last Name *
	              <input class="in-address" id="lastname" type="text"/>
	            </div>
	            <div class="col-md-4 label-form">Mr./Mrs./Ms. *
	              <input class="in-address" id="gender" type="text" />
	            </div>
	          </div>
	          <div class="row">
	            <div class="col-md-6 label-form">Job Title *
	              <input class="in-address" id="jobtitle" type="text"/></div>
	              <div class="col-md-6 label-form">Company *
	                <input class="in-address" id="company" type="text"/>
	              </div>
	            </div>
	            <div class="row ">
	              <div class="col-md-6 label-form">Email *
	                <input class="in-address" id="email" type="text" /></div>
	                <div class="col-md-6 label-form">Work Phone *
	                  <input class="in-address" id="workphone" type="text"/>
	                </div>
	              </div>
	              <div class="row margin-address">
	                Address *
	                <input class="in-address" id="address" type="text"/>
	              </div>
	              <div class="row margin-address">
	                <span class="message-color">*Required information</span>
	              </div>
	              <div class="row margin-address">
	                <span class="message-color1" id="message"></span>
	              </div>
	              <div class="row margin-address">
	                <div id="submit" type="button" class="dt-sc-button">SUBMIT</button>
	              </div>
	              <input type="hidden" name="language_code" value="<?php echo LANGUAGE_CODE_ENGLISH;?>">
	            </div>
	          </div>
	        </div>
	      </div>
	      <?php
	    }
	
	    ?>
	
	    <?php
	    function showSussecc()
	    {
	      echo ''?>
	      <div>
	        <div id="successdg" class="success-dialog row">
	          <div class="container-img">
	           <img class="img-success" src="<?php echo get_template_directory_uri() . '/images/customize/success-icon.png';?>" />
	         </div>
	         <div class="message-success"><p>Thank you! Registered <b>successfully.</b></br> We will contact you in the next few days.</p></div>
	         <div><button id="btn-success-ok" type="button" class="btn btn-primary btn-custom2">OK</button></div>
	       </div>
	     </div>
	     <?php
	   }
	
	   ?>
	   <?php
	   function showErr()
	   {
	    echo ''?>
	    <div>
	    <div id="errodg" class="success-dialog row">
	        <div class="container-img">
	         <img class="img-success" src="<?php echo get_template_directory_uri() . '/images/customize/failed-icon.png';?>" />
	       </div>
	       <div class="message-success"><p>Sorry, registration <b>failed</b> due to system errors. <br/>Please register again.</p></div>
	       <div><button id="btn-success-ok" type="button" class="btn btn-primary btn-custom2">OK</button></div>
	     </div>
	   </div>
	   <?php
	 }
	 ?>
	
	 <?php
	 function showloadWaiting()
	 {
	  echo ''?>
	  <div id="dvLoading" >
	    <img style="" class="loading-sendmail" src="<?php echo get_template_directory_uri().'/images/customize/loading.gif';?>" />
	  </div>
	  <?php
	}
	function addDialog(){
	  showSussecc();
	  showDialog();
	  showloadWaiting();
	  showErr();
	}
?>

