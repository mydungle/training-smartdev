<?php

	/*
	 * Template of Training course in English and Vietnamese.
	 */
    define('VN_URL_PREFIX', '/vn/');
 	// Checking if the post belongs to Vietnamese or not
 	// If it's written by Vietnamese, loading corresponding template
 	$url = $_SERVER['REQUEST_URI'];
 	$url_length = strlen($url);
 	$pos = strpos($url, VN_URL_PREFIX);
	if ($url_length > 1 && is_numeric($pos) && $pos == 0) {
		include 'vn-template/vn-header.php';
 		// Loading Vietnamese template for training course
 		get_template_part('vn-template/vn-single', 'training-catalog');
 	} else {
 		// Loading English temmplate
 		get_header();
 		include 'settings.php';
?>

<?php
    $curr_cert_course_category = __('certified-course-catalog', 'dt_themes');
    $curr_non_cert_course_category = __('non-certified-course-catalog', 'dt_themes');
    $text_download_course = __('Download this course', 'dt_themes');
    $text_course_information = __('Course information', 'dt_themes');
    $text_language = __('Language', 'dt_themes');
    $text_batch_size = __('Batch size', 'dt_themes');
    $text_hosted_by = __('Hosted by', 'dt_themes');
    $other_certificated_courses = __('Other Certified Courses', 'dt_themes');
    $other_non_certificated_courses = __('Other Non-certified Courses', 'dt_themes');
    $fasten_word = __('and', 'dt_themes');
?>
<?php
    // Get id of the current post
    if( have_posts() ): while( have_posts() ): the_post();
        $post_id = get_the_ID();
		break;
	endwhile; endif;
	// Get id of the current post
	if ($post_id <= 0) $post_id = $post->ID;
	$course_title = $post->post_title;
	$custom_fields = get_post_custom ( $post_id );
	$custom_fields = get_post_meta($post_id);
	
	// Get heading 1
	$heading1 = $custom_fields ['_heading_1'] [0];
	
	// Get trainer
	$trainer_info = $custom_fields ['_trainer'] [0];
	$trainer_info = unserialize($trainer_info ) [0];
	$trainer_name = $trainer_info ['trainer_name'];
	$trainer_profile = $trainer_info ['profile_url'];
	
	// Get languages
	$languages = $custom_fields ['_languages'] [0];
	$languages = unserialize ( $languages );
	$languages_arr = array();
	if (is_array ( $languages )) {
		foreach($languages as $ll) {
			if ($ll != DEFAULT_VALUE_SELECT_NONE_VALUE) $languages_arr[] = $ll;
		}
		$languages = $languages_arr;
		if (count($languages) > 0) {
			$languages_list = implode ( ', ', $languages );
		} else {
			$languages_list = '';
		}
		if (count ( $languages ) == 2) {
			$languages_list = implode (' ' .$fasten_word.' ', $languages );
		} else {
			$languages_list = implode ( ', ', $languages );
		}
	} else {
		$languages_list = '';
	}
	
	// Get batch size
	$size = $custom_fields ['_batch_size'] [0];
	
	// Get hosted by value
	$hosted_by = $custom_fields ['_hosted_by'];
	if (!is_array($hosted_by)) $hosted_by = '';
	else $hosted_by = $hosted_by[0];
	// Get Register url of Epicoaching course
	$epicoaching_reg_url = $custom_fields['_epicoaching_register_url'];
	if ($epicoaching_reg_url) {
		$epicoaching_reg_url = $epicoaching_reg_url[0];
	} else {
		$epicoaching_reg_url = '';
	}
	
	//Get url of upload file
	$file_upload = $custom_fields ['_upload_file'] [0];
	$file_upload = unserialize ( $file_upload );
	if (is_array ( $file_upload ) && count ( $file_upload ) > 0) {
		$file_upload = $file_upload [0];
		$file_upload_path = $file_upload ['image'];
		$file_upload_title = $file_upload ['title'];
		$index_pos = strpos ( $file_upload_path, '/wp-content/' );
		if ($index_pos >= 0) {
			// Getting sub path since /wp-content
			$file_upload_path = substr ( $file_upload_path, $index_pos );
		} else {
			$file_upload_path = '';
		}
	} else {
		$file_upload_path = '';
	}
	
	
	// Get logos if any
	$course_logos = $custom_fields ['_other_images'] [0];
	$course_logos = unserialize ( $course_logos );
	
	// Get sub course packages
	$packages = $custom_fields ['_sub_course_packages'];
	if (count ( $packages ) > 0) {
		$packages = unserialize ( $packages [0] );
		if (strlen($packages[0]['name']) <= 0 && strlen($packages[0]['original_price']) <= 0 && strlen($packages[0]['sell_price']) <= 0) {
			$packages = array();
		}
	} else {
		$packages = array ();
	}
	$rows = 1;
	$cols = 3;
	$packages_amount = count ( $packages );
	
	// Calculating amount of columns and rows to display sub course packages
	// If the amount of packages is less than Maximum columns, displaying within 1 row.
	if ($packages_amount < MAX_SUB_COURSE_PACKAGE_COLUMN && $packages_amount > 0) {
		$rows = 1;
		$cols = $packages_amount % MAX_SUB_COURSE_PACKAGE_COLUMN;
		$each_col_width = COLUMNS_PER_ROW / $cols;
	} else if ($packages_amount == 0) {
		$rows = 0;
		$cols = 0;
		$each_col_width = 0;
	} else {
		$rows = Math . ceil ( $packages_amount / MAX_SUB_COURSE_PACKAGE_COLUMN );
		$cols = 3;
		$each_col_width = COLUMNS_PER_ROW / $cols;
	}
	// Get facilitators
	$facilitators = $custom_fields ['_facilitators'] [0];
	$objectives = $custom_fields ['_objectives'] [0];
	$hand_to_you = $custom_fields ['_hand_to_you'] [0];
	$attenders = $custom_fields ['_attenders'] [0];
	$prerequisites = $custom_fields ['_prerequisites'] [0];
	$venue = $custom_fields ['_venue'] [0];
	// Checking which category the current post belongs, certificated or non-ceritficated course
	$current_cats = get_the_category ();
	// It belongs to Certificated course, a list of certificated courses shown in the footer.
	$certificate_flag = 1;
	foreach ( $current_cats as $cat ) {
		if ($cat->slug === $curr_cert_course_category) {
			$certificate_flag = 1;
			break;
		} else if ($cat->slug === $curr_non_cert_course_category) {
			$certificate_flag = 0;
			break;
		}
	}
	// Getting all certificated courses to show in the bottom of the page.
	if ($certificate_flag) {
		// Getting all certified courses from catalog
		$args = array (
				'category_name' => $curr_cert_course_category,
				'orderby' => 'post_title',
				'order' => 'ASC',
				'post_type' => POST_TYPE_COURSE_CATALOG,
				'post_status' => 'publish',
				'posts_per_page' => - 1
		);
		$courses = get_posts($args);
	} else {
		// Getting all certified courses from catalog
		$args = array (
				'category_name' => $curr_non_cert_course_category,
				'orderby' => 'post_title',
				'order' => 'ASC',
				'post_type' => POST_TYPE_COURSE_CATALOG,
				'post_status' => 'publish',
				'posts_per_page' => - 1
		);
		$courses = get_posts($args);
	}
	// Get message in case there is no course information
	$course_message = $custom_fields ['_message_no_course_information'] [0];
	// If all course information is empty, not show table content of Course information
	if (strlen($hosted_by) <= 0 && strlen($trainer_name) <= 0 && strlen($size) <= 0) $empty_course_info_flag = true;
	else $empty_course_info_flag = false;
	if (strlen($location_list) > 0 && strlen($duration) > 0 && strlen($custom_date) > 0 && strlen($size) > 0) $enough_course_info = true;
	else $enough_course_info = false;
	
	
?>
	<div id="detail-course-page-wrapper">
		<div class="breadcrums-detail-course">
                <?php if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                } ?>
        </div>
		<div id="detail-course-page-content">
			<div class="">
				<div class="detail-course-page-title"><h1><?php the_title(); ?></h1></div>
				<h1 class="hide"><?php
                if (strlen($heading1) > 0) echo $heading1;
                else echo $course_title;
                ?></h1>
			</div>
			<div class="course-information-wrapper">
				<div class="course-overview">
				    <?php
						// If file infomation course existed
						if (strlen($file_upload_path) > 0) {?>
					<div class="left-col-overview">
					    <h2 class="course-overview-title"><?php echo $text_course_information;?></h2>
					</div>
					<div>
						<a
						class="dt-sc-button filled small"
						title="<?php echo strlen($file_upload_title) > 0 ? $file_upload_title: '';?>" 
						href= <?php echo $file_upload_path;?> download ><?php echo $text_download_course;?>
						</a>
					</div>
					<?php } else {?>
					    <h2 class="course-overview-title"><?php echo $text_course_information;?></h2>
					<?php }?>
                    <div class="row course-overview-content">
						
						<div class="col-md-6">
						    <?php
                            	if (strlen($languages_list) > 0) {
                            	?>
                                <div
								class="course-overview-content-field">
								<div class="course-field-lable"><?php echo $text_language?>:</div>
								<div class="course-field-value"><?php echo $languages_list;?></div>
							    </div>
                                <?php }?>
                            <?php
                                if (strlen($size) > 0) {
                            ?>
                            <div
								class="course-overview-content-field">
								<div class="course-field-lable"><?php echo $text_batch_size;?>:</div>
								<div class="course-field-value"><?php echo $size;?></div>
							</div>
                            <?php }?>
                            <?php
                            	if (strlen($hosted_by) > 0) {
                            ?>
                            <div
								class="course-overview-content-field">
								<div class="course-field-lable"><?php echo $text_hosted_by;?>:</div>
								<div class="course-field-value"><?php echo $hosted_by;?></div>
							</div>
                            <?php }?>
                        </div>
					</div>
                    <div class="course-message"><?php if (!$enough_course_info) echo $course_message;?></div>
				</div>
            </div>
			<div id="course-detail-content-wrapper">
			    <?php the_content();?>
				
			</div>
			<div class="hrbreak"></div>
			<div id="single-course-catalog-page">
			    <div class="other-certificated-courses <?php if ($certificate_flag) echo 'certificate'; else echo 'non-certificate';?>">
					<div class="detail-content-item">
						<div class="detail-content-item-title">
						    <h2>
	                    	<?php
	                    	if ($certificate_flag) echo $other_certificated_courses;
	                    	else echo $other_non_certificated_courses;
	                    	?>
	                    	</h2>
	                    </div>
						<div class="detail-content-item-body">
						    <?php
							include(locate_template('course-catalog-table-template.php'));
							?>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>

<?php get_footer(); ?>

<script type="text/javascript">
function updateTraingCourseTables() {
	var windowWidth = jQuery('html').width();
	if (windowWidth <= 565) {
		// Hide some column in training courses tables
		jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(3)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(4)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(5)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(3)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(4)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(5)').addClass('hide');

		jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(4)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(5)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(6)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(4)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(5)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(6)').addClass('hide');

		jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(2)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(2)').addClass('hide');

		jQuery('.ie8 .course-price-detail .course-unit-price-col').css('float','none');
	} else if (windowWidth <= 900) {
		// Hide some column in training courses tables
		jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(3)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(4)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(5)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(3)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(4)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(5)').addClass('hide');

		jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(4)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(5)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(6)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(4)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(5)').addClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(6)').addClass('hide');
	} else if (windowWidth > 900) {
		// Show some column in training courses tables
		jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(3)').removeClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(4)').removeClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(5)').removeClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(3)').removeClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(4)').removeClass('hide');
		jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(5)').removeClass('hide');

		jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(4)').removeClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(5)').removeClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(6)').removeClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(4)').removeClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(5)').removeClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(6)').removeClass('hide');
		jQuery('.ie8 .course-price-detail .course-unit-price-col').css('float','left');
		jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(2)').removeClass('hide');
		jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(2)').removeClass('hide');
		
	}
}
jQuery(document).ready(function(){
	var logo = jQuery('.single-training-course #course-detail-content-wrapper .detail-course-logo:first').css('background-image');
	if (logo) {
		// Restyle background  position if the logo is Management 3.0
		if (logo.indexOf('Management3.0-logo') > 0&&!isIE()) {
			jQuery('.single-training-course #course-detail-content-wrapper .detail-course-logo:first').css('background-position',"100% 60%");
		}
	}
	// For IE 8, Set width for img tag of certificate icon if its width is greater than 200
	if (isIE() <= 8) {
		var certIcons = jQuery('.ie8 .other-certificated-courses tbody tr td.course-cert-icon img');
		var certIconWidth;
		if (certIcons.length > 0) {
			certIcons.each(function() {
				certIconWidth = jQuery(this).width();
				if (certIconWidth > 180) {
					jQuery(this).width(180+'px');
					}
				});
		}
	}
	jQuery('html.ie8').resize(function(){
		updateTraingCourseTables();
	});
	
});
</script>
	
<?php }// End loading English template?>