<?php
	// Define texts translated by String Translations
	$text_course_name = __('Course name', 'dt_themes');
	$text_host = __('Host', 'dt_themes');
	$text_download = __('Download', 'dt_themes');
	$text_date = __('Date', 'dt_themes');
	$text_location = __('Location', 'dt_themes');
	$text_trainer = __('Trainer', 'dt_themes');
	$text_hosted_by = __('Host', 'dt_themes');
	$text_upcoming = __('Upcoming', 'dt_themes');
	$text_will_come = __('Finalizing', 'dt_themes');
	$text_archived = __('Archived', 'dt_themes');
	$text_register = __('Register', 'dt_themes');
	$msg_empty_training_courses = __('No Courses Found!', 'dt_themes');
	// Checking flag to print id tag
	if ($certificate_flag == 1) {
		$html_course_wrapper_id = 'certified-courses-table-wrapper';
		$html_upcoming_course_wrapper_id = 'up-coming-certified-courses-wrapper';
		$html_finalizing_course_wrapper_id = 'will-come-certified-courses-wrapper';
		$html_archived_course_wrapper_id = 'archived-certified-courses-wrapper';
	} else {
		$html_course_wrapper_id = 'non-certified-courses-table-wrapper';
		$html_upcoming_course_wrapper_id = 'up-coming-non-certified-courses-wrapper';
		$html_finalizing_course_wrapper_id = 'will-come-non-certified-courses-wrapper';
		$html_archived_course_wrapper_id = 'archived-non-certified-courses-wrapper';
	}
?>
<div class="course-of-coach">
    <ul class="smd-tabs" ref-content-id="#<?php echo $html_course_wrapper_id;?>">
        <li class="selected"><a ref-id="#<?php echo $html_upcoming_course_wrapper_id;?>" class="smd-tab-title"><?php echo $text_upcoming;?></a></li>
        <li><a ref-id="#<?php echo $html_finalizing_course_wrapper_id;?>" class="smd-tab-title"><?php echo $text_will_come;?></a></li>
        <li><a ref-id="#<?php echo $html_archived_course_wrapper_id;?>" class="smd-tab-title"><?php echo $text_archived;?></a></li>
    </ul>
    <div id="<?php echo $html_course_wrapper_id;?>" class="panel-container">
        <div id="<?php echo $html_upcoming_course_wrapper_id;?>" class="smd-tab-content selected">
        	<?php
        	// If the list of upcoming courses is not empty
        	if (is_array($upcoming) && count($upcoming) > 0) {?>
        	<table class="table table-striped table-condensed">
            	<thead>
                	<tr>
                    	<th class='col-md-4'><?php echo $text_course_name;?></th>
						<th class="col-md-1"></th>
                        <th class="col-md-1"><?php echo $text_date;?></th>
                        <th class="col-md-1"><?php echo $text_location;?></th>
                        <th class="col-md-2"><?php echo $text_trainer;?></th>
                        <th class="col-md-2"><?php echo $text_hosted_by;?></th>
                        <th class="col-md-1"></th>
                    </tr>
                </thead>
                <tbody>
                	<?php
                	foreach ($upcoming as $course) {
                		// Prepare data to show list of certificated courses in the table
                		$tmp_course_title = $course->post_title;
                		if (strlen($tmp_course_title) > MAX_CHARACTERS_COURSE_TITLE) {
                			$tmp_short_course_title = substr($tmp_course_title, 0, MAX_CHARACTERS_COURSE_TITLE) . ETC_DOTS;
                		} else {
                			$tmp_short_course_title = $tmp_course_title;
                		}
                		// Get url of course page
                		$tmp_permalink =  $course->permalink;
                		// Get location
                		$tmp_location_list = $course->location;
                		// Check expired time of the course
                		$tmp_is_expiry = $course->is_expiry;
                		// Get custom date
                		$tmp_custom_date = $course->custom_date;
                		// Get trainer
                		$tmp_trainer_name = $course->trainer_name;
                		$tmp_trainer_profile = $course->trainer_profile;
                		// Get hosted by value
                		$tmp_hosted_by = $course->hosted_by;
                		// Getting certificate icon of the course
                		$cert_icon = $course->certified_icon;
                		// Get Register url of Epicoaching course
                		$tmp_epicoaching_reg_url = $course->epicoaching_reg_url;
                		$tmp_certified_flag = $course->certified_flag;
                		?>
                	    <tr>
                	    <td class="coures-name-col"><a title="<?php echo $tmp_course_title;?>"
                	        href="<?php echo $tmp_permalink;?>"><?php echo $tmp_short_course_title;?></a>
                	    </td>
                	    
                	    <td class="course-cert-icon" >
                	    <?php if ($tmp_certified_flag == 1) echo $cert_icon;?>
                		</td>
                	    <td class="course-date-col"><?php echo $tmp_custom_date;?></td>
                	    <td><?php echo $tmp_location_list;?></td>
                	    <td><a class="color-inherit" href="<?php echo $tmp_trainer_profile;?>"
                	        target="_blank"><?php echo $tmp_trainer_name;?></a></td>
                	    <td><?php echo $tmp_hosted_by;?></td>
                	    <td class="course-register-col">
                	    <?php if (!$tmp_is_expiry && $tmp_hosted_by == EPICOACHING) {?>
                	    <a
                		href="<?php echo $tmp_epicoaching_reg_url;?>"
                		rel="nofollow" target="_blank"><?php echo $text_register;?>
                		</a>
                	    <?php } else if (!$tmp_is_expiry) {?>
                	    <a
                		    onclick="loadCourseInfoIntoRegisterPopup(
                	    	 '<?php echo $tmp_course_title;?>','<?php echo $tmp_hosted_by;?>',<?php echo $tmp_certified_flag;?>,'<?php echo $tmp_custom_date?>','<?php echo $tmp_location_list;?>','<?php echo $tmp_trainer_name;?>')"
                			href="#login-box" class="register-login <?php echo $tmp_is_expiry == 1?STYLE_CLASS_EXPIRY_ON:STYLE_CLASS_EXPIRY_OFF;?>">
                			<?php echo $text_register;?>
                	    </a>
                	    <?php }?>
                	    </td>
                	</tr>
             <?php 
                 }// End foreach showing list of certificted course
             ?>
            </table>
			<?php } else {
                    // End checking empty list of upcomming courses. If it's empty, showing message.
            		?>
                <b><?php echo $msg_empty_training_courses;?></b>
                <?php  
                 }
            ?>
        </div>
        <div id="<?php echo $html_finalizing_course_wrapper_id;?>" class="smd-tab-content">
        	<?php
        	// If the list of upcoming courses is not empty
        	if (is_array($plan) && count($plan) > 0) {?>
        	<table class="table table-striped table-condensed">
            	<thead>
                	<tr>
                    	<th class='col-md-4'><?php echo $text_course_name;?></th>
						<th class="col-md-1"></th>
                        <th class="col-md-1"><?php echo $text_date;?></th>
                        <th class="col-md-1"><?php echo $text_location;?></th>
                        <th class="col-md-2"><?php echo $text_trainer;?></th>
                        <th class="col-md-2"><?php echo $text_hosted_by;?></th>
                        <th class="col-md-1"></th>
                    </tr>
                </thead>
                <tbody>
                	<?php
                	foreach ($plan as $course) {
                		// Prepare data to show list of certificated courses in the table
                		$tmp_course_title = $course->post_title;
                		if (strlen($tmp_course_title) > MAX_CHARACTERS_COURSE_TITLE) {
                			$tmp_short_course_title = substr($tmp_course_title, 0, MAX_CHARACTERS_COURSE_TITLE) . ETC_DOTS;
                		} else {
                			$tmp_short_course_title = $tmp_course_title;
                		}
                		// Get url of course page
                		$tmp_permalink =  $course->permalink;
                		// Get location
                		$tmp_location_list = $course->location;
                		// Check expired time of the course
                		$tmp_is_expiry = $course->is_expiry;
                		// Get custom date
                		$tmp_custom_date = $course->custom_date;
                		// Get trainer
                		$tmp_trainer_name = $course->trainer_name;
                		$tmp_trainer_profile = $course->trainer_profile;
                		// Get hosted by value
                		$tmp_hosted_by = $course->hosted_by;
                		// Getting certificate icon of the course
                		$cert_icon = $course->certified_icon;
                		// Get Register url of Epicoaching course
                		$tmp_epicoaching_reg_url = $course->epicoaching_reg_url;
                		$tmp_certified_flag = $course->certified_flag;
                		?>
                	    <tr>
                	    <td class="coures-name-col"><a title="<?php echo $tmp_course_title;?>"
                	        href="<?php echo $tmp_permalink;?>"><?php echo $tmp_short_course_title;?></a>
                	    </td>
                	    
                	    <td class="course-cert-icon" >
                	    <?php if ($tmp_certified_flag == 1) echo $cert_icon;?>
                		</td>
                	    <td class="course-date-col"><?php echo $tmp_custom_date;?></td>
                	    <td><?php echo $tmp_location_list;?></td>
                	    <td><a class="color-inherit" href="<?php echo $tmp_trainer_profile;?>"
                	        target="_blank"><?php echo $tmp_trainer_name;?></a></td>
                	    <td><?php echo $tmp_hosted_by;?></td>
                	    <td class="course-register-col">
                	    <?php if (!$tmp_is_expiry && $tmp_hosted_by == EPICOACHING) {?>
                	    <a
                		href="<?php echo $tmp_epicoaching_reg_url;?>"
                		rel="nofollow" target="_blank"><?php echo $text_register;?>
                		</a>
                	    <?php } else if (!$tmp_is_expiry) {?>
                	    <a
                		    onclick="loadCourseInfoIntoRegisterPopup(
                	    	 '<?php echo $tmp_course_title;?>','<?php echo $tmp_hosted_by;?>',<?php echo $tmp_certified_flag;?>,'<?php echo $tmp_custom_date?>','<?php echo $tmp_location_list;?>','<?php echo $tmp_trainer_name;?>')"
                			href="#login-box" class="register-login <?php echo $tmp_is_expiry == 1?STYLE_CLASS_EXPIRY_ON:STYLE_CLASS_EXPIRY_OFF;?>">
                			<?php echo $text_register;?>
                	    </a>
                	    <?php }?>
                	    </td>
                	</tr>
             <?php 
                 }// End foreach showing list of certificted course
             ?>
            </table>
			<?php } else {
                 // End checking empty list of upcomming courses. If it's empty, showing message.
                ?>
                <b><?php echo $msg_empty_training_courses;?></b>
                <?php                   
                 }
            ?>
        </div>
        <div id="<?php echo $html_archived_course_wrapper_id;?>" class="smd-tab-content">
        <?php
        	// If the list of upcoming courses is not empty
        	if (is_array($archived) && count($archived) > 0) {?>
        	<table class="table table-striped table-condensed">
            	<thead>
                	<tr>
                    	<th class='col-md-4'><?php echo $text_course_name;?></th>
						<th class="col-md-1"></th>
                        <th class="col-md-1"><?php echo $text_date;?></th>
                        <th class="col-md-1"><?php echo $text_location;?></th>
                        <th class="col-md-2"><?php echo $text_trainer;?></th>
                        <th class="col-md-2"><?php echo $text_hosted_by;?></th>
                        <th class="col-md-1"></th>
                    </tr>
                </thead>
                <tbody>
                	<?php
                	foreach ($archived as $course) {
                		// Prepare data to show list of certificated courses in the table
                		$tmp_course_title = $course->post_title;
                		if (strlen($tmp_course_title) > MAX_CHARACTERS_COURSE_TITLE) {
                			$tmp_short_course_title = substr($tmp_course_title, 0, MAX_CHARACTERS_COURSE_TITLE) . ETC_DOTS;
                		} else {
                			$tmp_short_course_title = $tmp_course_title;
                		}
                		// Get url of course page
                		$tmp_permalink =  $course->permalink;
                		// Get location
                		$tmp_location_list = $course->location;
                		// Check expired time of the course
                		$tmp_is_expiry = $course->is_expiry;
                		// Get custom date
                		$tmp_custom_date = $course->custom_date;
                		// Get trainer
                		$tmp_trainer_name = $course->trainer_name;
                		$tmp_trainer_profile = $course->trainer_profile;
                		// Get hosted by value
                		$tmp_hosted_by = $course->hosted_by;
                		// Getting certificate icon of the course
                		$cert_icon = $course->certified_icon;
                		// Get Register url of Epicoaching course
                		$tmp_epicoaching_reg_url = $course->epicoaching_reg_url;
                		$tmp_certified_flag = $course->certified_flag;
                		?>
                	    <tr>
                	    <td class="coures-name-col"><a title="<?php echo $tmp_course_title;?>"
                	        href="<?php echo $tmp_permalink;?>"><?php echo $tmp_short_course_title;?></a>
                	    </td>
                	    
                	    <td class="course-cert-icon" >
                	    <?php if ($tmp_certified_flag == 1) echo $cert_icon;?>
                		</td>
                	    <td class="course-date-col"><?php echo $tmp_custom_date;?></td>
                	    <td><?php echo $tmp_location_list;?></td>
                	    <td><a class="color-inherit" href="<?php echo $tmp_trainer_profile;?>"
                	        target="_blank"><?php echo $tmp_trainer_name;?></a></td>
                	    <td><?php echo $tmp_hosted_by;?></td>
                	    <td class="course-register-col">
                	    <?php if (!$tmp_is_expiry && $tmp_hosted_by == EPICOACHING) {?>
                	    <a
                		href="<?php echo $tmp_epicoaching_reg_url;?>"
                		rel="nofollow" target="_blank"><?php echo $text_register;?>
                		</a>
                	    <?php } else if (!$tmp_is_expiry) {?>
                	    <a
                		    onclick="loadCourseInfoIntoRegisterPopup(
                	    	 '<?php echo $tmp_course_title;?>','<?php echo $tmp_hosted_by;?>',<?php echo $tmp_certified_flag;?>,'<?php echo $tmp_custom_date?>','<?php echo $tmp_location_list;?>','<?php echo $tmp_trainer_name;?>')"
                			href="#login-box" class="register-login <?php echo $tmp_is_expiry == 1?STYLE_CLASS_EXPIRY_ON:STYLE_CLASS_EXPIRY_OFF;?>">
                			<?php echo $text_register;?>
                	    </a>
                	    <?php }?>
                	    </td>
                	</tr>
             <?php 
                 }// End foreach showing list of certificted course
             ?>
            </table>
			<?php } else {
                    // End checking empty list of upcomming courses. If it's empty, showing message.
            		?>
                <b><?php echo $msg_empty_training_courses;?></b>
                <?php  
                 }
            ?>
        </div>
    </div>
</div>