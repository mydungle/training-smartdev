<?php /*Template Name: Checkout Template*/?>
<?php get_header();
	session_start();
	
	$tpl_default_settings = get_post_meta( $post->ID, '_tpl_default_settings', TRUE );
	$tpl_default_settings = is_array( $tpl_default_settings ) ? $tpl_default_settings  : array();

	$page_layout  = array_key_exists( "layout", $tpl_default_settings ) ? $tpl_default_settings['layout'] : "content-full-width";
	$show_sidebar = $show_left_sidebar = $show_right_sidebar =  false;
	$sidebar_class = $thumbnail_sidebar = $post_thumbnail = "";

	switch ( $page_layout ) {
		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
			$thumbnail_sidebar = "-single-sidebar";
		break;

		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar	= true;
			$sidebar_class = "secondary-has-right-sidebar";
			$thumbnail_sidebar = "-single-sidebar";
		break;

		case 'both-sidebar':
			$page_layout = "page-with-sidebar page-with-both-sidebar";
			$show_sidebar = $show_right_sidebar	= $show_left_sidebar = true;
			$sidebar_class = "secondary-has-both-sidebar";
			$thumbnail_sidebar = "-both-sidebar";
		break;

		case 'content-full-width':
		default:
			$page_layout = "content-full-width";
			$thumbnail_sidebar = "";
		break;
	}

	if ( $show_sidebar ):
		if ( $show_left_sidebar ): ?>
			<!-- Secondary Left -->
			<section id="secondary-left" class="secondary-sidebar <?php echo $sidebar_class;?>"><?php get_sidebar( 'left' );?></section><?php
		endif;
	endif;?>

	<!-- ** Primary Section ** -->
<section id="primary" class="<?php echo $page_layout;?>">

<form method="post" action="">
    <h2><?php echo __('Please Select Payment method');?></h2>
    <div class="dt-sc-clear"></div>
      <div class="clearfix" id="fieldset_parrent">
        <p>
          <input name="payment" value="paypal" type="radio" />
          <span><?php echo __('Paypal');?></span>
        </p>
	        <fieldset id="paypal">
				<?php 
			$ID= $_SESSION['ID'];
			$starting_price = $_SESSION['starting_price'];
			$currency = $_SESSION['currency'];

			$paypal_dec = __('You are about to purchase the Course : ', 'dt_themes').get_the_title($ID);
			$paypal_sc = do_shortcode("[s2Member-PayPal-Button level='1' ccaps='cid_{$ID}' desc='{$paypal_dec}' ps='paypal' lc='' cc='{$currency}' dg='0' ns='1' custom='".$_SERVER["HTTP_HOST"]."' ta='0' tp='0' tt='D' ra='{$starting_price}' rp='1' rt='L' rr='BN' rrt='' rra='1' image='' output='url'/]");

			echo '<a href="'.$paypal_sc.'" target="_self"  class="dt-sc-button small filled">';?>
			<img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/PP_logo_h_150x38.png" alt="Buy now with PayPal" />
		</a>
		</fieldset>
        <p>
          <input name="payment" value="bank" type="radio" />
          <span><?php echo __('Bank credit transfer');?></span>
        </p>
        <fieldset id="bank">
		<p><?php echo __('Please transfer your money to our bank account with infomation given as below:');?></p>
		<?php the_content();?>
		</fieldset>

      </div>
</form>	
<script type="text/javascript">
jQuery(function ($) {
	$('#fieldset_parrent fieldset').hide();
	
	$('input[name="payment"]').click(function(){	
		$('#fieldset_parrent fieldset').hide();
		var fs = $(this).attr('value');
		if(this.checked){
			$('fieldset#'+fs).show();
		}	
		
	});
});
</script>

<?php get_footer(); ?>
