<?php
	include 'utilities.php';
	function showDialog()
	{
	  echo ''?>
	  <style >
	    .ui-dialog-titlebar {
	      display: none;
	    }
	  </style>
	  
	  <div>
	    <div id="login-box" class="container-dialog row">
	      <div class="row title-dialog">
	        <div class="row title-course">KHÓA HỌC ĐÃ CHỌN</div>
	        <div class="row container-name-course" id="iconsourse">
	          <img class="img-course" src="<?php echo get_template_directory_uri() . '/images/customize/ico1.png';?>" />
	          <span id="name-course"></span>
	        </div>
	        <div class="row">
	          <div class="row title-form">Thông tin người tham gia</div>
	        </div>
	        <div class="container-input">
	          <div class="row">
	            <div class="col-md-4 label-form">Tên *
	              <input class="in-address" id="firstname" type="text"/>
	            </div>
	            <div class="col-md-4 label-form">Họ *
	              <input class="in-address" id="lastname" type="text"/>
	            </div>
	            <div class="col-md-4 label-form">Anh/Chị *
	              <input class="in-address" id="gender" type="text" />
	            </div>
	          </div>
	          <div class="row">
	            <div class="col-md-6 label-form">Nghề nghiệp *
	              <input class="in-address" id="jobtitle" type="text"/></div>
	              <div class="col-md-6 label-form">Công ty *
	                <input class="in-address" id="company" type="text"/>
	              </div>
	            </div>
	            <div class="row ">
	              <div class="col-md-6 label-form">Email *
	                <input class="in-address" id="email" type="text" /></div>
	                <div class="col-md-6 label-form">Số điện thoại *
	                  <input class="in-address" id="workphone" type="text"/>
	                </div>
	              </div>
	              <div class="row ">
	                <div class="col-md-12 label-form">Địa chỉ *&nbsp;&nbsp;
	                </div>
	              </div>
	              <div class="row margin-address">
	              	<input class="in-address" id="address" type="text"/>
	              </div>
	              <div class="row margin-address">
	                <span class="message-color">*Thông tin bắt buộc</span>
	              </div>
	              <div class="row margin-address">
	                <span class="message-color1" id="message"></span>
	              </div>
	              <div class="row margin-address">
	                <div id="submit" type="button" class="dt-sc-button filled">Gửi</div>
	              </div>
	              <input type="hidden" name="language_code" value="<?php echo LANGUAGE_CODE_VN;?>">
	            </div>
	          </div>
	        </div>
	      </div>
	      <?php
	    }
	
	    ?>
	<?php
    function showSussecc()
    {
      echo ''?>
      <div>
        <div id="successdg" class="success-dialog row">
          <div class="container-img">
           <img class="img-success" src="<?php echo get_template_directory_uri() . '/images/customize/success-icon.png';?>" />
         </div>
         <div class="message-success"><p>Cảm ơn! Bạn đã đăng ký <b>thành công.</b></br> Chúng tôi sẽ liên lạc với bạn sau vài ngày.</p></div>
         <div><button id="btn-success-ok" type="button" class="btn btn-primary btn-custom2">OK</button></div>
       </div>
     </div>
     <?php
    }

    ?>
	<?php
	function showErr()
	{
	    echo ''?>
	    <div>
	    <div id="errodg" class="success-dialog row">
	        <div class="container-img">
	         <img class="img-success" src="<?php echo get_template_directory_uri() . '/images/customize/failed-icon.png';?>" />
	       </div>
	       <div class="message-success"><p>Rất tiếc, đăng ký <b>thất bại</b> do lỗi hệ thống. <br/>Xin vui lòng đăng ký lại.</p></div>
	       <div><button id="btn-success-ok" type="button" class="btn btn-primary btn-custom2">OK</button></div>
	     </div>
	   </div>
	   <?php
	 }
	 ?>
	
	 <?php
	 function showloadWaiting()
	 {
	 	echo ''?>
	 		  <div id="dvLoading" >
	 		    <img style="" class="loading-sendmail" src="<?php echo get_template_directory_uri().'/images/customize/loading.gif';?>" />
	 		  </div>
	 		  <?php
	 }
	 function addDialog(){
	 	showSussecc();
	 	showDialog();
	 	showloadWaiting();
	 	showErr();
	 }
?>

