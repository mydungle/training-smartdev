<?php
/**
 * This file is to get list of courses by trainer id which set in the previous file where this file imported.
 * @param $trainer_id a variable set in the previous file.
 * @author dung.le
 */
	// Define text translated by String Translations
	$curr_course_category = __('public-courses', 'dt_themes');
	// Get all coures conducted by the trainer
	$args = array (
			'category_name' => $curr_course_category,
			'meta_key' => CUSTOM_FIELD_COURSE_UNIX_START_DATE,
			'orderby' => 'meta_value post_title',
			'order' => 'DESC',
			'post_type' => POST_TYPE_TRAINING_COURSE,
			'meta_query' => array(
					array(
							'key' => CUSTOM_FIELD_COURSE_TRAINER,
							'value' => $trainer_id,
							'compare' => '='
					)
			),
			'post_status' => 'publish',
			'posts_per_page' => - 1
	);
	// dung.le
	$courses = get_posts ( $args );
	// Define two cases for courses: archived courses and upcoming
	$archived = [];
	$upcoming = [];
	$plan = [];
	$error = 0;
	// Go through course list to divide into three different groups: archived, upcoming and finalising
	foreach ($courses as $course) {
		// Renew object for data shown in course table
		$object = new stdClass();
        
		// Prepare data to show list of certificated courses in the table
		$id = $course->ID;
		// Get detail information such as Date, trainer, location
		$tmp_custom_fields = get_post_custom($id);
		// Get course catalog id
		// Get basic course information form catalog if this course belongs to an available course catalog
		$catalog_id = $tmp_custom_fields[CUSTOM_FIELD_COURSE_CATALOG_ID];
		if (is_array($catalog_id)) {
			$catalog_id = (int) $catalog_id[0];
		} else {
			$catalog_id =  0;
		}
		if ($catalog_id > 0) {
			// Getting basic course information from the course catalog
			$catalog = get_post_meta($catalog_id);
			$basic_catalog_info = get_post($catalog_id);
			// Get trainer
            $trainer_id = $catalog[CUSTOM_FIELD_COURSE_TRAINER] [0];
	        $translated_trainer_id = icl_object_id($trainer_id, 'page', true, ICL_LANGUAGE_CODE);
	        if ($translated_trainer_id > 0) $trainer_id = $translated_trainer_id;
	        $trainer_name = get_the_title($trainer_id);
	        $trainer_profile = get_the_permalink($trainer_id);
			if (strlen($tmp_trainer_name) <= 0) $tmp_trainer_name = CHARACTER_MINUS;
			if (strlen($trainer_profile) <= 0) $trainer_profile = CHARACTER_MINUS;
			// Get hosted by value
			$hosted_by = $catalog[CUSTOM_FIELD_COURSE_HOSTED_BY];
			if (!is_array($hosted_by)) $hosted_by = '';
			else $hosted_by = $hosted_by[0];
			// Get Register url of Epicoaching course
			$epicoaching_reg_url = $catalog[CUSTOM_FIELD_COURSE_EPICOACHING_REGISTER_URL];
			if ($epicoaching_reg_url) {
				$epicoaching_reg_url = $epicoaching_reg_url[0];
			} else {
				$epicoaching_reg_url = '';
			}
			// Get cefitified icon
			$cert_icon = $catalog[CUSTOM_FIELD_COURSE_CERTIFIED_ICON][0];
			if (strlen($cert_icon) > 0) {
				$cert_icon = '<img src="'.$cert_icon.'" />';
				$tmp_certfified_flag = 1;
			} else {
				$cert_icon = '';
				$tmp_certfified_flag = 0;
			}
		} else {
			$trainer_name = CHARACTER_MINUS;
			$trainer_profile = CHARACTER_MINUS;
			$hosted_by = '';
			$epicoaching_reg_url = '';
			$cert_icon = '';
			$tmp_certfified_flag = 1;
		}
		// Get location
		$tmp_location = unserialize($tmp_custom_fields [CUSTOM_FIELD_COURSE_LOCATION] [0]);
		$arr = array();
		if (is_array ( $tmp_location )) {
			foreach($tmp_location as $lc) {
				if ($lc != DEFAULT_VALUE_SELECT_NONE_VALUE) $arr[] = $lc;
			}
			$tmp_location = $arr;
			if (count($tmp_location) > 0) {
				$tmp_location_list = implode ( ', ', $tmp_location );
			} else {
				$tmp_location_list = '';
			}
		} else {
			$tmp_location_list = '';
		}
		// Get expiry date
		$tmp_end_date = $tmp_custom_fields[CUSTOM_FIELD_COURSE_END_DATE];
		if ($tmp_end_date) {
			$tmp_end_date = $tmp_end_date[0];
			// Check expired time of the course
			$tmp_is_expiry = is_expiry($tmp_end_date);
		} else {
			$tmp_end_date = '';
			$tmp_is_expiry = 0;
		}
		
		// Get custom date
		$tmp_custom_date = $tmp_custom_fields [CUSTOM_FIELD_COURSE_CUSTOM_DATE] [0];
		if (strlen($tmp_custom_date) <= 0) $tmp_custom_date = __('Coming soon', 'dt_themes');
		if (strlen($tmp_location_list) <= 0) $tmp_location_list = CHARACTER_MINUS;
		if (strlen($hosted_by) <= 0) $hosted_by = CHARACTER_MINUS;
		// Checking course is certified or not.
		
		
		$object->id = $id;
		$object->course_catalog_id = $catalog_id;
		$object->post_title = $course->post_title;
		$object->permalink = get_permalink($id);
		$object->certified_icon = $cert_icon;
		$object->trainer_name = $trainer_name;
		$object->trainer_profile = $trainer_profile;
		$object->hosted_by = $hosted_by;
		$object->location = $tmp_location_list;
		$object->custom_date = $tmp_custom_date;
		$object->is_expiry = $tmp_is_expiry;
		$object->epicoaching_reg_url = $epicoaching_reg_url;
		$object->certified_flag = $tmp_certfified_flag;
		
		// Get expiry date to divide into three different course groups: upcoming, finalising and archived
		$tmp_start_date = $tmp_custom_fields['_start_date'][0];
		$tmp_end_date = $tmp_custom_fields['_end_date'][0]; 
		// Checking condition of these groups
		$is_plan = is_plan($tmp_start_date);
		$is_comming = is_comming($tmp_start_date);
		$is_done = is_expiry($tmp_start_date);
		if ($is_done == 1) {
			$archived[] = $object;
		} else if ($is_comming == 1) {
			$upcoming[] = $object;
		} else if ($is_plan) {
			$plan[] = $object;
		} else {
			$error += 1;
		}
	}
?>