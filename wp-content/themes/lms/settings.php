<?php

// Language codes
define('LANGUAGE_CODE_VN', 'vi');
define('LANGUAGE_CODE_ENGLISH', 'en');

define('STYLE_CLASS_EXPIRY_ON', 'expiry-on');
define('STYLE_CLASS_EXPIRY_OFF', 'expiry-off');
define('VN_MESSAGE_REGISTER', 'Đăng ký');
// Default timezone
define('DEFAULT_TIMEZONE', 'Asia/Bangkok');
define('DATE_TIME_FORMAT', 'M d Y H:i:s');
define('START_TIME_FORMAT', '00:00:01');
define('END_TIME_FORMAT', '23:59:59');

// Field names
define('POST_TYPE_TRAINING_COURSE', 'training-course' );
define('POST_TYPE_COURSE_CATALOG', 'training-catalog' );
define('POST_TYPE_COACH', 'dt_teachers');
define('POST_TYPE_ONLINE_COURSE', 'dt_courses');
define('CATEGORY_CERTIFICATED_COURSE', 'certified-courses' );
define('CATEGORY_NON_CERTIFICATED_COURSE', 'non-certified-courses' );
define('CATEGORY_AGILE_COACHING', 'agile-training-and-coaching');
define('CATEGORY_COURSE_CATALOG_CERTIFIED', 'certified-course-catalog');
define('CATEGORY_COURSE_CATALOG_NON_CERTIFIED', 'non-certified-course-catalog');
define('CATEGORY_COURSE_CATALOG_CERTIFIED_NAME', 'Certified Course');
define('CATEGORY_COURSE_CATALOG_NON_CERTIFIED_NAME', 'Non Certified Course');

define('VN_CATEGORY_COURSE_CATALOG_CERTIFIED_NAME', 'Danh mục khóa học được cấp chứng nhận');
define('VN_CATEGORY_COURSE_CATALOG_NON_CERTIFIED_NAME', 'Danh mục khóa học không kèm chứng nhận');
define('VN_CATEGORY_COURSE_CATALOG_CERTIFIED', 'vn-certified-course-catalog');
define('VN_CATEGORY_COURSE_CATALOG_NON_CERTIFIED', 'vn-non-certified-course-catalog');
define('VN_CATEGORY_CERTIFICATED_COURSE', 'vn-certified-courses' );
define('VN_CATEGORY_NON_CERTIFICATED_COURSE', 'vn-non-certified-courses' );

define('CUSTOM_FIELD_COURSE_UNIX_START_DATE','_unix_start_date');
define('CUSTOM_FIELD_COURSE_UNIX_END_DATE','_unix_end_date');

define('CUSTOM_FIELD_COURSE_LOCATION', '_location');
define('CUSTOM_FIELD_COURSE_END_DATE', '_end_date');
define('CUSTOM_FIELD_COURSE_START_DATE', '_start_date');
define('CUSTOM_FIELD_COURSE_CUSTOM_DATE', '_custom_date');
define('CUSTOM_FIELD_COURSE_CATALOG_ID', '_course_catalog_id');
define('CUSTOM_FIELD_COURSE_PHOTO_SLIDER_ID', '_public_course_slider_id');
define('CUSTOM_FIELD_COURSE_CERTIFIED_ICON', '_certificate_icon');
define('CUSTOM_FIELD_COURSE_EPICOACHING_REGISTER_URL', '_epicoaching_register_url');
define('CUSTOM_FIELD_COURSE_TRAINER', '_trainer');
define('CUSTOM_FIELD_COURSE_TRAINER_NAME', 'trainer_name');
define('CUSTOM_FIELD_COURSE_TRAINER_PROFILE_URL', 'profile_url');
define('CUSTOM_FIELD_COURSE_HOSTED_BY', '_hosted_by');
define('CUSTOM_FIELD_ONLINE_PURCHASED_COURSE', '_purchased_course');

// Form - fields
define('FORM_CUSTOM_FIELD_TRAINING_COURSE_PHOTOS_FIELD', 'training_course_photos_field');

define('MESSAGE_COURSE_CATALOG_SELECT_BOX', 'Please select a course catalog');
define('MESSAGE_COURSE_PHOTOS_SELECT_BOX', 'Please select a course for loading its photos');
define('MESSAGE_COURSE_COACHES_SELECT_BOX', 'Please select a trainer from the list');
define('CHARACTER_MINUS', '-');

define('DEFAULT_VALUE_SELECT_NONE_VALUE', 'None');
define('MAX_CHARACTERS_COURSE_TITLE', 102);
define('MAX_COURSES_IN_HOME_PAGE', 3);
// initialize maximum sub course packages shown
define('SUB_COURSE_PACKAGES_MAX', 3 );
// Define the maximum amount of columns to show sub packages
define('MAX_SUB_COURSE_PACKAGE_COLUMN', 3 );
// Define total of columns per row
define('COLUMNS_PER_ROW', 12 );
// Maximum amount of logos shown in detail course page
define('MAX_COURSE_LOGOS', 2 );
define('OTHER_CERTIFICATED_COURSES', 'Other Certified Courses');
define('OTHER_NON_CERTIFICATED_COURSES', 'Other Non-certified Courses');



define('ETC_DOTS', '...');
define('EPICOACHING', 'Epicoaching');

// Titles
define('TEXT_CERTIFIED_COURSES', 'Certified Courses');
define('TEXT_NON_CERTIFIED_COURSES', 'Non-certified Courses');
define('TEXT_COURSE_INFORMATION', 'Course information');
define('TEXT_UPCOMING', 'Upcoming');
define('TEXT_WILL_COME', 'Finalizing');
define('TEXT_ARCHIVED', 'Archived');
define('TEXT_DOWNLOAD_COURSE', 'Download this course');
define('TEXT_VIEW_ALL_COURSES', 'View course catalog');
define('TEXT_COURSE_NAME', 'Course name');
define('TEXT_HOST', 'Host');
define('TEXT_DOWNLOAD', 'Download');
define('TEXT_ALL_COURSES', 'All courses');
define('TEXT_LANGUAGE', 'Language');
define('TEXT_DURATION', 'Duration');
define('TEXT_UNIT_PRICE', 'Unit price');
define('TEXT_BATCH_SIZE', 'Batch size');
define('TEXT_HOSTED_BY', 'Hosted by');
define('TEXT_OVERVIEW', 'Overview');
define('TEXT_AND', 'and');
define('TEXT_LOCATION', 'Location');
define('TEXT_DATE', 'Date');
define('TEXT_EXCLUDED_TAX', 'Excluded tax');
define('TEXT_REGISTER', 'Register');
define('TEXT_VENUE', 'Venue');
define('TEXT_TRAINER', 'Trainer');
define('TEXT_PUBLIC_COURSES', 'Public courses');
// Title for vn page
define('VN_TEXT_CERTIFIED_COURSES', 'Khóa học được cấp chứng nhận');
define('VN_TEXT_NON_CERTIFIED_COURSES', 'Khóa học không kèm chứng nhận');
define('VN_TEXT_UPCOMING', 'Sắp đến');
define('VN_TEXT_WILL_COME', 'Đang xếp lịch');
define('VN_TEXT_ARCHIVED', 'Đã qua');
define('VN_TEXT_DOWNLOAD_COURSE', 'Tải xuống khóa học');
define('VN_TEXT_VIEW_ALL_COURSES', 'Xem danh mục các khóa học');
define('VN_TEXT_COURSE_NAME', 'Khóa học');
define('VN_TEXT_DATE', 'Thời gian');
define('VN_TEXT_TRAINER', 'Điều phối viên');
define('VN_TEXT_LOCATION', 'Địa điểm');
define('VN_TEXT_HOST', 'Đào tạo');
define('VN_TEXT_DOWNLOAD', 'Tải xuống');
define('VN_TEXT_ALL_COURSES', 'Tất cả các khóa học');
define('VN_OTHER_CERTIFICATED_COURSES', 'Các khóa học được cấp chứng nhận khác');
define('VN_OTHER_NON_CERTIFICATED_COURSES', 'Các khóa học không kèm chứng nhận khác');
define('VN_TEXT_COURSE_INFORMATION', 'Thông tin khóa học');
define('VN_TEXT_LANGUAGE', 'Ngôn ngữ');
define('VN_TEXT_DURATION', 'Thời lượng');
define('VN_TEXT_UNIT_PRICE', 'Học phí');
define('VN_TEXT_BATCH_SIZE', 'Số lượng');
define('VN_TEXT_HOSTED_BY', 'Đơn vị tổ chức');
define('VN_TEXT_OVERVIEW', 'Tổng quan');
define('VN_TEXT_FACILITATORS', 'Điều phối viên');
define('VN_TEXT_OBJECTIVES', 'Mục tiêu');
define('VN_TEXT_WE_HAND_TO_YOU', 'Bạn sẽ mang về');
define('VN_TEXT_ATTENDERS', 'Ai nên tham gia?');
define('VN_TEXT_PREREQUISITES', 'Khuyến cáo trước khi tham gia');
define('VN_TEXT_VENUE', 'Địa điểm');
define('VN_TEXT_AND', 'và');
define('VN_TEXT_EXCLUDED_TAX', 'Chưa bao gồm thuế');
define('VN_TEXT_REGISTER', 'Đăng ký');
define('VN_TEXT_PUBLIC_COURSES', 'Khóa học công cộng');

// Messages
define('MSG_EMPTY_TRAINING_COURSES', 'No Courses Found!');
define('VN_MSG_EMPTY_TRAINING_COURSES', 'Không có khóa học nào.');

// Fixed url
define('URL_VIEW_ALL_COURSES', '/course-catalog');
define('VN_URL_VIEW_ALL_COURSES', '/vn/dich-vu/dao-tao-va-huan-luyen-agile/danh-muc-khoa-hoc/');
//Title of image slide in detail training course
define('VN_TEXT_TITLE_SLIDE_IMAGE_DETAIL_COURSE', 'Một số hình ảnh của khóa học');
define('TEXT_TITLE_SLIDE_IMAGE_DETAIL_COURSE', 'Some photos from this course');
?>
