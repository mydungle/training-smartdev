<?php

add_shortcode('map_wrapper','map_wrapper');
function map_wrapper( $attrs = null, $content = null,$shortcodename ="" ){
	return "<div class='map-wrapper'></div>";
}

/** fblike
  * Objective:
  *		1.Facebook Widget.
**/
add_shortcode('fblike','fblike');
function fblike( $attrs = null, $content = null,$shortcodename ="" ){
	extract(shortcode_atts(array('layout'=>'box_count','width'=>'','height'=>'','send'=>false,'show_faces'=>false,'action'=>'like','font'=> 'lucida+grande'
				,'colorscheme'=>'light'), $attrs));

	if ($layout == 'standard') { $width = '450'; $height = '35';  if ($show_faces == 'true') { $height = '80'; } }
	if ($layout == 'box_count') { $width = '55'; $height = '65'; }
	if ($layout == 'button_count') { $width = '90'; $height = '20'; }
	$layout = 'data-layout = "'.$layout.'" ';
	$width = 'data-width = "'.$width.'" ';
	$font = 'data-font = "'.str_replace("+", " ", $font).'" ';
	$colorscheme = 'data-colorscheme = "'.$colorscheme.'" ';
	$action = 'data-action = "'.$action.'" ';
	if ( $show_faces ) { $show_faces = 'data-show-faces = "true" '; } else { $show_faces = ''; }
	if ( $send ) { $send = 'data-send = "true" '; } else { $send = ''; }
	
    $out = '<div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));</script>';
	$out .= '<div class = "fb-like" data-href = "'.get_permalink().'" '.$layout.$width.$font.$colorscheme.$action.$show_faces.$send.'></div>';
return $out;
}


/** googleplusone
  * Objective:
  *		1.googleplusone Widget.
**/
add_shortcode('googleplusone','googleplusone');	
function googleplusone( $attrs = null, $content = null,$shortcodename ="" ){
	extract(shortcode_atts(array('size'=> '','lang'=> ''), $attrs));
	$size = empty($size) ? "size='small'" : "size='{$size}'";
	$lang = empty($lang) ? "{lang:en_GB}" : "{lang:'{$lang}'}";
	
	$out = '<script type="text/javascript" src="https://apis.google.com/js/plusone.js">'.$lang.'</script>';
	$out .= '<g:plusone '.$size.'></g:plusone>';
	return $out;
}

/** twitter
  * Objective:
  *		1.twitter Widget.
**/
add_shortcode('twitter','twitter');
function twitter( $attrs = null, $content = null,$shortcodename ="" ){
	extract(shortcode_atts(array('layout'=>'vertical','username'=>'','text'=>'','url'=>'','related'=> '','lang'=> ''), $attrs));
	
	$p_url= get_permalink();
	$p_title = get_the_title();
	
	$text = !empty($text) ? "data-text='{$text}'" :"data-text='{$p_title}'";
	$url = !empty($url) ? "data-url='{$url}'" :"data-url='{$p_url}'";
	$related = !empty($related) ? "data-related='{$related}'" :'';
	$lang = !empty($lang) ? "data-lang='{$lang}'" :'';
	$twitter_url = "http://twitter.com/share";
		$out = '<a href="{$twitter_url}" class="twitter-share-button" '.$url.' '.$lang.' '.$text.' '.$related.' data-count="'.$layout.'" data-via="'.$username.'">'.
	__('Tweet','dt_themes').'</a>';
		$out .= '<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>';
	return $out;	
}

/** stumbleupon
  * Objective:
  *		1.Stumbleupon Widget.
**/
add_shortcode('stumbleupon','stumbleupon');
function stumbleupon( $attrs = null, $content = null,$shortcodename ="" ){
	extract(shortcode_atts(array('layout'=>'5','url'=>get_permalink()),$attrs));
	$url = "&r='{$url}'";
	$out = '<script src="http://www.stumbleupon.com/hostedbadge.php?s='.$layout.$url.'"></script>';
	return $out;	
}

/** linkedin
  * Objective:
  *		1.linkedin Widget.
**/
add_shortcode('linkedin','linkedin');
function linkedin( $attrs = null, $content = null,$shortcodename ="" ){
	extract(shortcode_atts(array('layout'=>'2','url'=>get_permalink()),$attrs));
	
    	if ($url != '') { $url = "data-url='".$url."'"; }
	    if ($layout == '2') { $layout = 'right'; }
		if ($layout == '3') { $layout = 'top'; }
		$out = '<script type="text/javascript" src="http://platform.linkedin.com/in.js"></script><script type="in/share" data-counter = "'.$layout.'" '.$url.'></script>';
	return $out;	
}

/** delicious
  * Objective:
  *		1.Delicious Widget.
**/
add_shortcode('delicious','delicious');
function delicious( $attrs = null, $content = null,$shortcodename ="" ){
	extract(shortcode_atts(array('text'=>__("Delicious",'dt_themes')),$attrs));
	
	$delicious_url = "http://www.delicious.com/save";
	
	$out = '<img src="http://www.delicious.com/static/img/delicious.small.gif" height="10" width="10" alt="Delicious" />&nbsp;<a href="{$delicious_url}" onclick="window.open(&#39;http://www.delicious.com/save?v=5&noui&jump=close&url=&#39;+encodeURIComponent(location.href)+&#39;&title=&#39;+encodeURIComponent(document.title), &#39;delicious&#39;,&#39;toolbar=no,width=550,height=550&#39;); return false;">'.$text.'</a>';
	return $out;	
}

/** pintrest
  * Objective:
  *		1.Pintrest Widget.
**/
add_shortcode('pintrest','pintrest');
function pintrest( $attrs = null, $content = null,$shortcodename ="" ){
	extract(shortcode_atts(array('text'=>get_the_excerpt(),'layout'=>'horizontal','image'=>'','url'=>get_permalink(),'prompt'=>false),$attrs));
	$out = '<div class = "mysite_sociable"><a href="http://pinterest.com/pin/create/button/?url='.$url.'&media='.$image.'&description='.$text.'" class="pin-it-button" count-layout="'.$layout.'">'.__("Pin It",'dt_themes').'</a>';
	$out .= '<script type="text/javascript" src="http://assets.pinterest.com/js/pinit.js"></script>';
	
	if($prompt):
		$out = '<a title="'.__('Pin It on Pinterest','dt_themes').'" class="pin-it-button" href="javascript:void(0)">'.__("Pin It",'dt_themes').'</a>';
		$out .= '<script type = "text/javascript">';
		$out .= 'jQuery(document).ready(function(){';
			$out .= 'jQuery(".pin-it-button").click(function(event) {';
			$out .= 'event.preventDefault();';
			$out .= 'jQuery.getScript("http://assets.pinterest.com/js/pinmarklet.js?r=" + Math.random()*99999999);';
			$out .= '});';
		$out .= '});';
		$out .= '</script>';
		$out .= '<style type = "text/css">a.pin-it-button {position: absolute;background: url(http://assets.pinterest.com/images/pinit6.png);font: 11px Arial, sans-serif;text-indent: -9999em;font-size: .01em;color: #CD1F1F;height: 20px;width: 43px;background-position: 0 -7px;}a.pin-it-button:hover {background-position: 0 -28px;}a.pin-it-button:active {background-position: 0 -49px;}</style>';
	
	endif;
	return $out;
}

/** digg
  * Objective:
  *		1.Digg Widget.
**/
add_shortcode('digg','digg');
function digg( $attrs = null, $content = null,$shortcodename ="" ){
	extract(shortcode_atts(array('layout'=>'DiggMedium','url'=>get_permalink(),'title'=>get_the_title(),'type'=>'','description'=>get_the_content(),'related'=>''),$attrs));
	
	if ($title != '') { $title = "&title='".$title."'"; }
	if ($type != '') { $type = "rev='".$type."'"; }
	if ($description != '') { $description = "<span style = 'display: none;'>".$description."</span>"; }
	if ($related != '') { $related = "&related=no"; }

	$out = '<a class="DiggThisButton '.$layout.'" href="http://digg.com/submit?url='.$url.$title.$related.'"'.$type.'>'.$description.'</a>';
	$out .= '<script type = "text/javascript" src = "http://widgets.digg.com/buttons.js"></script>';
	return $out;
}

add_shortcode('dt_sc_social','dt_sc_social'); 
function dt_sc_social($attrs, $content=null,$shortcodename="") {
	$dttheme_options = get_option(IAMD_THEME_SETTINGS);
	if( isset($dttheme_options['general']['show-sociables']) && isset($dttheme_options['social']) ):
		$out = "<ul class='social-icons'>";
			foreach($dttheme_options['social'] as $social):
				$link = $social['link'];
				$icon = $social['icon'];
				$out .= '<li><a href="'.esc_url($link).'" target="_blank">';
				$out .= '<span class="fa '.$icon.'"></span>';
				$out .= "</a></li>"; 
			endforeach;
			// dung.le Adding instagram icon into the list
			$out .= '<li><a href="'.esc_url('http://instagram.com/smartdevllc/').'" target="_blank">';
			$out .= '<span class="fa fa-instagram"></span>';
			$out .= "</a></li>";
		$out .= "</ul>";
	return $out;	
	endif;	
}

// dung.le create some shortcodes
/**
 * Create a shortcodes to show list of recent certified courses.
 * @return HTML code for list of recent certfified courses.
 */
function display_recent_public_courses() {
	$curr_cert_course_category = __('certified-courses', 'dt_themes');
	// Getting recent courses
	$args = array (
			'category_name' => $curr_cert_course_category,
			'meta_key' => CUSTOM_FIELD_COURSE_UNIX_START_DATE,
			'orderby' => 'meta_value post_title',
			'order' => 'DESC',
			'post_type' => POST_TYPE_TRAINING_COURSE,
			'post_status' => 'publish',
			'posts_per_page' => 3
	);
	$string = '';
	$query = new WP_Query( $args );
	if( $query->have_posts() ){
		$string .= '<div class="widget widget_popular_entries"><div id="recent-public-courses-mega-menu" class="recent-course-widget"><ul>';
		while( $query->have_posts() ){
			$query->the_post();
			$title = ( strlen(get_the_title()) > 20 ) ? substr(get_the_title(),0,15)."..." :get_the_title();
			$string .=
			'<li>';
			if(has_post_thumbnail()):
			$image_url = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'dt-course-widget');
			$string .= '<img src="'.$image_url[0].'" alt="'.get_the_title().'" width="'.$image_url[1].'" height="'.$image_url[2].'" />';
			else:
			$string .= '<img src="http://placehold.it/110x90&text=Image" alt="'.get_the_title().'" />';
			endif;
			$string .=
			'<h6>'
			  .'<a href="'.get_permalink().'">'.$title
			  .'</a>'
			  		.'</li>';
		}
		$string .= '</ul></div></div>';
	}
	wp_reset_query();
	return $string;
}
add_shortcode( 'recent-public-courses', 'display_recent_public_courses' );
/**
 * Create shortcode to show a list of public certified courses under tabs.
 * @author dung.le
 */
function display_public_certified_courses_shortcode() {
	ob_start();
	include(locate_template('get-certified-courses.php'));
	include(locate_template('public-courses-table-template.php'));
	return ob_get_clean();
}
add_shortcode('public_certified_courses_shortcode', 'display_public_certified_courses_shortcode' );
/**
 * Create shortcode to show a list of public non-certified courses under tabs.
 * @author dung.le
 */
function display_public_non_certified_courses_shortcode() {
	ob_start();
	include(locate_template('get-non-certified-courses.php'));
	include(locate_template('public-courses-table-template.php'));
	return ob_get_clean();
}
add_shortcode('public_non_certified_courses_shortcode', 'display_public_non_certified_courses_shortcode' );
/**
 * Create shortcode to show a list of public courses under tabs by trainer id.
 * @author dung.le
 */
function display_public_courses_by_trainer_id_shortcode($atts) {
	ob_start();
	extract( shortcode_atts( array(
			'type' => 'trainer_id'
	), $atts ) );
	
	if (isset($atts['block_title']) && $atts['block_title'] == 1) {
		echo '<h5 class="border-title ">'.__('Public courses', 'dt_themes').'<span></span></h5>';
	}
	$trainer_id = $atts['trainer_id'];
	$translated_trainer_id = icl_object_id($trainer_id, 'page', true, ICL_LANGUAGE_CODE);
	if ($translated_id > 0) $trainer_id = $translated_trainer_id;
	include(locate_template('get-public-courses-by-trainer-id.php'));
	include(locate_template('public-courses-table-template.php'));
	return ob_get_clean();
}
add_shortcode('public_courses_by_trainer_id_shortcode', 'display_public_courses_by_trainer_id_shortcode' );
/**
 * Generating a register popup.
 * @author dung.le
 */
function display_register_popup_courses_shortcode() {
	ob_start();
	showDialog();
	return ob_get_clean();
}
add_shortcode('register_popup_courses_shortcode', 'display_register_popup_courses_shortcode' );
/**
 * Create doshortcode for showing public courses.
 * @author dung.le
*/
function display_all_public_courses() {
	// Define texts.
	$text_certified_courses = __('Certified Courses', 'dt_themes');
	$text_non_certified_courses = __('Non-certified Courses', 'dt_themes');
	// Execute doshortcode
	$certified = do_shortcode('[public_certified_courses_shortcode]');
	$non_certified_courses = do_shortcode('[public_non_certified_courses_shortcode]');
	$result = '
	<div class="container">
    <div id="all-public-courses-wrapper">
                    <div class="other-certificated-courses certificate">
                        <div class="detail-content-item">
                            <h3>
                                '.$text_certified_courses.'
                            </h3>
                            <div class="detail-content-item-body">
                                '.$certified.'
                            </div>
                        </div>
                    </div>
                    <?php $certificate_flag = 0;?>
                   <div class="other-certificated-courses certificate">
                        <div class="detail-content-item">
                            <div class="detail-content-item-title"><h3>'
                            .$text_non_certified_courses.'
                            </h3></div>
                            <div class="detail-content-item-body">
                               	'.$non_certified_courses.'
                            </div>
                        </div>
                    </div>
         </div>
     </div>
			';
	return $result;
}
add_shortcode('all-public-courses', 'display_all_public_courses');

function display_view_public_course_catalog_button($atts) {
	// Define text translated by String translation
	$text_view_all_courses = __('View course catalog', 'dt_themes');
	extract( shortcode_atts( array(
			'type' => 'post_id'
	
	), $atts ) );
	$id = $atts['post_id'];
	$translated_id = icl_object_id($id, 'page', true, ICL_LANGUAGE_CODE);
	if ($translated_id > 0) $id = $translated_id;
	$url_view_all_courses  = get_the_permalink($id);
	return '<div class="container"><div id="view-all-course-wrapper">
                <a class="dt-sc-button small" href="'.$url_view_all_courses.'">'.$text_view_all_courses.'</a>
            </div></div>';
}
add_shortcode('view-public-course-catalog-button', 'display_view_public_course_catalog_button');

/**
 * Creating a doshortcode to show list of coaches without using ajax loading.
 * @param unknown $attrs
 * @param string $content
 * @return string
 */
function dt_custom_sc_teacher($attrs, $content = null) {

	extract(shortcode_atts(array(
			'columns' => '',
			'limit' => '',
			'post_id' => '',
			'carousel' => ''
	), $attrs));

	$columns = !empty($columns) ? $columns : '4';
	$limit = !empty($limit) ? $limit : '-1';
	$col_class = $out = "";

	switch($columns):
	case '1':   $col_class = 'column dt-sc-one-column';   break;

	case '2':   $col_class = 'column dt-sc-one-half';   break;
		
	default:    $col_class = 'column dt-sc-one-fourth';   break;
	endswitch;

	if($post_id != '') {
		$post_id = explode(',', $post_id);
		$args = array('post_type' => 'dt_teachers', 'posts_per_page' => $limit, 'post__in' => $post_id, 'order' => 'ASC', 'orderby' => 'date');
	} else {
		$args = array('post_type' => 'dt_teachers', 'posts_per_page' => $limit, 'order' => 'ASC', 'orderby' => 'date');
	}
	if($carousel == 'true') {
		$html_tag = 'li';
	} else {
		$html_tag = 'div';
	}
		
	$wp_query = new WP_Query($args);

	if($wp_query->have_posts()): $i = 1;
	while($wp_query->have_posts()): $wp_query->the_post();
		
	$temp_class = ""; global $post;
	if($carousel!='true'){
		if($i == 1) $temp_class = $col_class." first"; else $temp_class = $col_class;
		if($i == $columns) $i = 1; else $i = $i + 1;
	}
	if($carousel=='true'){
		$temp_class = $col_class;
		if($i == $columns) $i = 1; else $i = $i + 1;
			
	}
		
	$teacher_settings = get_post_meta ( $post->ID, '_teacher_settings', TRUE );
		
	$s = "";
	$path = plugins_url() . "/designthemes-core-features/shortcodes/images/sociables/";
	if(isset($teacher_settings['teacher-social'])) {
		foreach ( $teacher_settings['teacher-social'] as $sociable => $social_link ) {
			if($social_link != '') {
				$img = $sociable;
				$class = explode(".",$img);
				$class = $class[0];
				$s .= "<li class='{$class}'><a href='{$social_link}' target='_blank'> <img src='{$path}hover/{$img}' alt='{$class}'/>  <img src='{$path}{$img}' alt='{$class}'/> </a></li>";
			}
		}
	}
		
	$s = ! empty ( $s ) ? "<div class='dt-sc-social-icons'><ul>$s</ul></div>" : "";

	$link = get_the_permalink($post->ID);

	$out .= '<'.$html_tag.' class="'.$temp_class.'">';
	$out .= "   <div class='dt-sc-team'>";
	$out .= "		<div class='image'>";
	if(get_the_post_thumbnail($post->ID, 'full') != ''):
	$out .= get_the_post_thumbnail($post->ID, 'full');
	else:
	$out .= '<img src="http://placehold.it/400x420" alt="member-image" />';
	endif;
	$out .= " 		</div>";
	$out .= '		<div class="team-details">';
	$out .= '			<h5><a href="'.$link.'"">'.get_the_title().'</a></h5>';
	if($teacher_settings['role'] != '')
		$out .= "<h6>".$teacher_settings['role']."</h6>";
	if(isset($teacher_settings['show-social-share']) && $teacher_settings['show-social-share'] != '') $out .= $s;
	$out .= '		</div>';
	$out .= '   </div>';
	$out .= '</'.$html_tag.'>';

	endwhile;
	else:
	$out .= '<h2>'.__('Nothing Found.', 'dt_themes').'</h2>';
	$out .= '<p>'.__('Apologies, but no results were found for the requested archive.', 'dt_themes').'</p>';
	endif;
	if($carousel == 'true') {
		return '<div class="dt-sc-teacher-carousel-wrapper"><ul class="dt-sc-teacher-carousel" data-column="'.$columns.'">'.$out.'</ul><div class="carousel-arrows"><a class="teacher-prev" href=""></a><a class="teacher-next" href=""></a></div></div>';
	} else {
		return $out;
	}

}
add_shortcode('dt_custom_sc_teacher', 'dt_custom_sc_teacher');
/**
 * Create a shortcode for listing all sub categories of given category by id.
 * @author dung.le
 */
function list_all_sub_categories_by_category_id($atts) {
	extract( shortcode_atts( array(
			'type' => 'id'

	), $atts ) );
	$id = $atts['id'];
	$translated_id = icl_object_id($id, 'category', true, ICL_LANGUAGE_CODE);
	if ($translated_id > 0) $id = $translated_id;
	$args = array(
			'type'                     => 'post',
			'child_of'                 => $id,
			'parent'                   => '',
			'orderby'                  => 'name',
			'order'                    => 'ASC',
			'hide_empty'               => 1,
			'hierarchical'             => 1,
			'exclude'                  => '',
			'include'                  => '',
			'number'                   => '',
			'taxonomy'                 => 'category',
			'pad_counts'               => false
	);
	$output ='<div class="tagcloud">';
	$categories = get_categories( $args );
	foreach($categories as $category) {
		$output .= '<a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a>';
	}
	$output .= '</div>';
	return $output;
}
add_shortcode('all_sub_categories_by_category_id', 'list_all_sub_categories_by_category_id');
?>