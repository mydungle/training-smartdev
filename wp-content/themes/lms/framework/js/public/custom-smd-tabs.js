/**
 * This is main handler of horizontal tabs.
 * Style: refer custom-smd-tab.css
 * @author dung.le
 */
jQuery(document).ready(function($) {
	$('.smd-tab-title').click(function(e) {
		var mainContentId = $(this).parent().parent().attr('ref-content-id');
		if (mainContentId) {
			var singleContentId = $(this).attr('ref-id');
			if (singleContentId) {
				// Highlight the selected tab
				$(this).parent().parent().children().removeClass('selected');
				$(this).parent().addClass('selected');
				// Hiding all tab contents
				$(mainContentId + ' .smd-tab-content').hide();
				$(singleContentId).show("fade", {}, 200);
			}
		}
	});
});