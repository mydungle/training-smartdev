var coursename='',_date='',_location='',_trainer='',_hosted='';
var _firstname,_lastname,_gender,_jobtitle,_company,_email,_wordkphone,_address;
var emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;    
// var phoneRegex = /^[0-9\-\+]{9,15}$/;
var phoneRegex = /^\d+$/;
function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "error-field" );
		return false;
	} else {
		return true;
	}
}
function isIE () {
	var myNav = navigator.userAgent.toLowerCase();
	return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}
jQuery(document).ready(function ($) {
	$("#firstname").focusout(function() {
		if ($(this).val() != "") $(this).removeClass("error-field");
	});
	$("#lastname").focusout(function() {
		if ($(this).val() != "") $(this).removeClass("error-field");
	});
	$("#email").focusout(function() {
		if ($(this).val() != "") $(this).removeClass("error-field");
	});
	$("#workphone").focusout(function() {
		if ($(this).val() != "") $(this).removeClass("error-field");
	});
	$("#address").focusout(function() {
		if ($(this).val() != "") $(this).removeClass("error-field");
	});
	$("#jobtitle").focusout(function() {
		if ($(this).val() != "") $(this).removeClass("error-field");
	});
	$("#gender").focusout(function() {
		if ($(this).val() != "") $(this).removeClass("error-field");
	});
	$("#company").focusout(function() {
		if ($(this).val() != "") $(this).removeClass("error-field");
	});
	// Event handler of Register button in detail course
	$('a.expiry-off.register-login').click(function() {
		$('#message').html('');
		var loginBox = $(this).attr('href');
		$(loginBox).fadeIn("slow");
		$('body').append('<div id="over"></div>');
		$('#over').fadeIn(300);
		$("html, body").animate({ scrollTop: 0 }, "fast");
		return false;
	});
	$('a.expiry-on.register-login').click(function() {
		return false;
	});
	$(document).on('click', "a.close, #over, #btn-success-ok", function() { 
		$('#over, .container-dialog,.success-dialog').fadeOut(300 , function() {
			$('#over').remove();  
		}); 
		return false;
	});
var msgValidField='Please input valid data for all parameters';
var msgValidMail='Email not formatted correctly';
var msgValidPhone='Phone number not formatted correctly';
//click submit
$('#submit').click(function(){
	var language = $('input[name="language_code"]');
	if (language) {
		language = language.val();
		if (language == 'vi') {
			msgValidField='Vui lòng nhập đúng và đầy đủ các thông tin bắt buộc';
			msgValidMail='Email không đúng định dạng';
			msgValidPhone='Số điện thoại không đúng định dạng';
		}
	}
	removeClassFail();
	var emailObj =$('#login-box #email');
	var phoneObj =$('#login-box #workphone');
	if(getallvalue()){
		$('#message').html(msgValidField);
	}else if(!checkRegexp( emailObj, emailRegex, "eg. ex@exam.com" )){
		$('#message').html(msgValidMail);
	}
	else if(!checkRegexp( phoneObj, phoneRegex, "eg. 0123456" )){
		$('#message').html(msgValidPhone);
	}
	else{
		$('#over, .container-dialog').fadeOut(300 , function() {
			$('#over').remove(); 
				//open loading
				$('#dvLoading').fadeIn('slow');
				$('body').append('<div id="over"></div>');
				$('#over').fadeIn(300);
			});
		$('.message-color1').empty();
		var data = {action:'ajax_public_course_register',course:coursename,hosted:_hosted,date:_date,location:_location,trainer:_trainer,firstname:_firstname,lastname:_lastname,gender:_gender,jobtitle:_jobtitle,company:_company,email:_email,workphone:_workphone,address:_address};
		var url = PublicRegisterAjax.ajaxurl;
		var checkTimeout=true;
		setTimeout(function() {
			if(checkTimeout){
				showDialogErr();
			}
		}, 15000);
		resetinput();
		$.ajax({
			url: url,
			type:'post',
			data:data,
			success: function(res){
				checkTimeout=false;
				if(res){
					showDialogSucc();
				}else{
					showDialogErr();
				}
				
			},
			error: function(jqXHR,textStatus,errorThrown){
				showDialogErr();
			}
		});
	}

});
function showDialogSucc(){
	$('#dvLoading').fadeOut(300 , function() {
		$('#over').remove(); 
		$('#successdg').fadeIn('slow');
		$('body').append('<div id="over"></div>');
		$('#over').fadeIn(300);
	});
}
function showDialogErr(){
	$('#dvLoading').fadeOut(300 , function() {
		$('#over').remove(); 
		$('#errodg').fadeIn('slow');
		$('body').append('<div id="over"></div>');
		$('#over').fadeIn(300);
	});
}
//get value all input and check

function getallvalue(){
	_firstname=$('#firstname').val();
	_lastname=$('#lastname').val();
	_gender=$('#gender').val();
	_jobtitle=$('#jobtitle').val();
	_company=$('#company').val();
	_email=$('#email').val();
	_workphone=$('#workphone').val();
	_address=$('#address').val();
	var checkfield=false;
	if(_firstname==''){
		$('#firstname').addClass( "error-field" );
		checkfield=true;
	}
	if(_lastname==''){
		$('#lastname').addClass( "error-field" );
		checkfield=true;
	}
	if(_gender==''){
		$('#gender').addClass( "error-field" );
		checkfield=true;
	}
	if(_jobtitle==''){
		$('#jobtitle').addClass( "error-field" );
		checkfield=true;
	}
	if(_company==''){
		$('#company').addClass( "error-field" );
		checkfield=true;
	}
	if(_email==''){
		$('#email').addClass( "error-field" );
		checkfield=true;
	}
	if(_workphone==''){
		$('#workphone').addClass( "error-field" );
		checkfield=true;
	}
	if(_address==''){
		$('#address').addClass( "error-field" );
		checkfield=true;
	}

	return checkfield;
}
//reset all input to empty
function resetinput(){
	$('#firstname').val('');
	$('#lastname').val('');
	$('#gender').val('');
	$('#jobtitle').val('');
	$('#company').val('');
	$('#email').val('');
	$('#workphone').val('');
	$('#address').val('');
	$('.message-color1').empty();
}

}); 

function getid(_id,date,location,trainer){
	_date=date;
	_location=location;
	_trainer=trainer;
	switch(_id){
		case 'login-box':
		coursename='Management 3.0';
		jQuery('#name-course').html('Management 3.0');
		console.log(PublicRegisterAjax.cert_icon_path);
		jQuery("#iconsourse img").attr("src", PublicRegisterAjax.cert_icon_path);
		break;
		case 'login-box1':
		coursename='Agile Fundamentals';
		jQuery('#name-course').html('Agile Fundamentals');
		jQuery("#iconsourse img").attr("src", PublicRegisterAjax.cert_icon_path);
		break;
		case 'login-box2':
		coursename='Certified Scrum Developer (CSD)';
		jQuery('#name-course').html('Certified Scrum Developer (CSD)');
		jQuery("#iconsourse img").attr("src", PublicRegisterAjax.cert_icon_path);
		break;
		case 'login-box3':
		coursename='Agile Development';
		jQuery('#name-course').html('Agile Development');
		jQuery("#iconsourse img").attr("src", PublicRegisterAjax.non_cert_icon_path);
		break;
		case 'login-box4':
		coursename='Agile Testing';
		jQuery('#name-course').html('Agile Testing');
		jQuery("#iconsourse img").attr("src", PublicRegisterAjax.non_cert_icon_path);
		break;
		case 'login-box5':
		coursename='Scrum Master';
		jQuery('#name-course').html('Scrum Master');
		jQuery("#iconsourse img").attr("src", PublicRegisterAjax.non_cert_icon_path);
		break;
		case 'login-box6':
		coursename='Scrum Product Owner';
		jQuery('#name-course').html('Scrum Product Owner');
		jQuery("#iconsourse img").attr("src", PublicRegisterAjax.non_cert_icon_path);
		break;
	}
}
/*
 * Functions to fill data into popup (Course name) 
 * */
function loadCourseInfoIntoRegisterPopup(courseName, hosted, certificateFlag, date, location, trainer) {
	_date=date;
	_location=location;
	_trainer=trainer;
	_hosted=hosted;
	coursename = courseName;
	jQuery('#name-course').html(courseName);
	// Show certificate icon if it belongs to certificated course
	if (certificateFlag) {
		jQuery("#iconsourse img").attr("src", PublicRegisterAjax.cert_icon_path);
	} else {
		// Show non-certifcated icon
		jQuery("#iconsourse img").attr("src", PublicRegisterAjax.non_cert_icon_path);
	}
}

function removeClassFail(){
	jQuery('#firstname').removeClass('error-field');
	jQuery('#lastname').removeClass('error-field');
	jQuery('#gender').removeClass('error-field');
	jQuery('#jobtitle').removeClass('error-field');
	jQuery('#company').removeClass('error-field');
	jQuery('#email').removeClass('error-field');
	jQuery('#workphone').removeClass('error-field');
	jQuery('#address').removeClass('error-field');
}