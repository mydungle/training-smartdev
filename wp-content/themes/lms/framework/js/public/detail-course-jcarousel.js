jQuery(function ($) {
    var owl = $('#course-detail-content-slider-id');
    var nextControlElement = '.owl-next';
	var prevControlElement = '.owl-prev';
	var disableControlClass = 'disabled';

	if (owl.length > 0){
		owl
					.on(
							'initialized.owl.carousel changed.owl.carousel refreshed.owl.carousel',
							function(event) {
								if (!event.namespace)
									return;
								var carousel = event.relatedTarget, element = event.target, current = carousel
										.current();
								// var imageWidth, imageHeight;
								jQuery(nextControlElement, element).toggleClass(
										disableControlClass,
										current === carousel.maximum());
								jQuery(prevControlElement, element).toggleClass(
										disableControlClass,
										current === carousel.minimum());
								});
	}
	  owl.owlCarousel({
				items : 1,
				itemsScaleUp : true,
			
				slideSpeed : 200,
				// paginationSpeed : 800,
				// rewindSpeed : 1000,
		
				// Autoplay
				autoPlay : true,
						
				// Navigation
				nav : true,
				navText : [ " ", " " ],
				rewindNav : false,
				scrollPerPage : true,
		
				// Pagination
				pagination : false,
				paginationNumbers : false,
		
				// CSS Styles
				baseClass : "owl-carousel",
				theme : "owl-theme",
				navigation : true,
				transitionStyle : 'false',
				addClassActive : true,
				// afterAction: afterAction
	  });
});
