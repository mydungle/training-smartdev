jQuery(window).load(function() {
		  // Teacher Carousel
	  if(jQuery(".dt-sc-teacher-carousel").length) {
			jQuery(".dt-sc-teacher-carousel").each(function(){
				var $item = jQuery(this),
			  $column  = $item.attr("data-column");

			  if($column == 1) $item_width = '1170';
			  else if($column == 2) $item_width = '600';
			  else $item_width = '340';
				
			  var pagger = jQuery(this).parents(".dt-sc-teacher-carousel-wrapper").find("div.carousel-arrows"),
				  next = pagger.find("a.teacher-next"),
				  prev = pagger.find("a.teacher-prev");
				
			   jQuery(this).carouFredSel({
				 responsive: true,
				 auto: false,
				 width: '100%',
				 height: 'variable',
				 prev: prev,
				 next: next,
				 scroll: 1,                          
				 items: {
					   width:$item_width,
					   height: 'variable',
					   visible: {
						 min: 1,
						 max: 5
					   }
				 }                                
			   });  
		    });                     
		 }
});