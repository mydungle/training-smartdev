<?php
	// Define texts which will be translated in String translation
	$curr_course_category = __('certified-course-catalog', 'dt_themes');
	$text_course_name = __('Course name', 'dt_themes');
	$text_host = __('Host', 'dt_themes');
	$text_download = __('Download', 'dt_themes');
?>
<div id="certified-courses-table-wrapper" class="panel-container">
        <div class="smd-tab-content selected">
        	<?php
        	// If the list of upcoming courses is not empty
        	if (is_array($courses) && count($courses) > 0) {?>
        	<table class="table table-striped table-condensed">
            	<thead>
                	<tr>
                    	<th class="<?php if ($certificate_flag ==1) echo 'col-md-6'; else echo 'col-md-8';?>"><?php echo $text_course_name;?></th>
						<?php if ($certificate_flag == 1) {?>
						<th class="col-md-2"></th>
						<?php }?>
                        <th class="col-md-2"><?php echo $text_host;?></th>
                        <th class="col-md-2"></th>
                    </tr>
                </thead>
                <tbody>
                	<?php
                	foreach ($courses as $course) {
                		$cert_icon = '';
                		// Prepare data to show list of certificated courses in the table
                		$id = $course->ID;
                		$tmp_course_title = $course->post_title;
                		if (strlen($tmp_course_title) > MAX_CHARACTERS_COURSE_TITLE) {
                			$tmp_short_course_title = substr($tmp_course_title, 0, MAX_CHARACTERS_COURSE_TITLE) . ETC_DOTS;
                		} else {
                			$tmp_short_course_title = $tmp_course_title;
                		}
                		// Get url of course page
                		$tmp_permalink =  get_permalink($course->ID);
                		// Get detail information such as Date, trainer, location
                		$tmp_custom_fields = get_post_custom($id);
                		// Get hosted by value
                		$tmp_hosted_by = $tmp_custom_fields['_hosted_by'];
                		if (!is_array($tmp_hosted_by)) $tmp_hosted_by = '';
                		else $tmp_hosted_by = $tmp_hosted_by[0];
                		// Getting certificate icon of the course
                		$cert_icon = $tmp_custom_fields['_certificate_icon'][0];
                		if (strlen($cert_icon) > 0) {
                			$cert_icon = '<img src="'.$cert_icon.'" />';
                		} else {
                			$cert_icon = '';
                		}
                		//Get url of upload file
                		$file_upload = $tmp_custom_fields['_upload_file'] [0];
                		$file_upload = unserialize($file_upload);
                		if (is_array($file_upload) && count($file_upload) > 0) {
                			$file_upload = $file_upload[0];
                			$file_upload_path = $file_upload['image'];
                			$file_upload_title = $file_upload['title'];
                			$index_pos = strpos($file_upload_path, '/wp-content/');
                			if ($index_pos >= 0) {
                				// Getting sub path since /wp-content
                				$file_upload_path = substr($file_upload_path, $index_pos);
                			} else {
                				$file_upload_path = '';
                			}
                		} else {
                			$file_upload_path = '';
                		}
                		?>
                	    <tr>
                	    <td class="coures-name-col"><a title="<?php echo $tmp_course_title;?>"
                	        href="<?php echo $tmp_permalink;?>"><?php echo $tmp_short_course_title;?></a>
                	    </td>
                	    <?php if ($certificate_flag == 1) {?>
                	    <td class="course-cert-icon" >
                				<?php echo $cert_icon;?>
                		</td>
                		<?php }?>
                	    <td><?php echo $tmp_hosted_by;?></td>
                	    <td class="course-download-col">
                	        <?php if (strlen($file_upload_path) > 0) {?>
                	        <a 
							<?php if (strlen($file_upload_title) > 0) {echo 'title="'.$file_upload_title.'"';}?>
							href="<?php echo $file_upload_path;?>" download>
							<?php echo $text_download;?>
						    </a>
						    <?php } ?>
                	    </td>
                	</tr>
             <?php 
                 }// End foreach showing list of certificted course
             ?>
            </table>
			<?php } else {
                    // End checking empty list of upcomming courses. If it's empty, showing message.
            		echo MSG_EMPTY_TRAINING_COURSES;
                 }
            ?>
        </div>
</div>
