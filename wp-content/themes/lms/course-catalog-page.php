<?php
/* Template Name Posts: Course Catalog Page */
/* Template Name: Course Catalog Page */
?>
<?php
	include 'settings.php';
    get_header();
    // Define texts.
    $text_certified_courses = __('Certified Courses', 'dt_themes');
    $text_non_certified_courses = __('Non-certified Courses', 'dt_themes');
    $curr_cert_course_category = __('certified-course-catalog', 'dt_themes');
    $curr_non_cert_course_category = __('non-certified-course-catalog', 'dt_themes');
?>
<div class="service-section">
    <div class="container">
    	<div>
    		<?php if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                } ?>
    	</div>
        <div class="smartdev-content section">
            <div id="course-catalog-page">
                <div class="other-certificated-courses certificate">
                        <div class="detail-content-item">
                            <div class="detail-content-item-title">
                                <h3><?php echo $text_certified_courses;
                                ?></h3>
                            </div>
                            <div class="detail-content-item-body">
                                <?php 
                                $certificate_flag = 1;
                                // Getting all certified courses from catalog
                                $args = array (
                                		'category_name' => $curr_cert_course_category,
                                		'orderby' => 'post_title',
                                		'order' => 'ASC',
                                		'post_type' => POST_TYPE_COURSE_CATALOG,
                                		'post_status' => 'publish',
                                		'posts_per_page' => - 1
                                );
                                $courses = get_posts($args);
                                include(locate_template('course-catalog-table-template.php'));
                                ?>
                            </div>
                        </div>
                    </div>
                   <div class="other-certificated-courses non-certificate">
                        <div class="detail-content-item">
                            <div class="detail-content-item-title">
                               <h3><?php echo $text_non_certified_courses;
                                 ?></h3>
                            </div>
                            <div class="detail-content-item-body">
	                            <?php 
	                            $certificate_flag = 0;
                                // Getting all certified courses from catalog
                                $args = array (
                                		'category_name' => $curr_non_cert_course_category,
                                		'orderby' => 'post_title',
                                		'order' => 'ASC',
                                		'post_type' => POST_TYPE_COURSE_CATALOG,
                                		'post_status' => 'publish',
                                		'posts_per_page' => - 1
                                );
                                $courses = get_posts($args);
                                include(locate_template('course-catalog-table-template.php'));
                                ?>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
     </div>
</div>

<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
    	function updateTraingCourseTables() {
    		var windowWidth = jQuery('html').width();
    		if (windowWidth <= 565) {
    			// Hide some column in training courses tables
    			jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(3)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(4)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(5)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(3)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(4)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(5)').addClass('hide');

    			jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(4)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(5)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(6)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(4)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(5)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(6)').addClass('hide');

    			jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(2)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(2)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses table tbody tr *, .other-certificated-courses table thead tr *').css('font-size', '0.7em');
    			jQuery('.ie8 .other-certificated-courses table tbody tr .course-date-col').css('font-size', '0.7em');
    		} else if (windowWidth <= 900) {
    			// Hide some column in training courses tables
    			jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(3)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(4)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(5)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(3)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(4)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(5)').addClass('hide');

    			jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(4)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(5)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(6)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(4)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(5)').addClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(6)').addClass('hide');
    		} else if (windowWidth > 900) {
    			// Show some column in training courses tables
    			jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(3)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(4)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table thead tr th:nth-child(5)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(3)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(4)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses.non-certificate table tbody tr td:nth-child(5)').removeClass('hide');

    			jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(4)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(5)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(6)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(4)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(5)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(6)').removeClass('hide');

    			jQuery('.ie8 .other-certificated-courses.certificate table thead tr th:nth-child(2)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses.certificate table tbody tr td:nth-child(2)').removeClass('hide');
    			jQuery('.ie8 .other-certificated-courses table tbody tr *, .other-certificated-courses table thead tr *').css('font-size', '1em');
    			jQuery('.ie8 .other-certificated-courses table tbody tr .course-date-col').css('font-size', '1em');
    		}
    	}
        
        // Handling old urls
        // services/#agile-project-management
        // services/#agile-offshore-development
        // services/#agile-training-and-coaching
        var apm = '#agile-project-management';
        var aod = '#agile-offshore-development';
        var atc = '#agile-training-and-coaching';
        // Expected new url forwarded from the above old urls
        var exAPM = '/services/agile-project-management';
        var exAOD = '/services/agile-offshore-development';
        var exATC = '/services/agile-training-and-coaching';
        var hash = window.location.hash;
        switch (hash) {
        case apm:
        case apm+'/':
            window.location.href = exAPM;
            break;
        case aod:
        case aod+'/':
            window.location.href = exAOD;
            break;
        case atc:
        case atc+'/':
            window.location.href = exATC;
            break; 
        }
        
        // For IE 8, Set width for img tag of certificate icon if its width is greater than 200
    	if (isIE() <= 8) {
    		var certIcons = jQuery('.ie8 .other-certificated-courses tbody tr td.course-cert-icon img');
    		var certIconWidth;
    		if (certIcons.length > 0) {
    			certIcons.each(function() {
    				certIconWidth = jQuery(this).width();
    				if (certIconWidth > 180) {
    					jQuery(this).width(180+'px');
    					}
    				});
    		}
    		// Updating training course tables by hidding some column in small screen sizes
    		updateTraingCourseTables();
    	}
    	jQuery('html.ie8').resize(function(){
    		updateTraingCourseTables();
    	});
});
</script>