<?php
include 'settings.php';
if( ! defined('IAMD_BASE_URL' ) ){	define( 'IAMD_BASE_URL',  get_template_directory_uri().'/'); }
define('IAMD_FW_URL', IAMD_BASE_URL . 'framework/' );
define('IAMD_FW',get_template_directory().'/framework/');
define('IAMD_TD',get_template_directory());
define('IAMD_IMPORTER_URL',IAMD_FW.'wordpress-importer/');
define('IAMD_THEME_SETTINGS', 'mytheme');
define('IAMD_SAMPLE_FONT', __('The quick brown fox jumps over the lazy dog','dt_themes') );

$user_id = get_current_user_id();
if($user_id > 0) {
	$user_info = get_userdata($user_id);
	foreach($user_info -> roles as $role) {	$user_role = $role; }
	define('IAMD_USER_ROLE', strtolower($user_role));
}


/* Define IAMD_THEME_NAME
 * Objective:	
 *		Used to show theme name where ever needed( eg: in widgets title ar the back-end).
 */
// get themedata version wp 3.4+
if(function_exists('wp_get_theme')):
	$theme_data = wp_get_theme();
	define('IAMD_THEME_NAME',$theme_data->get('Name'));
	define('IAMD_THEME_FOLDER_NAME',$theme_data->template);
	define('IAMD_THEME_VERSION',(float) $theme_data->get('Version'));
endif;

#ALL BACKEND DETAILS WILL BE IN include.php
require_once (get_template_directory () . '/framework/include.php');
if ( ! isset( $content_width ) ) $content_width = 1170;

/**
 * To get language code of post id.
 * @param unknown $post_id
 * @author dung.le
 */
function langcode_post_id($post_id){
	global $wpdb;

	$query = $wpdb->prepare('SELECT language_code FROM ' . $wpdb->prefix . 'icl_translations WHERE element_id="%d"', $post_id);
	$query_exec = $wpdb->get_row($query);

	return $query_exec->language_code;
}
/**
 * Create unix time stamp from date picker - an custom filed in Just custom field plugin
 * @author dung.le
 */
function custom_unixtimesamp($post_id ) {
	if ( get_post_type( $post_id ) == 'training-course' ) {
		$start_date = get_post_meta($post_id, '_start_date', true);
		$end_date = get_post_meta($post_id, '_end_date', true);
		if (strlen($start_date) > 0) {
			update_post_meta($post_id, '_unix_start_date', strtotime($start_date));
		} else {
			update_post_meta($post_id, '_unix_start_date', '');
		}
		if (strlen($end_date) > 0) {
			update_post_meta($post_id, '_unix_end_date', strtotime($end_date));
		} else {
			update_post_meta($post_id, '_unix_end_date', '');
		}
	}
}
add_action('save_post', 'custom_unixtimesamp', 100, 2);


/**
 * Change upload folder of pdf files.
 * @author thao.tran
 */
add_filter('wp_handle_upload_prefilter', 'wpse47415_pre_upload');
add_filter('wp_handle_upload', 'wpse47415_post_upload');
/**
 * Change upload folder of pdf files.
 * @author thao.tran
 */
function wpse47415_pre_upload($file){
	add_filter('upload_dir', 'wpse47415_custom_upload_dir');
	return $file;
}
/**
 * Change upload folder of pdf files.
 * @author thao.tran
 */
function wpse47415_post_upload($fileinfo){
	remove_filter('upload_dir', 'wpse47415_custom_upload_dir');
	return $fileinfo;
}
/**
 * Change upload folder of pdf files.
 * @author thao.tran
 */
function wpse47415_custom_upload_dir($path){
	$extension = substr(strrchr($_POST['name'],'.'),1);
	if(!empty($path['error']) ||  $extension != 'pdf') { return $path; } //error or other filetype; do nothing.
	$customdir = '/pdf';
	$path['subdir']  = $customdir;
	$path['path']   .= $customdir;
	$path['url']    .= $customdir;
	return $path;
}
/**
 * Adding custom field with a select box showing course catalog.
 * @author dung.le
 * */
function training_course_add_custom_box() {
	$screens = array( 'post', POST_TYPE_TRAINING_COURSE );
	foreach ( $screens as $screen ) {
		add_meta_box(
				'training_course_box_id',            // Unique ID
				'Course Catalog',      // Box title
				'training_course_inner_custom_box',  // Content callback
				$screen                      // post type
		);
		add_meta_box(
				'training_course_photos_list_id',            // Unique ID
				'Photos from galleries',      // Box title
				'training_course_photos_reference_custom_box',  // Content callback
				$screen                      // post type
		);
	}
	// Adding list of coaches into Edit detail course catalog page
	$screens = array( 'post', POST_TYPE_COURSE_CATALOG );
	foreach ( $screens as $screen ) {
		// Adding list of trainer
		add_meta_box(
				'training_course_coach_id',            // Unique ID
				'Coaches List',      // Box title
				'training_course_coaches_reference_custom_box',  // Content callback
				$screen                      // post type
		);
	}
}
add_action( 'add_meta_boxes', 'training_course_add_custom_box' );

// add custom field showing list user in online course
function online_course_add_custom_field(){
	$screens = array( 'dt_courses', POST_TYPE_ONLINE_COURSE );
	foreach ( $screens as $screen ) {
		add_meta_box(
				'online_course_purchase_id',            // Unique ID
				'Set User Purchased Course',      // Box title
				'online_course_purchase_reference_custom_box',  // Content callback
				$screen                      // post type
		);
	}

}
add_action('add_meta_boxes', 'online_course_add_custom_field');
//Showing list of user in edit of online course
function online_course_purchase_reference_custom_box($post){
	$list_user = get_post_meta($post->ID, CUSTOM_FIELD_ONLINE_PURCHASED_COURSE, true);
	$value= explode(",", $list_user);
	?>
	<label>(*) Optimal. Please select users who purchased this course.</label>
	</br>
	<select name = "custom_field_purchased_course[ ]" id = "custom_field_purchased_course" class="postbox" multiple>
		<?php
		$args = array('orderby' => 'display_name',);
		$users_query = new WP_User_Query($args);
		$users= $users_query->get_results();
		foreach ($users as $user) {
			$id = $user->ID;
			$name = $user->display_name;
		 ?>
		<option value="<?php echo $id;?>" <?php echo (in_array($id, $value))? 'selected': '';?>>
		 <?php echo $name;?></option>
		<?php
		}
		 ?>
	</select>
	<?php
}
?>
<?php
/**
 * Showing list of course cataloges in Edit page of public training courses.
 * @author dung.le
 * @param WP_Post $post The object for the current post/page.
*/
function training_course_inner_custom_box( $post ) {
	// Get all available course catalog
	$args = array (
			'orderby' => 'post_title',
			'order' => 'ASC',
			'post_type' => POST_TYPE_COURSE_CATALOG,
			'post_status' => 'publish',
			'posts_per_page' => - 1
	);
	$course_catalog = get_posts ( $args );
	$value = (int) get_post_meta($post->ID, CUSTOM_FIELD_COURSE_CATALOG_ID, true);
	?>
	<label for="training_course_field">(*) Required. Please select a course catalog from the list.</label>
	<br/>
	<select name="training_course_catalog_field" id="training_course_catalog_field" class="postbox">
	    <option value="0"><?php echo MESSAGE_COURSE_CATALOG_SELECT_BOX;?></option>
		<?php if (is_array($course_catalog) && count($course_catalog) > 0) { ?>
			<?php
			// Loading all course catalog into select box custom field
			foreach($course_catalog as $catalog) {
				$id = $catalog->ID;
				if (in_category(array(VN_CATEGORY_COURSE_CATALOG_CERTIFIED, VN_CATEGORY_COURSE_CATALOG_NON_CERTIFIED), $id)) {
					$course_catalog_name = $catalog->post_title . ' - VN';
				} else $course_catalog_name = $catalog->post_title;
				
			?>
			    <option value="<?php echo $id;?>" <?php echo ($value == $id)? 'selected': '';?>>
			        <?php echo $course_catalog_name;?>
			    </option>
		    <?php
			}// End For loop
			?>
		<?php 
	    } // End if
	    ?>
	</select>
<?php
}// End the function
?>

<?php
/**
 * Showing list of coaches in Edit detail public training courses.
 * @author dung.le
 * @param WP_Post $post The object for the current post/page.
*/
function training_course_coaches_reference_custom_box( $post ) {
	// Get all available coaches
	$args = array (
			'orderby' => 'post_title',
			'order' => 'ASC',
			'post_type' => POST_TYPE_COACH,
			'post_status' => 'publish',
			'posts_per_page' => - 1
	);
	$coaches = get_posts ( $args );
	$value = (int) get_post_meta($post->ID, CUSTOM_FIELD_COURSE_TRAINER, true);
	?>
	<label for="training_course_coaches_field">(*) Required. Please select a trainer from the list.</label>
	<br/>
	<select name="training_course_coaches_select_field" id="training_course_coaches_select_field" class="postbox">
	    <option value="0"><?php echo MESSAGE_COURSE_COACHES_SELECT_BOX;?></option>
		<?php if (is_array($coaches) && count($coaches) > 0) { ?>
			<?php
			// Loading all coaches into select box custom field
			foreach($coaches as $catalog) {
				$id = $catalog->ID;
			    $coach_name = $catalog->post_title;
			    $lang = langcode_post_id($id);
			    if ($lang == LANGUAGE_CODE_VN) {
			    	$coach_name = $catalog->post_title . ' - VN';
			    } else $coach_name = $catalog->post_title;
			?>
			    <option value="<?php echo $id;?>" <?php echo ($value == $id)? 'selected': '';?>>
			        <?php echo $coach_name;?>
			    </option>
		    <?php
			}// End For loop
			?>
		<?php 
	    } // End if
	    ?>
	</select>
<?php
}// End the function
?>

<?php 
/**
 * Prints list of training courses in English in Edit page of training course type.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function training_course_photos_reference_custom_box( $post ) {
	$args = array('taxonomy' => 'portfolio_entries', 'paged' => get_query_var( 'paged' ) ,'posts_per_page' => $post_per_page,'post_type' => 'dt_portfolios');
	query_posts($args);
	$value = (int) get_post_meta($post->ID, CUSTOM_FIELD_COURSE_PHOTO_SLIDER_ID, true);
	?>
	<label for="training_photos_field">(*) Optional. Please select a gallery from the list to use use images for the course.</label>
	<br/>
	<select name="<?php echo FORM_CUSTOM_FIELD_TRAINING_COURSE_PHOTOS_FIELD; ?>" id="<?php echo FORM_CUSTOM_FIELD_TRAINING_COURSE_PHOTOS_FIELD; ?>" class="postbox">
	    <option value="0"><?php echo MESSAGE_COURSE_PHOTOS_SELECT_BOX;?></option>
	    <?php 
	    if( have_posts() ):
	    while( have_posts() ):
		    the_post();
		    $id = get_the_ID();
		    $title = get_the_title();
	    ?>
	    <option value="<?php echo $id;?>" <?php echo ($value == $id)? 'selected': '';?>>
			        <?php echo $title;?>
	    </option>
	    <?php 
	    endwhile;
	    endif;
	    // ensure that the main query has been reset to the original main query
	    wp_reset_query();
	    ?>
	</select>
<?php
}// End the function
?>
<?php 
/**
 * Prints list of Galleries of active languages.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function training_course_gallery_reference_custom_box( $post ) {
	$curr_cert_course_category = __('certified-courses', 'dt_themes');
	$curr_non_cert_course_category = __('non-certified-courses', 'dt_themes');
	// Get all available training courses in English including certified and non-certified courses.
	// Initialize array of public course category ids.
	$cat_ids = array();
	// Get category id of public course categories in English
	$cert_cat = get_category_by_slug($curr_cert_course_category);
	if ($cert_cat) $cat_ids[] = $cert_cat->term_id;
	$non_cert_cat = get_category_by_slug($curr_non_cert_course_category);
	if ($non_cert_cat) $cat_ids[] = $non_cert_cat->term_id;
	// Getting all certificated and non certificated courses.
	$args = array (
			'category__in' => $cat_ids,
			'meta_key' => CUSTOM_FIELD_COURSE_UNIX_START_DATE,
			'orderby' => 'meta_value post_title',
			'order' => 'DESC',
			'post_type' => POST_TYPE_TRAINING_COURSE,
			'post_status' => 'publish',
			'posts_per_page' => - 1
	);
	$courses = get_posts ( $args );
	$value = (int) get_post_meta($post->ID, CUSTOM_FIELD_COURSE_PHOTO_SLIDER_ID, true);
	?>
	<label for="training_photos_field">(*) Optional. Please select a course from the list for getting its photos.</label>
	<br/>
	<select name="<?php echo FORM_CUSTOM_FIELD_TRAINING_COURSE_PHOTOS_FIELD; ?>" id="<?php echo FORM_CUSTOM_FIELD_TRAINING_COURSE_PHOTOS_FIELD; ?>" class="postbox">
	    <option value="0"><?php echo MESSAGE_COURSE_PHOTOS_SELECT_BOX;?></option>
		<?php if (is_array($courses) && count($courses) > 0) { ?>
			<?php
			// Loading all course catalog into select box custom field
			foreach($courses as $course) {
				$id = $course->ID;
				$title = $course->post_title;
				$start_date = get_post_meta($id, '_start_date', true);
				if (strlen($start_date) <= 0) $start_date = __('Coming soon', 'dt_themes');
			?>
			    <option value="<?php echo $id;?>" <?php echo ($value == $id)? 'selected': '';?>>
			        <?php echo $title . ' - ' .$start_date;?>
			    </option>
		    <?php
			}// End For loop
			?>
		<?php 
	    } // End if
	    ?>
	</select>
<?php
}// End the function
?>
<?php
/**
 * Save course catalog id when saving public training courses.
 * @author dung.le
 * @param int $post_id
 */
function just_custom_field_save_postdata($post_id) {
	if (array_key_exists('training_course_catalog_field', $_POST)) {
		$course_catalog_id = (int) $_POST['training_course_catalog_field'];
		if (is_numeric($course_catalog_id)) {
			// saving course catalog based on each course
			update_post_meta($post_id,
					CUSTOM_FIELD_COURSE_CATALOG_ID,
					$_POST['training_course_catalog_field']
			);
			// getting trainer of course catalog
			$trainer = get_post_meta($course_catalog_id, CUSTOM_FIELD_COURSE_TRAINER, true);
			// saving trainer name based on each course
			update_post_meta($post_id,
					CUSTOM_FIELD_COURSE_TRAINER,
					$trainer
			);
		}
	}
	// Saving course photos slider
	if (array_key_exists(FORM_CUSTOM_FIELD_TRAINING_COURSE_PHOTOS_FIELD, $_POST)) {
		
		$course_id = (int) $_POST[FORM_CUSTOM_FIELD_TRAINING_COURSE_PHOTOS_FIELD];
		if (is_numeric($course_id)) {
			// saving course catalog based on each course
			update_post_meta($post_id,
					CUSTOM_FIELD_COURSE_PHOTO_SLIDER_ID,
					$course_id
			);
		}
	}
	// Saving coaches of the course
	if (array_key_exists('training_course_coaches_select_field', $_POST)) {
		$course_coach_id = (int) $_POST['training_course_coaches_select_field'];
		if (is_numeric($course_coach_id)) {
			// saving course coach id for the course
			update_post_meta($post_id,
					CUSTOM_FIELD_COURSE_TRAINER,
					$course_coach_id
			);
			
		}
	}
	// Saving users purchased online course
	if(array_key_exists('custom_field_purchased_course', $_POST)){
		$user_purchased_ID = $_POST['custom_field_purchased_course'];
		$string = implode((","), $user_purchased_ID);
		update_post_meta($post_id,
				CUSTOM_FIELD_ONLINE_PURCHASED_COURSE,
				$string
				);
	}
}
add_action('save_post', 'just_custom_field_save_postdata' );
/**
 * Auto update trainer information of all courses belongs to a course catalog when the trainer updated in course catalog.
 * @author dung.le
 */
function course_catalog_update_trainer($post_id) {

	if (get_post_type($post_id) == POST_TYPE_COURSE_CATALOG) {
		// Get current trainer information of the current course catalog
		$trainer_id = get_post_meta($post_id, CUSTOM_FIELD_COURSE_TRAINER, true);
		// Getting all certificated and non certificated courses.
		$args = array (
				'meta_key' => CUSTOM_FIELD_COURSE_CATALOG_ID,
				'post_type' => POST_TYPE_TRAINING_COURSE,
				'post_status' => 'publish',
				'posts_per_page' => - 1,
				'meta_value' => $post_id
		);
		$courses = get_posts ( $args );
		// Update all available training courses which belongs to this course catalog
		foreach ($courses as $course) {
			update_post_meta($course->ID, CUSTOM_FIELD_COURSE_TRAINER, $trainer_id);
		}
	}
}
add_action('save_post', 'course_catalog_update_trainer', 100, 2);


// embed the javascript file that makes the AJAX request
wp_enqueue_script( 'my-ajax-request', get_template_directory_uri() . '/framework/js/public/custom-dialog.js', array( 'jquery' ) );

// declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
wp_localize_script( 'my-ajax-request', 'PublicRegisterAjax', 
		array(
		    'ajaxurl' => site_url( '/wp-admin/admin-ajax.php' ),
			'cert_icon_path' => get_template_directory_uri().'/images/customize/ico1.png',
			'non_cert_icon_path' => get_template_directory_uri().'/images/customize/ico2.png',
) );

function set_content_type($content_type){
	return 'text/html';
}

// dung.le Adding ajax handling for registering public course.
// if both logged in and not logged in users can send this AJAX request,
// add both of these actions, otherwise add only the appropriate one
add_action( 'wp_ajax_nopriv_ajax_public_course_register', 'ajax_public_course_register' );
add_action( 'wp_ajax_ajax_public_course_register', 'ajax_public_course_register' );
/**
 * Submit of public course register. 
 */
function ajax_public_course_register() {
	if(isset($_POST['firstname'])){
		global $contact_errors;
		global $message;
		$contact_errors = false;
		//get training and coaching infor
		$coursename= strip_tags($_POST['course']);
		$date=strip_tags($_POST['date']);
		$location=strip_tags($_POST['location']);
		$trainer=strip_tags($_POST['trainer']);
		$hosted=$_POST['hosted'];
	
		//put message variable
		$message ="<h3>COURSE INFORMATION</h3>";
		$message .= "<table cellpadding='4'><tr> <td><b>Course Name:</b></td><td> $coursename</td></tr>";
		$message .= "<tr><td><b>Date:</b></td><td> $date</td></tr>";
		$message .= "<tr><tr><b>Loaction:</b></td><td>$location</td></tr>";
		$message .= "<tr><tr><b>Hosted by:</b></td><td>$hosted</td></tr>";
		$message .= "<tr><td><b>Trainer:</b></td><td> $trainer</td></tr></table><br/><hr/><br/>";
		//get ATTENDEE INFORMATION
		$firstname= strip_tags($_POST['firstname']);
		$lastname= strip_tags($_POST['lastname']);
		$gender= strip_tags($_POST['gender']);
		$jobtitle= strip_tags($_POST['jobtitle']);
		$company= strip_tags($_POST['company']);
		$email= strip_tags($_POST['email']);
		$workphone= strip_tags($_POST['workphone']);
		$address=strip_tags($_POST['address']);
		//put message variable
		$message .="<h3>ATTENDEE INFORMATION</h3>";
		$message .= "<table cellpadding='4'><tr><td><b>Title:</b></td><td> $gender</td></tr>";
		$message .= "<tr><td><b>First Name:</b></td><td> $firstname</td></tr>";
		$message .= "<tr><td><b>Last Name:</b></td><td> $lastname</td></tr>";
		$message .= "<tr><td><b>Job Title:</b></td><td> $jobtitle</td></tr>";
		$message .= "<tr><td><b>Company:</b></td><td> $company</td></tr>";
		$message .= "<tr><td><b>Email:</b></td><td> $email</td></tr>";
		$message .= "<tr><td><b>Work Phone:</b></td><td> $workphone</td></tr>";
		$message .= "<tr><td><b>Address:</b></td><td> $address</td></tr></table>";
		$subject = '[Public Training Course]['.$hosted.'][Register] '.$_POST['course'];
		$subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
		add_filter( 'wp_mail_content_type', 'set_content_type' );
		$user = get_userdatabylogin('smartdev_training');
		$to = $user->user_email;
		// send the email using wp_mail()
		$contact_errors = wp_mail($to, $subject, $message);
		remove_filter( 'wp_mail_content_type', 'set_content_type' );
		exit;
	}
}
add_action('send_mail_public_course_register', 'send_mail_public_course_register');

//defer js files
function defer_parsing_of_js ( $url ) {
if ( FALSE === strpos( $url, '.js' ) ) return $url;
if ( strpos( $url, 'jquery.js' ) ) return $url;
return "$url' defer ";
}
add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
