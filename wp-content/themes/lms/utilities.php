<?php
/**
 * Function to check a course is expired or not. It's expired if current date >= its start date. We are not allowed to register.
 * @param string $date
 * @return 0 if it's not expired. 1 if it's expired.
 */
function isExpiry($date) {
	if ($date && strlen($date) > 0) {
		$tmp_date = new DateTime($date. ' '. START_TIME_FORMAT);
		$date_time = strtotime($tmp_date->format(DATE_TIME_FORMAT));
	} else {
		return 0;
	}
	$result = 0; // Default value is not expired
	// Get current date time
	$curr_date = date(DATE_TIME_FORMAT);
	$curr_date = strtotime($curr_date);
	$curr_date = new DateTime(date(DATE_TIME_FORMAT));
	$curr_date->setTimezone(new DateTimeZone(DEFAULT_TIMEZONE));
	$cur_date = strtotime($curr_date->format(DATE_TIME_FORMAT));
	// If the end date of course is over, it's expired.
	if ($cur_date >= $date_time) $result = 1;
	else $result = 0;
	return $result;
}
/**
 * Function to check a course is expired or not. It's expired if current date >= its start date. We are not allowed to register.
 * @param string $date
 * @return 0 if it's not expired. 1 if it's expired.
 */
function is_expiry($date) {
	if ($date && strlen($date) > 0) {
		$tmp_date = new DateTime($date. ' '. START_TIME_FORMAT);
		$date_time = strtotime($tmp_date->format(DATE_TIME_FORMAT));
	} else {
		return 0;
	}
	$result = 0; // Default value is not expired
	// Get current date time
	$curr_date = date(DATE_TIME_FORMAT);
	$curr_date = strtotime($curr_date);
	$curr_date = new DateTime(date(DATE_TIME_FORMAT));
	$curr_date->setTimezone(new DateTimeZone(DEFAULT_TIMEZONE));
	$cur_date = strtotime($curr_date->format(DATE_TIME_FORMAT));
	// If the end date of course is over, it's expired.
	if ($cur_date >= $date_time) $result = 1;
	else $result = 0;
	return $result;
}
/**
 * Function to check a course is upcoming with defined start date or not. It's upcoming when the start date < the current date.
 * @param string $date
 * @return 0 if it's not upcoming. 1 if it's upcoming.
 */
function is_comming($date) {
	// If the date is defined, we will check more condition.
	if ($date && strlen($date) > 0) {
		$tmp_date = new DateTime($date. ' '. START_TIME_FORMAT);
		$date_time = strtotime($tmp_date->format(DATE_TIME_FORMAT));
	} else {
		// If the date is undefined, it's not in coming
		return 0;
	}
	$result = 0; // Default value is not expired
	// Get current date time
	$curr_date = date(DATE_TIME_FORMAT);
	$curr_date = strtotime($curr_date);
	$curr_date = new DateTime(date(DATE_TIME_FORMAT));
	$curr_date->setTimezone(new DateTimeZone(DEFAULT_TIMEZONE));
	$cur_date = strtotime($curr_date->format(DATE_TIME_FORMAT));
	// If the end date of course is over, it's expired.
	if ($cur_date < $date_time) $result = 1;
	else $result = 0;
	return $result;
}
/**
 * Function to check a course is going without defined start date.
 * @param string $date
 * @return 0 if it's not in plan. 1 if it's plan.
 */
function is_plan($date) {
	// If the date is undefined, it's supposed to be in plan.
	if (!$date) return 1;
	else return 0;
}