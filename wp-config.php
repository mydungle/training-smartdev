<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'training');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'abc@12345');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');

define('WP_HOME','http://training.smartdev.vn/');
define('WP_SITEURL','http://training.smartdev.vn/');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*Tki%nzM3:+HZbF~`rKd}zPe{?RXP1KkJPZ=cqcgBKB[{+_ky.!A0]kAgbhbCIe5');
define('SECURE_AUTH_KEY',  'Dwh/4^0}MgY0]|~T[c&.=tq~EH!R-M?AZ?%Y8&39aLE$Z~}?1dvVNDvS$z#c=~[=');
define('LOGGED_IN_KEY',    'yONV&qY3Y|_ltZfoI-v2ibpn;Kh8?zt;:4jw)Rct55dHtxM0*qd~V%7/@mll-8)9');
define('NONCE_KEY',        'J}6V>vgw t8cxPx,khpiFq]i>(][-3d`LqwA|}Gb>(KEOG?]11rPhFX9vr:W)q)k');
define('AUTH_SALT',        '|,ShD+$NFS;4HrX<9HkmCh^9;q5qlP0/6Fc;enIMvu@-TiUyf#s9J8+{E7?v?($O');
define('SECURE_AUTH_SALT', ';.ayd8m>GN*uz6*4N>;{cjG9o5h_rKH4akF0UT*MXryE=hzzKu%J+)`YgrJ;Q)FW');
define('LOGGED_IN_SALT',   'h>i-;Nrh[BeDDc4d.|oSW;7n2b+2|Yo,S+;@-qhqM--<bszI;FKW>gop<-SE3Twm');
define('NONCE_SALT',       'Wc$I5y1|WoOT|mI6TYbgVPFGmR>XFoZ/d|:/Q!$nCtf:+j-O.Kci38Y:fc15:d{Q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
/*Customize path of uploaded files*/
define('UPLOADS', 'wp-content/themes/lms/images/uploads');

define('WP_MEMORY_LIMIT', '256M');

