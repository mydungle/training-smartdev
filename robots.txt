Sitemap: http://training.smartdev.vn/sitemap.xml
Sitemap: http://training.smartdev.vn/sitemap-images.xml
 
# Google Image
User-agent: Googlebot-Image
Disallow:
Allow: /*
 
# Google AdSense
User-agent: Mediapartners-Google
Disallow:
 
# digg mirror
User-agent: duggmirror
Disallow: /

User-agent: Googlebot
Disallow: /wp-content/themes/lms/framework/

# global
User-agent: *
Disallow: /cgi-bin/
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /wp-content/cache/
Disallow: /wp-content/plugins/
Disallow: /wp-content/themes/lms/framework/
Disallow: /trackback/
Disallow: /feed/
Disallow: /comments/
Disallow: /category/*/*
Disallow: */trackback/
Disallow: */feed/
Disallow: */comments/
Disallow: /*?
Allow: /wp-content/themes/lms/images/
